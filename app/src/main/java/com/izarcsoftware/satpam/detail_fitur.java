package com.izarcsoftware.satpam;

import android.content.Intent;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.widget.ScrollView;
import android.widget.TextView;

public class detail_fitur extends AppCompatActivity
{
    private GestureDetectorCompat gestureDetectorCompat;
    String judul;
    int detail;
    private TextView LJudul, LIsi;
    private ScrollView SVIsi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_fitur);

        Intent intent = getIntent();
        detail = intent.getIntExtra("detail", 0);
        judul = intent.getStringExtra("judul");

        LIsi = (TextView) findViewById(R.id.LIsi);
        LJudul = (TextView) findViewById(R.id.LJudul);
        SVIsi = (ScrollView) findViewById(R.id.SVIsi);

        LIsi.setText(detail);
        LJudul.setText(judul);

        gestureDetectorCompat = new GestureDetectorCompat(this, new MyGestureListener());
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event)
    {
        this.gestureDetectorCompat.onTouchEvent(event);
        return super.dispatchTouchEvent(event);
    }

    class MyGestureListener extends GestureDetector.SimpleOnGestureListener
    {
        @Override
        public boolean onFling(MotionEvent event1, MotionEvent event2, float velocityX, float velocityY)
        {
            float selisih = event2.getEventTime() - event2.getDownTime();
            float y = event1.getY() - event2.getY();
            float x = event1.getX() - event2.getX();

            if(Math.abs(y) > Math.abs(x) && y < 0 && SVIsi.getScrollY() == 0)
            {
                finish();
                overridePendingTransition(R.anim.slide_up, R.anim.slide_down);
            }
            return true;
        }
    }
}
