package com.izarcsoftware.satpam;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.LayoutAnimationController;
import android.view.animation.TranslateAnimation;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.izarcsoftware.satpam.adapter.FeedListAdapter;
import com.izarcsoftware.satpam.app.AppController;
import com.izarcsoftware.satpam.app.Crypt;
import com.izarcsoftware.satpam.data.FeedItem;
import com.izarcsoftware.satpam.data.StringUtil;
import com.twotoasters.jazzylistview.JazzyEffect;
import com.twotoasters.jazzylistview.JazzyListView;
import com.twotoasters.jazzylistview.effects.CardsEffect;
import com.twotoasters.jazzylistview.effects.SlideInEffect;
import com.twotoasters.jazzylistview.effects.TiltEffect;
import com.twotoasters.jazzylistview.effects.WaveEffect;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Aziz Nur Ariffianto on 0009, 09 Feb 2016.
 */

public class FragmentFasilitas extends Fragment
{
    private static final String TAG = FragmentActivity.class.getSimpleName();
    private static JazzyListView listView;
    private static TextView LInfo;
    public static FeedListAdapter listAdapter;
    public static List<FeedItem> feedItems;
    private static String URL_FEED;
    private static SwipeRefreshLayout mSwipeRefreshLayout;
    public static boolean done = false;
    public static SharedPreferences sharedPref;
    public static Context cc;
    public static int limit = 10;
    public static boolean loadpo = false;

    private static boolean beranda;

    public static void refreshContent(final boolean daricache)
    {
        String id = sharedPref.getString(cc.getString(R.string.p_id_pengguna), "Tidak Ada");
        if (beranda)
        {
            String all = "";
            String sebagai = sharedPref.getString(cc.getString(R.string.p_sebagai), "Tidak Ada");
            if (sebagai.equalsIgnoreCase("Admin E-Complaint") || sebagai.equalsIgnoreCase("Developer E-Complaint") || sebagai.equalsIgnoreCase("Kapolres Banyumas"))
            {
                all = "&all=1";
            }
            URL_FEED = cc.getString(R.string.alamat_server) + cc.getString(R.string.link_feed_laporanfas) + "?oleh=" + id + "&limit=" + String.valueOf(limit) + all;
        }
        else
        {
            URL_FEED = cc.getString(R.string.alamat_server) + cc.getString(R.string.link_feed_laporanfas) + "?me=1&oleh=" + id;
        }

        URL_FEED = Uri.encode(URL_FEED, LokasiLaporanFas.ALLOWED_URI_CHARS);

        Cache cache = AppController.getInstance().getRequestQueue().getCache();
        Cache.Entry entry = cache.get(URL_FEED);
        if (entry != null && daricache) {
            try {
                String data = new String(entry.data, "UTF-8");
                try {
                    parseJsonFeed(new JSONObject(data), false);
                } catch (JSONException e)
                {
                    mSwipeRefreshLayout.setRefreshing(false);
                    refreshContent(false);
                }
            } catch (UnsupportedEncodingException e) {
                mSwipeRefreshLayout.setRefreshing(false);
                refreshContent(false);
            }

        } else {
            JsonObjectRequest jsonReq = new JsonObjectRequest(Request.Method.GET, URL_FEED, null, new Response.Listener<JSONObject>()
            {
                @Override
                public void onResponse(JSONObject response)
                {
                    if (response != null) {
                        feedItems.clear();
                        parseJsonFeed(response, false);
                    }
                }
            }, new Response.ErrorListener()
            {
                @Override
                public void onErrorResponse(VolleyError error) {
                    try
                    {
                        mSwipeRefreshLayout.setRefreshing(false);
                    }
                    catch (Exception e)
                    {
                        refreshContent(false);
                    }
                }
            });

            AppController.getInstance().addToRequestQueue(jsonReq);
        }
    }

    public static void refreshContent(final int lfid)
    {
        String id = sharedPref.getString(cc.getString(R.string.p_id_pengguna), "Tidak Ada");
        URL_FEED = cc.getString(R.string.alamat_server) + cc.getString(R.string.link_feed_laporanfas) + "?oleh=" + id + "&lfid=" + lfid;

        JsonObjectRequest jsonReq = new JsonObjectRequest(Request.Method.GET, URL_FEED, null, new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response) {
                if (response != null) {
                    parseJsonFeed(response, true);
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                try
                {
                    mSwipeRefreshLayout.setRefreshing(false);
                    refreshContent(lfid);
                }
                catch (Exception e)
                {
                    refreshContent(lfid);
                }
            }
        });

        // Adding request to volley request queue
        AppController.getInstance().addToRequestQueue(jsonReq);
    }

    public FragmentFasilitas()
    {

    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        beranda = BerandaActivity.isBeranda();
        done = false;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View convertView = inflater.inflate(R.layout.fragment_fasilitas, container, false);

        cc = convertView.getContext();
        FeedListAdapter.cc = cc;
        sharedPref = cc.getSharedPreferences("File_Pengaturan", Context.MODE_PRIVATE);

        feedItems = new ArrayList<FeedItem>();

        listView = (JazzyListView) convertView.findViewById(R.id.ListFasilitas);
        LInfo = (TextView) convertView.findViewById(R.id.LInfo);

        listView.setTransitionEffect(new TiltEffect());

        mSwipeRefreshLayout = (SwipeRefreshLayout) convertView.findViewById(R.id.fasilitas_swipe_refresh_layout);
        mSwipeRefreshLayout.setProgressBackgroundColorSchemeColor(Color.parseColor("#FF4CA3A3"));
        mSwipeRefreshLayout.setColorSchemeColors(Color.parseColor("#FFFFFFFF"));
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshContent(false);
            }
        });

        /*
        AnimationSet set = new AnimationSet(true);

        Animation animation = new AlphaAnimation(0.0f, 1.0f);
        animation.setDuration(500);
        set.addAnimation(animation);

        LayoutAnimationController controller = new LayoutAnimationController(set, 0.5f);
        listView.setLayoutAnimation(controller);
        */
        listAdapter = new FeedListAdapter(getActivity(), feedItems);
        listView.setAdapter(listAdapter);

        return convertView;
    }

    static int jumlah;

    public static void parseJsonFeed(JSONObject response, Boolean satuid) {
        try {
            JSONArray feedArray = response.getJSONArray("feed");

            if (loadpo && jumlah == feedArray.length())
            {
                Snackbar.make(listView, "Tidak ada laporan informasi lagi.", Snackbar.LENGTH_LONG).show();
            }

            for (int i = 0; i < feedArray.length(); i++)
            {
                JSONObject feedObj = (JSONObject) feedArray.get(i);

                final FeedItem item = new FeedItem();
                item.setId(feedObj.getInt("a"));
                item.setName(feedObj.getString("n"));

                String image = feedObj.isNull("b") ? null : feedObj.getString("b");
                item.setImge(image);
                StringUtil stringUtil = new StringUtil();
                item.setStatus(stringUtil.HapusBaris(feedObj.getString("c")));
                item.setTimeStamp(feedObj.getString("h"));

                item.setLat(feedObj.getString("d"));
                item.setLing(feedObj.getString("e"));

                item.setDukungan(feedObj.getString("l"));
                item.setKomentar(feedObj.getString("m"));

                item.setOleh(feedObj.getString("j"));

                item.setPrioritas(feedObj.getString("g"));

                item.setSudahlike(feedObj.getString("o"));

                item.setBagian("fas");

                item.setIndividual(feedObj.getString("i"));

                item.setAlamat(feedObj.getString("f"));

                item.setSebagai(feedObj.getString("z"));

                if (feedItems.size() < feedArray.length())
                {
                    feedItems.add(item);
                }
                else if (feedItems.size() > feedArray.length() && !satuid)
                {
                    feedItems.clear();
                    feedItems.add(item);
                }
                else
                {
                    for (int j = 0; j < feedItems.size(); j++)
                    {
                        if (feedItems.get(j).getId() == feedObj.getInt("a"))
                        {
                            feedItems.remove(j);
                            feedItems.add(j, item);
                        }
                    }
                }
            }

            if (listView.getVisibility() == View.GONE)
            {
                LInfo.setVisibility(View.GONE);
                listView.setVisibility(View.VISIBLE);
            }

            jumlah = feedItems.size();

            if (feedItems.get(feedItems.size() - 1).getStatus().equalsIgnoreCase("LOAD POST"))
            {
                feedItems.remove(feedItems.size() - 1);
            }

            if (beranda)
            {
                FeedItem loadpost = new FeedItem();
                loadpost.setStatus("LOAD POST");
                loadpost.setBagian("fas");
                feedItems.add(loadpost);
            }

            loadpo = false;

            listAdapter.notifyDataSetChanged();
        } catch (JSONException e) {
            LInfo.setText("Tidak ada berita baru");
            listView.setVisibility(View.GONE);
        }
        mSwipeRefreshLayout.setRefreshing(false);
    }

    public boolean isDiliat = true;

    public boolean isDiliat()
    {
        return isDiliat;
    }

    public void setIsDiliat(boolean isDiliat)
    {
        this.isDiliat = isDiliat;
    }

    public static boolean perlu = true;

    public static boolean isPerlu()
    {
        return perlu;
    }

    public static void setPerlu(boolean perlu)
    {
        FragmentFasilitas.perlu = perlu;
    }

    @Override
    public void onResume()
    {
        super.onResume();

        if (isDiliat && perlu)
        {
            if (!done)
            {
                refreshContent(true);
                done = true;
            }
            else
            {
                refreshContent(false);
            }
        }
        perlu = true;
        loadpo = false;
    }
}
