package com.izarcsoftware.satpam;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.gcm.GcmListenerService;
import com.izarcsoftware.satpam.app.ObjectSerializer;
import com.izarcsoftware.satpam.data.ChatItem;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Aziz Nur Ariffianto on 10/03/2016.
 */
public class PushNotification extends GcmListenerService {
    public static NotificationManager notificationManager;

    @Override
    public void onMessageReceived(String from, Bundle data) {
        String jenis = data.getString("jenis");
        if (jenis.equalsIgnoreCase("Pemberitahuan"))
        {
            String pesan = data.getString("pesan");
            String dari = data.getString("dari");
            String tipe = data.getString("tipe");
            String id = data.getString("id");
            TampilkanNotif(pesan, tipe, dari, id);
        }
        else if (jenis.equalsIgnoreCase("Obrolan"))
        {
            String obid = data.getString("obid");
            String dari = data.getString("dari");
            String untuk = data.getString("untuk");
            String isipesan = data.getString("isipesan");
            String waktu = data.getString("waktu");
            String nama = data.getString("nama");
            AturChat(obid, dari, untuk, isipesan, waktu, nama);
        }
    }

    public void AturChat(String obid, String dari, String untuk, String isipesan, String waktu, String nama)
    {
        try
        {
            if (dari.equalsIgnoreCase(RuangObrolanActivity.tpgid))
            {
                ChatItem item = new ChatItem();
                item.setObid(obid);
                item.setDari(dari);
                item.setUntuk(untuk);
                item.setIsipesan(isipesan);
                item.setWaktu(waktu);

                RuangObrolanActivity.chatItems.add(item);

                if (RuangObrolanActivity.LInfo.getVisibility() == View.VISIBLE)
                {
                    RuangObrolanActivity.LInfo.setVisibility(View.GONE);
                }

                RuangObrolanActivity.listAdapter.notifyDataSetChanged();

                RuangObrolanActivity.LVObrolan.smoothScrollToPosition(RuangObrolanActivity.LVObrolan.getCount());

                try
                {
                    final MediaPlayer mp = MediaPlayer.create(this, R.raw.ecomplaint_tone);
                    mp.start();
                }
                catch (Exception e)
                {

                }
            }
            else
            {
                TampilkanNotif(isipesan, "Obrolan", nama, dari, isipesan);
            }
        }
        catch (Exception e)
        {
            TampilkanNotif(isipesan, "Obrolan", nama, dari, isipesan);
        }
    }

    public void TampilkanNotif(String pesan, String tipe, String dari, String id, String msg)
    {
        NotificationManager nn = notificationManager;

        Intent intent;
        PendingIntent pIntent;

        intent = new Intent(getApplicationContext(), RuangObrolanActivity.class);
        String[] data = {id, dari};
        intent.putExtra("data", data);
        pIntent = PendingIntent.getActivity(getApplicationContext(), (int) System.currentTimeMillis(), intent, 0);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
        builder.setSmallIcon(R.drawable.ic_chat);
        builder.setContentIntent(pIntent);
        builder.setAutoCancel(true);
        builder.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.ic_chat));
        builder.setTicker(msg);
        builder.setContentTitle(dari);
        builder.setContentText(msg);
        builder.setPriority(NotificationCompat.PRIORITY_HIGH);
        Uri suara = Uri.parse("android.resource://com.izarcsoftware.satpam/" + R.raw.ecomplaint_tone);
        builder.setSound(suara);
        builder.setDefaults(Notification.DEFAULT_VIBRATE);

        notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notificationManager.notify(Integer.valueOf(id), builder.build());

        return;
    }

    public void TampilkanNotif(String pesan, String tipe, String dari, String id)
    {
        NotificationManager nn = notificationManager;

        Intent intent;
        PendingIntent pIntent;
        String isian;

        if (tipe.equalsIgnoreCase("Komentar Fasilitas"))
        {
            intent = new Intent(getApplicationContext(), Komentar.class);
            String[] data = {id, "fas"};
            intent.putExtra("data", data);
            intent.putExtra("daripemberitahuan", true);
            pIntent = PendingIntent.getActivity(getApplicationContext(), (int) System.currentTimeMillis(), intent, 0);
            isian = dari + " telah mengomentari laporan informasi Anda.";
        }
        else if (tipe.equalsIgnoreCase("Komentar Pelanggaran"))
        {
            intent = new Intent(getApplicationContext(), Komentar.class);
            String[] data = {id, "plgr"};
            intent.putExtra("data", data);
            intent.putExtra("daripemberitahuan", true);
            pIntent = PendingIntent.getActivity(getApplicationContext(), (int) System.currentTimeMillis(), intent, 0);
            isian = dari + " telah mengomentari laporan pelanggaran Anda.";
        } else if (tipe.equalsIgnoreCase("Pemberitahuan"))
        {
            intent = new Intent(getApplicationContext(), PemberitahuanLangsung.class);
            intent.putExtra("info", pesan);
            pIntent = PendingIntent.getActivity(getApplicationContext(), (int) System.currentTimeMillis(), intent, 0);
            isian = pesan;

            NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
            builder.setSmallIcon(R.drawable.ic_ada_pemberitahuan);
            builder.setContentIntent(pIntent);
            builder.setAutoCancel(true);
            builder.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.ic_ada_pemberitahuan));
            builder.setTicker(isian);
            builder.setContentTitle("E-Complaint");
            builder.setContentText(isian);
            builder.setPriority(NotificationCompat.PRIORITY_HIGH);
            Uri suara = Uri.parse("android.resource://com.izarcsoftware.satpam/" + R.raw.ecomplaint_tone);
            builder.setSound(suara);
            builder.setDefaults(Notification.DEFAULT_VIBRATE);

            notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            notificationManager.notify((int) System.currentTimeMillis(), builder.build());

            addPemberitahuan(isian);

            return;
        }
        else
        {
            intent = new Intent(getApplicationContext(), PemberitahuanActivity.class);
            pIntent = PendingIntent.getActivity(getApplicationContext(), (int) System.currentTimeMillis(), intent, 0);
            isian = pesan;
        }

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
        builder.setSmallIcon(R.drawable.ic_ada_pemberitahuan);
        builder.setContentIntent(pIntent);
        builder.setAutoCancel(true);
        builder.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.ic_ada_pemberitahuan));
        builder.setTicker(isian);
        builder.setContentTitle("E-Complaint");
        builder.setContentText(isian);
        builder.setSubText("Tap untuk membuka " + tipe.replace("Fasilitas", "Informasi") + " Anda.");
        builder.setPriority(NotificationCompat.PRIORITY_HIGH);
        Uri suara = Uri.parse("android.resource://com.izarcsoftware.satpam/" + R.raw.ecomplaint_tone);
        builder.setSound(suara);
        builder.setDefaults(Notification.DEFAULT_VIBRATE);

        notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notificationManager.notify((int) System.currentTimeMillis(), builder.build());
    }

    public void addPemberitahuan(String t)
    {
        SharedPreferences prefs = getSharedPreferences("File_Pengaturan", Context.MODE_PRIVATE);

        ArrayList<String> pemberitahuan = new ArrayList<String>();

        if (!prefs.getString("pemberitahuan", "Tidak Ada").equalsIgnoreCase("Tidak Ada"))
        {
            try {
                pemberitahuan = (ArrayList<String>) ObjectSerializer.deserialize(prefs.getString("pemberitahuan", ObjectSerializer.serialize(new ArrayList<String>())));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        try
        {
        }
        catch (Exception e)
        {}

        pemberitahuan.add(t);

        SharedPreferences.Editor editor = prefs.edit();
        try {
            editor.putString("pemberitahuan", ObjectSerializer.serialize(pemberitahuan));
        } catch (IOException e) {
            e.printStackTrace();
        }
        editor.commit();
    }
}
