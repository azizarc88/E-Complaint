package com.izarcsoftware.satpam;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.LayoutAnimationController;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.izarcsoftware.satpam.adapter.PemerintahListAdapter;
import com.izarcsoftware.satpam.adapter.PenggunaListAdapter;
import com.izarcsoftware.satpam.app.AppController;
import com.izarcsoftware.satpam.data.PemerintahItem;
import com.izarcsoftware.satpam.data.PenggunaItem;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ObrolanActivity extends AppCompatActivity
{
    private Toolbar toolbar;
    private static String URL_FEED;
    private static ListView LVPemerintah;
    private static PemerintahListAdapter listAdapter;
    private TextView LInfo;
    private static List<PemerintahItem> pemerintahItems;
    private String pgid;
    private String sebagai;

    private void RefreshContent()
    {
        JsonObjectRequest jsonReq = new JsonObjectRequest(Request.Method.GET, URL_FEED, null, new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response) {
                if (response != null) {
                    parseJsonLaporan(response);
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                if (!isFinishing())
                {
                    RefreshContent();
                }
            }
        });

        AppController.getInstance().addToRequestQueue(jsonReq);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_obrolan);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Obrolan Masyarakat");
        toolbar.setNavigationOnClickListener(new Toolbar.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                finish();
            }
        });

        LVPemerintah = (ListView) findViewById(R.id.LVPemerintah);
        LInfo = (TextView) findViewById(R.id.LInfo);

        LVPemerintah.setOnItemClickListener(new ListView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                Intent intent = new Intent(getApplicationContext(), RuangObrolanActivity.class);
                String[] data = {pemerintahItems.get(position).getPgid(), pemerintahItems.get(position).getNama()};
                intent.putExtra("data", data);
                startActivity(intent);
            }
        });

        SharedPreferences sharedPref;
        sharedPref = getApplicationContext().getSharedPreferences("File_Pengaturan", Context.MODE_PRIVATE);
        sebagai = sharedPref.getString(getString(R.string.p_sebagai), "Tidak Ada");
        pgid = sharedPref.getString(getString(R.string.p_id_pengguna), "Tidak Ada");

        if (sebagai.equalsIgnoreCase("Masyarakat"))
        {
            URL_FEED = getString(R.string.alamat_server) + getString(R.string.link_ambil_obrolan_user);
        }
        else
        {
            URL_FEED = getString(R.string.alamat_server) + getString(R.string.link_ambil_obrolan_admin) + "?untuk=" + pgid;
        }

        AnimationSet set = new AnimationSet(true);

        Animation animation = new AlphaAnimation(0.0f, 1.0f);
        animation.setDuration(200);
        set.addAnimation(animation);

        animation = new TranslateAnimation(
                Animation.RELATIVE_TO_SELF, 0.0f,Animation.RELATIVE_TO_SELF, 0.0f,
                Animation.RELATIVE_TO_SELF, -1.0f, Animation.RELATIVE_TO_SELF, 0.0f
        );
        animation.setDuration(100);
        set.addAnimation(animation);

        LayoutAnimationController controller = new LayoutAnimationController(set, 0.5f);
        LVPemerintah.setLayoutAnimation(controller);

        pemerintahItems = new ArrayList<PemerintahItem>();
        listAdapter = new PemerintahListAdapter(pemerintahItems, this);
        LVPemerintah.setAdapter(listAdapter);

        if (sebagai.equalsIgnoreCase("Masyarakat"))
        {
            LInfo.setText("Dalam urutan abjad");
        }
        else
        {
            LInfo.setText("Dalam urutan terbaru ke terlama");
        }

        RefreshContent();
    }

    private void parseJsonLaporan(JSONObject response)
    {
        try
        {
            JSONArray feedArray = response.getJSONArray("feed");

            for (int i = 0; i < feedArray.length(); i++)
            {
                JSONObject feedObj = (JSONObject) feedArray.get(i);

                PemerintahItem item = new PemerintahItem();
                item.setNomor(i + 1);
                item.setPgid(feedObj.getString("pgid"));
                item.setNama(feedObj.getString("nama"));
                item.setSebagai(feedObj.getString("sebagai"));

                pemerintahItems.add(item);
            }

            listAdapter.notifyDataSetChanged();
        }
        catch (Exception e)
        {
        }
    }
}
