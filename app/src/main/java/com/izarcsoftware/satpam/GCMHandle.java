package com.izarcsoftware.satpam;

import android.app.Activity;
import android.app.IntentService;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.support.design.widget.Snackbar;
import android.widget.Toast;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.izarcsoftware.satpam.R;
import com.izarcsoftware.satpam.RequestHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;

/**
 * Created by Aziz Nur Ariffianto on 11/03/2016.
 */
public class GCMHandle extends IntentService
{
    GoogleCloudMessaging gcm = new GoogleCloudMessaging();

    private void doGcmSendUpstreamMessage() {

    }

    public GCMHandle()
    {
        super("GCMHandle");
    }

    @Override
    public void onCreate() {

    }

    @Override
    protected void onHandleIntent(Intent intent) {

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        String dari = "", bagian = "", isipesan = "", oleh = "", untuk = "", tipe = "";

        try
        {
             tipe = intent.getStringExtra("tipe");
        }
        catch (Exception ex)
        {
        }

        if (tipe.equalsIgnoreCase("komentar"))
        {
            try
            {
                bagian = intent.getStringExtra("bagian");
                oleh = intent.getStringExtra("oleh");
                untuk = intent.getStringExtra("untuk");
            }
            catch (Exception e)
            {
            }

            KirimNotifikasi(getApplicationContext(), "null", oleh, "a", bagian, untuk);
        }
        else if (tipe.equalsIgnoreCase("obrolan"))
        {
            try
            {
                dari = intent.getStringExtra("dari");
                isipesan = intent.getStringExtra("isipesan");
                untuk = intent.getStringExtra("untuk");
            }
            catch (Exception e)
            {
            }

            KirimChat(getApplicationContext(), dari, untuk, isipesan);
        }

        return Service.START_STICKY;
    }

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    @Override
    public void onDestroy() {
    }

    public static void KirimNotifikasi(Context context, String pgid, String dari, String pesan, String tipe, String id)
    {
        class NotifikasiManager extends AsyncTask<String, Void, String>
        {
            private Context context;
            String pgid; String dari; String pesan; String tipe; String id;

            public NotifikasiManager(Context context) {
                this.context = context;
            }

            @Override
            protected void onPreExecute()
            {
                super.onPreExecute();
            }

            @Override
            protected String doInBackground(String... arg0)
            {
                RequestHandler rh = new RequestHandler();

                String link;
                String result;

                try
                {
                    HashMap<String,String> data = new HashMap<>();
                    data.put("pgid", arg0[0]);
                    pgid = arg0[0];

                    data.put("dari", arg0[1]);
                    dari = arg0[1];

                    data.put("pesan", arg0[2]);
                    pesan = arg0[2];

                    data.put("tipe", arg0[3]);
                    tipe = arg0[3];

                    data.put("id", arg0[4]);
                    id = arg0[4];

                    link = context.getString(R.string.alamat_server) + context.getString(R.string.link_kirim_notifikasi);

                    result = rh.sendPostRequest(link, data);

                    return result;
                } catch (Exception e)
                {
                    return new String("Exception: " + e.getMessage());
                }
            }

            @Override
            protected void onPostExecute(String result)
            {
                String jsonStr = result;
                if (jsonStr != null)
                {
                    try
                    {
                        JSONObject jsonObj = new JSONObject(jsonStr);
                        String query_result = jsonObj.getString("hasil");
                        if (query_result.equals("SUKSES"))
                        {

                        } else if (query_result.equals("GAGAL"))
                        {
                        }
                    } catch (JSONException e)
                    {
                        new NotifikasiManager(context).execute(pgid, dari, pesan, tipe, id);
                    }
                } else
                {
                    new NotifikasiManager(context).execute(pgid, dari, pesan, tipe, id);
                }
            }
        }

        new NotifikasiManager(context).execute(pgid, dari, pesan, tipe, id);
    }

    public static void KirimChat(Context context, String dari, String untuk, String isipesan)
    {
        class ChatManager extends AsyncTask<String, Void, String>
        {
            private Context context;
            String dari; String untuk; String pesan;

            public ChatManager(Context context) {
                this.context = context;
            }

            @Override
            protected void onPreExecute()
            {
                super.onPreExecute();
            }

            @Override
            protected String doInBackground(String... arg0)
            {
                RequestHandler rh = new RequestHandler();

                String link;
                String result;

                try
                {
                    HashMap<String,String> data = new HashMap<>();
                    data.put("dari", arg0[0]);
                    dari = arg0[0];
                    data.put("untuk", arg0[1]);
                    untuk = arg0[1];
                    data.put("isipesan", arg0[2]);
                    pesan = arg0[2];

                    link = context.getString(R.string.alamat_server) + context.getString(R.string.link_kirim_chat);

                    result = rh.sendPostRequest(link, data);

                    return result;
                } catch (Exception e)
                {
                    return new String("Exception: " + e.getMessage());
                }
            }

            @Override
            protected void onPostExecute(String result)
            {
                String jsonStr = result;
                if (jsonStr != null)
                {
                    try
                    {
                        JSONObject jsonObj = new JSONObject(jsonStr);
                        String query_result = jsonObj.getString("hasil");
                        if (query_result.equals("SUKSES"))
                        {

                        } else if (query_result.equals("GAGAL"))
                        {
                        }
                    } catch (JSONException e)
                    {
                        new ChatManager(context).execute(dari, untuk, pesan);
                    }
                } else
                {
                    new ChatManager(context).execute(dari, untuk, pesan);
                }
            }
        }

        new ChatManager(context).execute(dari, untuk, isipesan);
    }
}
