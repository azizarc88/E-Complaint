package com.izarcsoftware.satpam;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Handler;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.AsyncTask;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.izarcsoftware.satpam.app.Animasi;
import com.izarcsoftware.satpam.app.Crypt;
import com.izarcsoftware.satpam.app.Satpam;

import android.os.Build;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Base64;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.ExecutionException;
import java.util.jar.Attributes;

public class LoginActivity extends AppCompatActivity
{
    public static final String ID_USER = "";
    public static Context ccontext;

    private AutoCompleteTextView mEmailView;
    private EditText mPasswordView;
    private TextView LInfo, LLupaPass, LMasukAkun;
    private View mProgressView;
    private View mLoginFormView;
    private LoginButton loginButton;
    private CallbackManager callbackManager;
    private AccessTokenTracker accessTokenTracker;
    private LinearLayout LLIsi, LLInfo, LLUtama, LLMasuk;
    private String gcmid;
    private AccessToken token;

    private static final String TAG = "LoginActivity";

    SharedPreferences sharedPref;

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void TampilkanIsi(final Boolean tampilkan)
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2)
        {
            int shortAnimTime = 500;

            LLIsi.setVisibility(tampilkan ? View.VISIBLE : View.GONE);
            LLIsi.animate().setDuration(shortAnimTime).alpha(
                    tampilkan ? 1 : 0).setListener(new AnimatorListenerAdapter()
            {
                @Override
                public void onAnimationEnd(Animator animation)
                {
                    LLIsi.setVisibility(tampilkan ? View.VISIBLE : View.GONE);
                }
            });
        } else
        {
            LLIsi.setVisibility(tampilkan ? View.VISIBLE : View.GONE);
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void TampilkanInfo(final Boolean tampilkan)
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2)
        {
            int shortAnimTime = 500;

            LLInfo.setVisibility(tampilkan ? View.VISIBLE : View.GONE);
            LLInfo.animate().setDuration(shortAnimTime).alpha(
                    tampilkan ? 1 : 0).setListener(new AnimatorListenerAdapter()
            {
                @Override
                public void onAnimationEnd(Animator animation)
                {
                    LLInfo.setVisibility(tampilkan ? View.VISIBLE : View.GONE);
                }
            });
        } else
        {
            LLInfo.setVisibility(tampilkan ? View.VISIBLE : View.GONE);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_login);

        //getSupportActionBar().hide();

        String PROJECT_NUMBER="1081660124702";
        GCMClientManager pushClientManager = new GCMClientManager(this, PROJECT_NUMBER);
        pushClientManager.registerIfNeeded(new GCMClientManager.RegistrationCompletedHandler() {
            @Override
            public void onSuccess(String registrationId, boolean isNewRegistration) {
                gcmid = registrationId;
            }

            @Override
            public void onFailure(String ex) {
                super.onFailure(ex);
                gcmid = ex;
            }
        });

        LLIsi = (LinearLayout) findViewById(R.id.LLIsi);
        LLInfo = (LinearLayout) findViewById(R.id.LLInfo);
        LLMasuk = (LinearLayout) findViewById(R.id.LLMasuk);
        LLUtama = (LinearLayout) findViewById(R.id.LLUtama);
        LInfo = (TextView) findViewById(R.id.LInfo);

        loginButton = (LoginButton) findViewById(R.id.login_button);
        loginButton.setReadPermissions(Arrays.asList("public_profile, email, user_birthday, user_hometown"));
        callbackManager = CallbackManager.Factory.create();

        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(final LoginResult loginResult)
            {
                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback()
                        {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response)
                            {
                                String tanggallahir = "", nama = "", email = "", kota = "", token = "";
                                try
                                {
                                    email = object.getString("email");
                                }
                                catch (Exception e) {}
                                try
                                {
                                    nama = object.getString("name");
                                }
                                catch (Exception e) {}
                                try
                                {
                                    JSONObject hometown = object.getJSONObject("hometown");
                                    kota = hometown.getString("name");
                                }
                                catch (Exception e)
                                {
                                }
                                try
                                {
                                    token = loginResult.getAccessToken().getToken();
                                }
                                catch (Exception e) {}
                                try
                                {
                                    String expectedPattern = "MM/dd/yyyy";
                                    SimpleDateFormat formatter = new SimpleDateFormat(expectedPattern);
                                    String userInput = object.getString("birthday");
                                    Date tanggal = null;
                                    try
                                    {
                                        tanggal = formatter.parse(userInput);
                                    } catch (ParseException e)
                                    {
                                        e.printStackTrace();
                                    }
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                                    Calendar cal = Calendar.getInstance();
                                    sdf.setTimeZone(cal.getTimeZone());

                                    tanggallahir = sdf.format(tanggal);
                                }
                                catch (Exception e) {}


                                new LoginFacebookActivity(getApplicationContext()).execute(tanggallahir, nama, email, kota, token);
                            }
                        });

                Bundle parameters = new Bundle();
                parameters.putString("fields", "id, name, email, gender, birthday, hometown");
                request.setParameters(parameters);
                request.executeAsync();


                accessTokenTracker = new AccessTokenTracker() {
                    @Override
                    protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken,
                                                               AccessToken currentAccessToken) {
                        if (currentAccessToken == null) {
                        }
                    }
                };
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {

            }
        });

        Button BBatal = (Button) findViewById(R.id.BBatal);
        BBatal.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Animasi.Fade(LLMasuk, LLUtama, 200);
            }
        });

        mEmailView = (AutoCompleteTextView) findViewById(R.id.email);
        LLupaPass = (TextView) findViewById(R.id.LLupaPass);
        LLupaPass.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), Lupakatasandi.class);
                startActivity(intent);
                finish();
            }
        });

        LMasukAkun = (TextView) findViewById(R.id.LMasukAkun);
        LMasukAkun.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Animasi.Fade(LLUtama, LLMasuk, 200);
            }
        });

        mPasswordView = (EditText) findViewById(R.id.password);
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener()
        {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent)
            {
                if (id == R.id.login || id == EditorInfo.IME_NULL)
                {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        Button mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        mEmailSignInButton.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                attemptLogin();
            }
        });

        ImageView BDaftar = (ImageView) findViewById(R.id.email_sign_up_button);
        BDaftar.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent(getApplicationContext(), SigninActivity.class);
                startActivity(intent);
                finish();
            }
        });

        ImageView BLoginFacebook = (ImageView) findViewById(R.id.BLoginFacebook);
        BLoginFacebook.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                loginButton.performClick();
            }
        });

        mLoginFormView = findViewById(R.id.email_login_form);
        mProgressView = findViewById(R.id.login_progress);

        Intent i = getIntent();
        mEmailView.setText(i.getStringExtra(SigninActivity.EMAIL));
        ccontext = getApplicationContext();

        if (!TextUtils.isEmpty(mEmailView.getText().toString()))
        {
            LMasukAkun.performClick();
            mPasswordView.requestFocus();

            Snackbar.make(LMasukAkun, "Harap segera loginkan Akun baru Anda", Snackbar.LENGTH_LONG).show();
        }

        sharedPref = getSharedPreferences("File_Pengaturan", Context.MODE_PRIVATE);
    }

    String nohpt, emailt, passwordt, crypt;

    @Override
    protected void onResumeFragments()
    {
        super.onResumeFragments();

        try
        {
            token = AccessToken.getCurrentAccessToken();
        }
        catch (Exception e)
        {

        }

        LLIsi = (LinearLayout) findViewById(R.id.LLIsi);
        LLInfo = (LinearLayout) findViewById(R.id.LLInfo);
        LLMasuk = (LinearLayout) findViewById(R.id.LLMasuk);
        LLUtama = (LinearLayout) findViewById(R.id.LLUtama);
        LInfo = (TextView) findViewById(R.id.LInfo);

        String id_pengguna = sharedPref.getString(getString(R.string.p_id_pengguna), "Tidak Ada");

        if (!id_pengguna.equalsIgnoreCase("Tidak Ada"))
        {
            crypt = sharedPref.getString(getString(R.string.p_crypt), "Tidak Ada");
            if (crypt.equalsIgnoreCase("Tidak Ada"))
            {
                nohpt = sharedPref.getString(getString(R.string.p_nohp), "Tidak Ada");
                emailt = sharedPref.getString(getString(R.string.p_email), "Tidak Ada");
                passwordt = sharedPref.getString(getString(R.string.p_password), "Tidak Ada");
            }
            else
            {
                nohpt = Crypt.Decrypt(sharedPref.getString(getString(R.string.p_nohp), "Tidak Ada"));
                emailt = Crypt.Decrypt(sharedPref.getString(getString(R.string.p_email), "Tidak Ada"));
                passwordt = Crypt.Decrypt(sharedPref.getString(getString(R.string.p_password), "Tidak Ada"));
            }

            final String satu;

            if (TextUtils.isEmpty(nohpt))
            {
                satu = emailt;
            }
            else
            {
                satu = nohpt;
            }

            if (TextUtils.isEmpty(passwordt))
            {
                try
                {
                    passwordt = token.getToken();
                }
                catch (Exception e)
                {}
            }

            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    CekUser(satu, passwordt);
                }
            }, 1000);
        }
        else
        {
            showProgress(false);
            TampilkanIsi(true);
        }
    }

    public String email, password;

    private void attemptLogin()
    {
        mEmailView.setError(null);
        mPasswordView.setError(null);

        email = mEmailView.getText().toString();
        password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        if (!TextUtils.isEmpty(password) && !isPasswordValid(password))
        {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        if (TextUtils.isEmpty(email))
        {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        } else if (TextUtils.isEmpty(password))
        {
            mPasswordView.setError(getString(R.string.error_field_required));
            focusView = mPasswordView;
            cancel = true;
        }

        if (cancel)
        {
            focusView.requestFocus();
        } else
        {
            showProgress(true);

            CekUser(email, password);
        }
    }

    private boolean isPasswordValid(String password)
    {
        return password.length() > 4;
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show)
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2)
        {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter()
            {
                @Override
                public void onAnimationEnd(Animator animation)
                {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter()
            {
                @Override
                public void onAnimationEnd(Animator animation)
                {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else
        {
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    public void CekUser(String user, String pass)
    {
        new LoginManagerMain(getApplicationContext()).execute(user, pass, gcmid);
    }

    class LoginManagerMain extends AsyncTask<String, Void, String> {

        private Context context;

        public LoginManagerMain(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... arg0) {
            RequestHandler rh = new RequestHandler();


            String link;
            String result;

            try {
                HashMap<String,String> data = new HashMap<>();
                data.put("email", arg0[0]);
                data.put("password", arg0[1]);
                data.put("gcm", arg0[2]);

                TelephonyManager telephonyManager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
                String android_id = telephonyManager.getDeviceId();
                data.put("phoneid", android_id);

                link = getString(R.string.alamat_server) + getString(R.string.link_login_pengguna);
                result = rh.sendPostRequest(link, data);;

                return result;
            } catch (Exception e) {
                return new String("Exception: " + e.getMessage());
            }
        }

        @Override
        protected void onPostExecute(String result) {
            String jsonStr = Crypt.Decrypt(result);
            if (jsonStr != null) {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);
                    String hasil = jsonObj.getString("hasil");
                    String infog = jsonObj.getString("info");
                    if (hasil.equals("ADA")) {
                        String pgid = jsonObj.getString("pgid");
                        String sebagai = jsonObj.getString("sebagai");
                        String nama = jsonObj.getString("nama");
                        String pass = jsonObj.getString("pass");
                        String nohp = jsonObj.getString("nohp");
                        String email = jsonObj.getString("email");
                        String belumlengkap = jsonObj.getString("belumlengkap");

                        SharedPreferences.Editor editor = sharedPref.edit();
                        editor.putString(getString(R.string.p_id_pengguna), pgid);
                        editor.putString(getString(R.string.p_sebagai), sebagai);
                        editor.putString(getString(R.string.p_nama), nama);
                        editor.putString(getString(R.string.p_nohp), Crypt.Encrypt(nohp));
                        editor.putString(getString(R.string.p_email), Crypt.Encrypt(email));
                        editor.putString(getString(R.string.p_password), Crypt.Encrypt(pass));
                        editor.putString(getString(R.string.p_belumlengkap), belumlengkap);
                        editor.putString(getString(R.string.p_crypt), "1");
                        editor.commit();

                        String[] data = {pgid, sebagai, nama, nohp, email};
                        Intent i = new Intent(getApplicationContext(), MainActivity.class);
                        i.putExtra(ID_USER, data);
                        if (!TextUtils.isEmpty(infog))
                        {
                            int versionCode = BuildConfig.VERSION_CODE;
                            int versiinfo = Integer.valueOf(infog.substring(infog.lastIndexOf("(") + 1, infog.length() - 1));
                            if (versionCode < versiinfo)
                            {
                                i.putExtra("infog", infog);
                            }
                        }
                        startActivity(i);

                        finish();
                    } else if (hasil.equals("TIDAK ADA")) {
                        showProgress(false);
                        try
                        {
                            Snackbar.make(LLIsi, "Email / No.Telp dan Password tidak valid.", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                        }
                        catch (Exception e)
                        {
                            Toast.makeText(context, "Email / No.Telp dan Password tidak valid.", Toast.LENGTH_LONG).show();
                        }
                        TampilkanIsi(true);

                        if (!TextUtils.isEmpty(infog))
                        {
                            int versionCode = BuildConfig.VERSION_CODE;
                            int versiinfo = Integer.valueOf(infog.substring(infog.lastIndexOf("(") + 1, infog.length() - 1));
                            if (versionCode < versiinfo)
                            {
                                Toast.makeText(context, infog, Toast.LENGTH_LONG).show();
                            }
                        }
                    } else if (hasil.equals("DIBLOKIR")) {
                        showProgress(false);
                        try
                        {
                            Snackbar.make(LLIsi, "Akun Anda untuk sementara diblokir, silakan hubungi Admin.", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                        }
                        catch (Exception e)
                        {
                            Toast.makeText(context, "Akun Anda untuk sementara diblokir, silakan hubungi Admin.", Toast.LENGTH_LONG).show();
                        }
                        TampilkanIsi(true);

                        if (!TextUtils.isEmpty(infog))
                        {
                            int versionCode = BuildConfig.VERSION_CODE;
                            int versiinfo = Integer.valueOf(infog.substring(infog.lastIndexOf("(") + 1, infog.length() - 1));
                            if (versionCode < versiinfo)
                            {
                                Toast.makeText(context, infog, Toast.LENGTH_LONG).show();
                            }
                        }
                    } else if (hasil.equals("INFO")) {
                        String info = jsonObj.getString("info");
                        showProgress(false);
                        LInfo.setText(info);
                        LLIsi.setVisibility(View.GONE);
                        TampilkanInfo(true);
                    } else {
                        showProgress(false);
                        try
                        {
                            Snackbar.make(LLIsi, "Tidak dapat terhubung ke database. ", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                        }
                        catch (Exception e)
                        {
                            Toast.makeText(context, "Tidak dapat terhubung ke database. ", Toast.LENGTH_LONG).show();
                        }
                        TampilkanIsi(true);

                        if (!TextUtils.isEmpty(infog))
                        {
                            int versionCode = BuildConfig.VERSION_CODE;
                            int versiinfo = Integer.valueOf(infog.substring(infog.lastIndexOf("(")), infog.length() - 1);
                            if (versionCode < versiinfo)
                            {
                                Toast.makeText(context, infog, Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                } catch (JSONException e)
                {
                    final String nohp, email, password, crypt;
                    crypt = sharedPref.getString(getString(R.string.p_crypt), "Tidak Ada");
                    if (crypt.equalsIgnoreCase("Tidak Ada"))
                    {
                        nohp = sharedPref.getString(getString(R.string.p_nohp), "Tidak Ada");
                        email = sharedPref.getString(getString(R.string.p_email), "Tidak Ada");
                        password = sharedPref.getString(getString(R.string.p_password), "Tidak Ada");
                    }
                    else
                    {
                        nohp = Crypt.Decrypt(sharedPref.getString(getString(R.string.p_nohp), "Tidak Ada"));
                        email = Crypt.Decrypt(sharedPref.getString(getString(R.string.p_email), "Tidak Ada"));
                        password = Crypt.Decrypt(sharedPref.getString(getString(R.string.p_password), "Tidak Ada"));
                    }

                    final String satu;

                    if (TextUtils.isEmpty(nohp))
                    {
                        satu = email;
                    }
                    else
                    {
                        satu = nohp;
                    }

                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            CekUser(satu, password);
                        }
                    }, 1000);
                }
            } else
            {
                showProgress(false);
                try
                {
                    Snackbar.make(LLIsi, "Terjadi kesalahan.", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                }
                catch (Exception e)
                {
                    Toast.makeText(context, "Terjadi kesalahan. " + jsonStr.toString(), Toast.LENGTH_LONG).show();
                }
                TampilkanIsi(true);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    class LoginFacebookActivity extends AsyncTask<String, Void, String>
    {
        private Context context;
        private ProgressDialog pDialog;

        String emailfb, token;

        public LoginFacebookActivity(Context context)
        {
            this.context = context;
        }

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            pDialog = new ProgressDialog(LoginActivity.this);
            pDialog.setMessage("Memproses...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... arg0)
        {
            RequestHandler rh = new RequestHandler();

            String link, result;

            try {
                HashMap<String,String> data = new HashMap<>();
                data.put("tanggallahir", arg0[0]);
                data.put("nama", arg0[1]);
                data.put("email", arg0[2]);
                data.put("kota", arg0[3]);
                data.put("token", arg0[4]);

                emailfb = arg0[2];
                token = arg0[4];

                link = getString(R.string.alamat_server) + getString(R.string.link_login_facebook);

                result = rh.sendPostRequest(link, data);

                return result;
            }
            catch (Exception e)
            {
                return new String("Exception: " + e.getMessage());
            }
        }

        @Override
        protected void onPostExecute(String result)
        {
            String jsonStr = result;
            if (jsonStr != null)
            {
                try
                {
                    JSONObject jsonObj = new JSONObject(jsonStr);
                    String query_result = jsonObj.getString("hasil");
                    if (query_result.equals("SUKSES"))
                    {
                        new LoginManagerMain(getApplicationContext()).execute(emailfb, token, gcmid);
                    } else if (query_result.equals("GAGAL"))
                    {
                        Toast.makeText(context, "Pendaftaran gagal.", Toast.LENGTH_SHORT).show();
                    } else if (query_result.equals("NO HP TIDAK VALID"))
                    {
                        Toast.makeText(context, "No. Handphone sudah dipakai.", Toast.LENGTH_SHORT).show();
                    } else
                    {
                        Toast.makeText(context, "Tidak dapat terhubung ke database.", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e)
                {
                    e.printStackTrace();
                    Toast.makeText(context, "Tidak dapat terhubung ke database, periksa koneksi internet kamu. ", Toast.LENGTH_LONG).show();
                }
            } else
            {
                Toast.makeText(context, "Tidak dapat mengambil data JSON.", Toast.LENGTH_SHORT).show();
            }

            if (pDialog.isShowing())
                pDialog.dismiss();
        }
    }
}

