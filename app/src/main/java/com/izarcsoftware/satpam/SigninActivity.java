package com.izarcsoftware.satpam;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class SigninActivity extends AppCompatActivity
{
    private EditText TBNama, TBNoHp, TBPassword, TBUlangiPassword;
    private Button BDaftar;
    private boolean daftar;

    public static final String EMAIL = "";

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if (isAllValid())
                {
                    String[] isi = AmbilIsian();
                    new SignupActivity(getApplicationContext()).execute(isi[0],
                            isi[1],
                            isi[2],
                            isi[3]);
                }
            }
        });

        TBNama = (EditText) findViewById(R.id.TBNama);
        TBNoHp = (EditText) findViewById(R.id.TBNoHp);
        TBPassword = (EditText) findViewById(R.id.TBPassword);
        TBUlangiPassword = (EditText) findViewById(R.id.TBUlangiPassword);

        BDaftar = (Button) findViewById(R.id.BDaftar);
        BDaftar.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (isAllValid())
                {
                    String[] isi = AmbilIsian();
                    new SignupActivity(getApplicationContext()).execute(isi[0],
                            isi[1],
                            isi[2],
                            isi[3]);
                }
            }
        });

        try
        {
            TelephonyManager tm = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
            TBNoHp.setText(tm.getLine1Number());
        }
        catch (Exception e)
        {

        }
    }

    private boolean isAllValid()
    {
        Boolean valid = true;

        if (TextUtils.isEmpty(TBNama.getText().toString()))
        {
            TBNama.setError(getString(R.string.error_field_required));
            TBNama.requestFocus();
            valid = false;
        }
        else if (TextUtils.isEmpty(TBNoHp.getText().toString()))
        {
            TBNoHp.setError(getString(R.string.error_field_required));
            TBNoHp.requestFocus();
            valid = false;
        }
        else if (!TextUtils.isEmpty(TBNoHp.getText().toString()) && TBNoHp.getText().toString().length() < 10)
        {
            TBNoHp.setError(getString(R.string.error_invalid_nohp));
            TBNoHp.requestFocus();
            valid = false;
        }
        else if (TextUtils.isEmpty(TBPassword.getText().toString()))
        {
            TBPassword.setError(getString(R.string.error_field_required));
            TBPassword.requestFocus();
            valid = false;
        }
        else if (!TextUtils.isEmpty(TBPassword.getText().toString()) && TBPassword.getText().toString().length() < 5)
        {
            TBPassword.setError(getString(R.string.error_invalid_password));
            TBPassword.requestFocus();
            valid = false;
        }
        else if (!TBPassword.getText().toString().equals(TBUlangiPassword.getText().toString()))
        {
            TBUlangiPassword.setError(getString(R.string.notmatch_password));
            TBUlangiPassword.requestFocus();
            valid = false;
        }

        return valid;
    }

    public String[] AmbilIsian()
    {
        String[] Isian = {"", "", "", ""};

        Isian[0] = "1";

        while (TBNama.getText().toString().substring(0, 1).equalsIgnoreCase(" "))
        {
            TBNama.setText(TBNama.getText().toString().substring(1));
        }

        String str = TBNama.getText().toString();
        String[] strArray = str.split(" ");
        StringBuilder builder = new StringBuilder();
        for (String s : strArray) {
            String cap = s.substring(0, 1).toUpperCase() + s.substring(1).toLowerCase();
            builder.append(cap + " ");
        }

        Isian[1] = builder.toString();
        Isian[2] = TBNoHp.getText().toString();
        Isian[3] = TBPassword.getText().toString();

        return  Isian;
    }

    private ProgressDialog pDialog;

    class SignupActivity extends AsyncTask<String, Void, String>
    {
        private Context context;

        public SignupActivity(Context context)
        {
            this.context = context;
        }

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            pDialog = new ProgressDialog(SigninActivity.this);
            pDialog.setMessage("Mendaftarkan...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... arg0)
        {
            RequestHandler rh = new RequestHandler();

            String link;
            String result;

            try {
                HashMap<String,String> data = new HashMap<>();
                data.put("sebagai", arg0[0]);
                data.put("nama", arg0[1]);
                data.put("nohp", arg0[2]);
                data.put("password", arg0[3]);

                link = getString(R.string.alamat_server) + getString(R.string.link_tambah_pengguna);

                result = rh.sendPostRequest(link, data);

                return result;
            }
            catch (Exception e)
            {
                return new String("Exception: " + e.getMessage());
            }
        }

        @Override
        protected void onPostExecute(String result)
        {
            String jsonStr = result;
            if (jsonStr != null)
            {
                try
                {
                    JSONObject jsonObj = new JSONObject(jsonStr);
                    String query_result = jsonObj.getString("hasil");
                    if (query_result.equals("SUKSES"))
                    {
                        Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                        i.putExtra(EMAIL, TBNoHp.getText().toString());
                        startActivity(i);
                        finish();
                    } else if (query_result.equals("GAGAL"))
                    {
                        Toast.makeText(context, "Pendaftaran gagal.", Toast.LENGTH_SHORT).show();
                    } else if (query_result.equals("NO HP TIDAK VALID"))
                    {
                        Toast.makeText(context, "No. Handphone sudah dipakai.", Toast.LENGTH_SHORT).show();
                    } else
                    {
                        Toast.makeText(context, "Tidak dapat terhubung ke database.", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e)
                {
                    e.printStackTrace();
                    Toast.makeText(context, "Tidak dapat terhubung ke database, periksa koneksi internet kamu. " + jsonStr.toString(), Toast.LENGTH_LONG).show();
                }
            } else
            {
                Toast.makeText(context, "Tidak dapat mengambil data JSON.", Toast.LENGTH_SHORT).show();
            }

            if (pDialog.isShowing())
                pDialog.dismiss();
        }
    }

    @Override
    public void onBackPressed()
    {
        Intent i = new Intent(getApplicationContext(), LoginActivity.class);
        startActivity(i);
        finish();
        super.onBackPressed();
    }
}
