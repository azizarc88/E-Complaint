package com.izarcsoftware.satpam;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.os.Build;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Display;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.LayoutAnimationController;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.izarcsoftware.satpam.adapter.ImagePageAdapter;

import java.util.Map;

public class Fitur extends ActionBarActivity implements ViewPager.OnPageChangeListener
{
    SharedPreferences sharedPref;
    private GestureDetectorCompat gestureDetectorCompat;

    int[] pictures = new int[]{
            R.drawable.spash_screen2,
            R.drawable.fitur_1,
            R.drawable.fitur_2,
            R.drawable.fitur_3,
            R.drawable.fitur_4,
            R.drawable.fitur_5,
            R.drawable.fitur_6,
            R.drawable.fitur_7,
            R.drawable.spash_screen2
    };


    int[] pictures2 = new int[]{
            R.drawable.fitur_1,
            R.drawable.fitur_2,
            R.drawable.fitur_3,
            R.drawable.fitur_4,
            R.drawable.fitur_5,
            R.drawable.fitur_6,
            R.drawable.fitur_7
    };

    int[] icon = new int[]{
            R.drawable.blank,
            R.drawable.icon_laporkan_fasilitas,
            R.drawable.icon_laporkan_pelanggaran,
            R.drawable.icon_beranda_satpam,
            R.drawable.icon_laporan_anda,
            R.drawable.icon_pelapor_tertinggi,
            R.drawable.icon_obrolan_masyarakat,
            R.drawable.ic_loc_informasi2,
            R.drawable.blank
    };

    int[] icon2 = new int[]{
            R.drawable.icon_laporkan_fasilitas,
            R.drawable.icon_laporkan_pelanggaran,
            R.drawable.icon_beranda_satpam,
            R.drawable.icon_laporan_anda,
            R.drawable.icon_pelapor_tertinggi,
            R.drawable.icon_obrolan_masyarakat,
            R.drawable.ic_loc_informasi2
    };

    String[] info = new String[]{
            "Selamat datang di Aplikasi E-Complaint.\n\nIzinkan kami memberitahukan Anda apa fungsi kami\nUntuk melanjutkan, silakan geser layar ke kiri.\nUntuk melihat detail, geser layar ke atas",
            "Laporkan Informasi adalah media untuk masyarakat guna melaporkan suatu informasi yang perlu untuk diketahui oleh publik atau perihal yang erat kaitanya dengan kegiatan kepolisian, seperti melaporkan kondisi lalu lintas, pelanggaran yang dilakukan oleh masyarakat, penipuan oleh seseorang, dan sebagainya.",
            "Laporkan Pelanggaran Polisi adalah media untuk masyarakat guna melaporkan anggota kepolisian Polres Banyumas yang melanggar norma norma atau hukum yang berlaku baik di masyarakat maupun di Polres Banyumas.",
            "Beranda E-Complaint adalah tempat semua laporan dari masyarakat ditampilkan, Anda dapat memberikan dukungan, komentar, atau melaporkan laporan yang tidak sopan untuk masing masing laporan",
            "Laporan Anda adalah tempat semua laporan yang Anda kirimkan melalui aplikasi ini ditampilkan - E-Complaint Polres Banyumas.",
            "Pelapor Tertinggi adalah tempat menampilkan 10 besar masyarakat yang memiliki total laporan tertinggi di tiap bulannya.",
            "Obrolan Untuk Masyarakat adalah tempat masyarakat yang mempunyai pertanyaan seputar kepolisian untuk dapat bertanya langsung melalui obrolan ke admin yang ada.",
            "Peta Lokasi Laporan adalah tempat beberapa laporan ditampilkan dalam sebuah titik di dalam peta yang mewakili lokasi dari laporan tersebut",
            "Terimakasih, sekarang Anda sudah siap !.\n\nSilakan tekan kembali."
    };

    String[] info2 = new String[]{
            "Laporkan Informasi adalah media untuk masyarakat guna melaporkan suatu informasi yang perlu untuk diketahui oleh publik atau perihal yang erat kaitanya dengan kegiatan kepolisian, seperti melaporkan kondisi lalu lintas, pelanggaran yang dilakukan oleh masyarakat, penipuan oleh seseorang, dan sebagainya.",
            "Laporkan Pelanggaran Polisi adalah media untuk masyarakat guna melaporkan anggota kepolisian Polres Banyumas yang melanggar norma norma atau hukum yang berlaku baik di masyarakat maupun di Polres Banyumas.",
            "Beranda E-Complaint adalah tempat semua laporan dari masyarakat ditampilkan, Anda dapat memberikan dukungan, komentar, atau melaporkan laporan yang tidak sopan untuk masing masing laporan",
            "Laporan Anda adalah tempat semua laporan yang Anda kirimkan melalui aplikasi ini ditampilkan - E-Complaint Polres Banyumas.",
            "Pelapor Tertinggi adalah tempat menampilkan 10 besar masyarakat yang memiliki total laporan tertinggi di tiap bulannya.",
            "Obrolan Untuk Masyarakat adalah tempat masyarakat yang mempunyai pertanyaan seputar kepolisian untuk dapat bertanya langsung melalui obrolan ke admin yang ada.",
            "Peta Lokasi Laporan adalah tempat beberapa laporan ditampilkan dalam sebuah titik di dalam peta yang mewakili lokasi dari laporan tersebut"
    };

    String[] judul = new String[]{
            "E-Complaint Polres Banyumas",
            "Laporkan Informasi",
            "Laporkan Pelanggaran Polisi",
            "Beranda E-Complaint",
            "Laporan Anda",
            "Pelapor Tertinggi",
            "Obrolan Untuk Masyarakat",
            "Peta Lokasi Laporan",
            "Terimakasih"
    };

    String[] judul2 = new String[]{
            "Laporkan Informasi",
            "Laporkan Pelanggaran Polisi",
            "Beranda E-Complaint",
            "Laporan Anda",
            "Pelapor Tertinggi",
            "Obrolan Untuk Masyarakat",
            "Peta Lokasi Laporan"
    };

    int[] detail = new int[]{
            R.string.halaman_utama,
            R.string.laporan_fasilitas,
            R.string.laporan_pelanggaran,
            R.string.beranda_ekomplen,
            R.string.laporan_anda,
            R.string.pelapor_tertinggi,
            R.string.obrolan_masyarakat,
            R.string.peta_lokasi_laporan,
            R.string.terimakasih
    };

    int[] detail2 = new int[]{
            R.string.laporan_fasilitas,
            R.string.laporan_pelanggaran,
            R.string.beranda_ekomplen,
            R.string.laporan_anda,
            R.string.pelapor_tertinggi,
            R.string.obrolan_masyarakat,
            R.string.peta_lokasi_laporan
    };

    private TextView LJudul, LKeterangan, BSelanjutnya, BLewati;
    private ImageView IVIcon;
    private View VGAtas, VGBawah;

    private int width, posisi = 0;
    ViewPager viewPager;
    private ImagePageAdapter adapter;

    private void initViewPagerAndSetAdapter(){
        viewPager = (ViewPager) findViewById(R.id.parallaxSlider);
        adapter = new ImagePageAdapter(this, pictures);
        viewPager.setAdapter(adapter);

        addPageChangeListenerIfSDKAbove11();
    }

    private void addPageChangeListenerIfSDKAbove11() {
        if (Build.VERSION.SDK_INT >11) {
            viewPager.setOnPageChangeListener(this);
        }
    }

    private void calculateWidth() {
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        viewPager.getWidth();

        if (Build.VERSION.SDK_INT <13) {
            width = display.getWidth();
        } else {
            display.getSize(size);
            width = size.x;
        }
    }

    private void parallaxImages(int position, int positionOffsetPixels) {
        Map<Integer, View> imageViews = adapter.getImageViews();

        for (Map.Entry<Integer, View> entry: imageViews.entrySet()){
            try
            {
                int imagePosition = entry.getKey();
                int correctedPosition = imagePosition - position;
                int displace = -(correctedPosition * width/2)+ (positionOffsetPixels / 2);

                View view = entry.getValue();
                if (Build.VERSION.SDK_INT > Build.VERSION_CODES.GINGERBREAD_MR1)
                {
                    view.setX(displace);
                }
                view.setBackgroundResource(R.drawable.bg_fitur);
            }
            catch (Exception e)
            {
            }
        }
    }

    boolean firstrun;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fitur);

        LJudul = (TextView) findViewById(R.id.LJudul);
        LKeterangan = (TextView) findViewById(R.id.LKeterangan);
        BLewati = (TextView) findViewById(R.id.BLewati);
        BSelanjutnya = (TextView) findViewById(R.id.BSelanjutnya);
        IVIcon = (ImageView) findViewById(R.id.IVIcon);
        VGAtas = (View) findViewById(R.id.VGAtas);
        VGBawah = (View) findViewById(R.id.VGBawah);


        sharedPref = getSharedPreferences("File_Pengaturan", Context.MODE_PRIVATE);
        firstrun = sharedPref.getBoolean(getString(R.string.p_f), true);

        if (!firstrun)
        {
            info = info2;
            pictures = pictures2;
            judul = judul2;
            icon = icon2;
            detail = detail2;
        }

        BLewati.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (firstrun)
                {
                    finish();
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(intent);
                }
                else
                {
                    finish();
                }
            }
        });

        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(getString(R.string.p_f), false);
        editor.commit();

        initViewPagerAndSetAdapter();
        calculateWidth();

        LJudul.setText(judul[posisi]);
        LKeterangan.setText(info[posisi]);
        IVIcon.setImageResource(icon[posisi]);

        gestureDetectorCompat = new GestureDetectorCompat(this, new MyGestureListener());

        BSelanjutnya.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (viewPager.getCurrentItem() == pictures.length - 1)
                {
                    if (firstrun)
                    {
                        finish();
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                    }
                    else
                    {
                        finish();
                    }
                }

                viewPager.setCurrentItem(viewPager.getCurrentItem() + 1, true);
            }
        });
    }

    @Override
    public void onBackPressed()
    {
        if (firstrun)
        {
            finish();
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(intent);
        }
        else
        {
            finish();
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event)
    {
        this.gestureDetectorCompat.onTouchEvent(event);
        return super.dispatchTouchEvent(event);
    }

    class MyGestureListener extends GestureDetector.SimpleOnGestureListener
    {
        @Override
        public boolean onFling(MotionEvent event1, MotionEvent event2, float velocityX, float velocityY)
        {
            float y = event1.getY() - event2.getY();
            float x = event1.getX() - event2.getX();

            if(Math.abs(y) > Math.abs(x) && y > 0)
            {
                Intent intent = new Intent(getApplicationContext(), detail_fitur.class);
                intent.putExtra("detail", detail[posisi]);
                intent.putExtra("judul", judul[posisi]);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.slide_down);
            }
            return true;
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        parallaxImages(position, positionOffsetPixels);
    }

    @Override
    public void onPageSelected(int position)
    {
        posisi = position;
    }

    @Override
    public void onPageScrollStateChanged(int state) {
        if (state == 1)
        {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2)
            {
                IVIcon.animate().setDuration(300).alpha(0).setListener(new AnimatorListenerAdapter() {
                    @SuppressLint("NewApi")
                    @Override
                    public void onAnimationEnd(Animator animation)
                    {
                    }
                });

                VGAtas.animate().setDuration(300).alpha(0).setListener(new AnimatorListenerAdapter() {
                    @SuppressLint("NewApi")
                    @Override
                    public void onAnimationEnd(Animator animation)
                    {
                    }
                });

                VGBawah.animate().setDuration(300).alpha(0).setListener(new AnimatorListenerAdapter() {
                    @SuppressLint("NewApi")
                    @Override
                    public void onAnimationEnd(Animator animation)
                    {
                    }
                });

                LJudul.animate().setDuration(300).alpha(0).setListener(new AnimatorListenerAdapter() {
                    @SuppressLint("NewApi")
                    @Override
                    public void onAnimationEnd(Animator animation)
                    {
                    }
                });

                LKeterangan.animate().setDuration(300).alpha(0).setListener(new AnimatorListenerAdapter() {
                    @SuppressLint("NewApi")
                    @Override
                    public void onAnimationEnd(Animator animation)
                    {
                    }
                });
            }
            else
            {
                if (Build.VERSION.SDK_INT > Build.VERSION_CODES.GINGERBREAD_MR1)
                {
                    IVIcon.setAlpha(0.0f);
                    VGAtas.setAlpha(0.0f);
                    VGBawah.setAlpha(0.0f);
                    LJudul.setAlpha(0.0f);
                    LKeterangan.setAlpha(0.0f);
                }
            }
        }
        else if (state == 0)
        {
            LJudul.setText(judul[posisi]);
            LKeterangan.setText(info[posisi]);
            IVIcon.setImageResource(icon[posisi]);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2)
            {
                IVIcon.animate().setDuration(300).alpha(1).setListener(new AnimatorListenerAdapter() {
                    @SuppressLint("NewApi")
                    @Override
                    public void onAnimationEnd(Animator animation)
                    {
                    }
                });

                VGAtas.animate().setDuration(300).alpha(1).setListener(new AnimatorListenerAdapter() {
                    @SuppressLint("NewApi")
                    @Override
                    public void onAnimationEnd(Animator animation)
                    {
                    }
                });

                VGBawah.animate().setDuration(300).alpha(1).setListener(new AnimatorListenerAdapter() {
                    @SuppressLint("NewApi")
                    @Override
                    public void onAnimationEnd(Animator animation)
                    {
                    }
                });

                LJudul.animate().setDuration(300).alpha(1).setListener(new AnimatorListenerAdapter() {
                    @SuppressLint("NewApi")
                    @Override
                    public void onAnimationEnd(Animator animation)
                    {
                    }
                });

                LKeterangan.animate().setDuration(300).alpha(1).setListener(new AnimatorListenerAdapter() {
                    @SuppressLint("NewApi")
                    @Override
                    public void onAnimationEnd(Animator animation)
                    {
                    }
                });
            }
            else
            {
                if (Build.VERSION.SDK_INT > Build.VERSION_CODES.GINGERBREAD_MR1)
                {
                    IVIcon.setAlpha(1.0f);
                    VGAtas.setAlpha(1.0f);
                    VGBawah.setAlpha(1.0f);
                    LJudul.setAlpha(1.0f);
                    LKeterangan.setAlpha(1.0f);
                }
            }
        }
    }
}
