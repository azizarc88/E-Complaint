package com.izarcsoftware.satpam;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.izarcsoftware.satpam.adapter.PenggunaListAdapter;
import com.izarcsoftware.satpam.adapter.TentangItemAdapter;
import com.izarcsoftware.satpam.data.PenggunaItem;
import com.izarcsoftware.satpam.data.TentangItem;

import java.util.ArrayList;
import java.util.List;

public class tentang extends AppCompatActivity
{
    private TextView LVersi, LNama, LKebijakanPrivasi;
    private static ListView LVIsian;
    private static TentangItemAdapter listAdapter;
    private static List<TentangItem> tentangItems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tentang);

        LVersi = (TextView) findViewById(R.id.LVersi);
        LNama = (TextView) findViewById(R.id.LNama);
        LKebijakanPrivasi = (TextView) findViewById(R.id.LKebijakanPrivasi);
        LVIsian = (ListView) findViewById(R.id.LVIsian);

        String version = BuildConfig.VERSION_NAME + " (" + BuildConfig.VERSION_CODE + ")";
        LVersi.setText("Versi " + version);

        tentangItems = new ArrayList<TentangItem>();
        tentangItems.add(new TentangItem("Dibuat dan dikembangkan oleh\nAziz Nur Ariffianto", R.mipmap.ic_dibuatoleh));
        tentangItems.add(new TentangItem("Didukung oleh\nAmPu Studio - Rujianto Eko Saputro", R.mipmap.ic_dukungan));
        tentangItems.add(new TentangItem("STMIK AMIKOM PURWOKERTO", R.mipmap.ic_amikom));
        tentangItems.add(new TentangItem("POLRES BANYUMAS", R.mipmap.ic_polres));
        tentangItems.add(new TentangItem("Bagikan dengan teman", R.mipmap.ic_bagikan));
        tentangItems.add(new TentangItem("Apa yang baru ?", R.mipmap.ic_new));
        tentangItems.add(new TentangItem("Suka", R.mipmap.ic_like));
        listAdapter = new TentangItemAdapter(tentangItems, this);
        LVIsian.setAdapter(listAdapter);

        LVIsian.setOnItemClickListener(new ListView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                if (position == 0)
                {
                    goToUrl("https://www.facebook.com/dilittlesnoopy");
                }
                else if (position == 1)
                {
                    goToUrl("https://www.facebook.com/AmPuStudio");
                }
                else if (position == 2)
                {
                    goToUrl("http://amikompurwokerto.ac.id");
                }
                else if (position == 3)
                {
                    goToUrl("http://polresbanyumas.net/");
                }
                else if (position == 4)
                {
                    goToUrl("https://m.facebook.com/sharer.php?u=https%3A%2F%2Fplay.google.com%2Fstore%2Fapps%2Fdetails%3Fid%3Dcom.izarcsoftware.satpam&sid=0&referrer=social_plugin&_rdr");
                }
                else if (position == 5)
                {
                    Intent intent = new Intent(getApplicationContext(), apayangbaru.class);
                    startActivity(intent);
                }
                else if (position == 6)
                {
                    final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                    try {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                    } catch (android.content.ActivityNotFoundException anfe) {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                    }
                }
            }
        });
    }

    private void goToUrl (String url) {
        Uri uriUrl = Uri.parse(url);
        Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
        startActivity(launchBrowser);
    }
}
