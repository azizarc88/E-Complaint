package com.izarcsoftware.satpam.data;

/**
 * Created by Aziz Nur Ariffianto on 0011, 11 Feb 2016.
 */
public class ChatItem
{
    private String obid, dari, untuk, isipesan, waktu;
    private boolean pending = false;

    public ChatItem()
    {
    }

    public ChatItem(String obid, String dari, String untuk, String isipesan, String waktu, boolean pending) {
        this.obid = obid;
        this.dari = dari;
        this.untuk = untuk;
        this.isipesan = isipesan;
        this.waktu = waktu;
        this.pending = pending;
    }

    public boolean isPending() {
        return pending;
    }

    public void setPending(boolean pending) {
        this.pending = pending;
    }

    public String getObid()
    {
        return obid;
    }

    public void setObid(String obid)
    {
        this.obid = obid;
    }

    public String getDari()
    {
        return dari;
    }

    public void setDari(String dari)
    {
        this.dari = dari;
    }

    public String getUntuk()
    {
        return untuk;
    }

    public void setUntuk(String untuk)
    {
        this.untuk = untuk;
    }

    public String getIsipesan()
    {
        return isipesan;
    }

    public void setIsipesan(String isipesan)
    {
        this.isipesan = isipesan;
    }

    public String getWaktu()
    {
        return waktu;
    }

    public void setWaktu(String waktu)
    {
        this.waktu = waktu;
    }
}
