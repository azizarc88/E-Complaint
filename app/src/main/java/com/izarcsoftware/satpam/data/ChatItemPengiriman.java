package com.izarcsoftware.satpam.data;

/**
 * Created by Aziz Nur Ariffianto on 0011, 11 Feb 2016.
 */
public class ChatItemPengiriman
{
    private String dari, untuk, isipesan, waktu;
    private String posisi;

    public ChatItemPengiriman()
    {
    }

    public ChatItemPengiriman(String dari, String untuk, String isipesan, String waktu, String posisi) {
        this.dari = dari;
        this.untuk = untuk;
        this.isipesan = isipesan;
        this.waktu = waktu;
        this.posisi = posisi;
    }

    public void setPosisi(String posisi) {
        this.posisi = posisi;
    }

    public String getPosisi() {
        return posisi;
    }

    public String getDari()
    {
        return dari;
    }

    public void setDari(String dari)
    {
        this.dari = dari;
    }

    public String getUntuk()
    {
        return untuk;
    }

    public void setUntuk(String untuk)
    {
        this.untuk = untuk;
    }

    public String getIsipesan()
    {
        return isipesan;
    }

    public void setIsipesan(String isipesan)
    {
        this.isipesan = isipesan;
    }

    public String getWaktu()
    {
        return waktu;
    }

    public void setWaktu(String waktu)
    {
        this.waktu = waktu;
    }
}
