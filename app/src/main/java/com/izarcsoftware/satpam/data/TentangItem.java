package com.izarcsoftware.satpam.data;

import android.graphics.drawable.Drawable;

/**
 * Created by Ampu2 on 07/04/2016.
 */
public class TentangItem
{
    String isi;
    int icon;

    public TentangItem() {
    }

    public TentangItem(String isi, int icon) {
        this.isi = isi;
        this.icon = icon;
    }

    public String getIsi() {
        return isi;
    }

    public void setIsi(String isi) {
        this.isi = isi;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }
}
