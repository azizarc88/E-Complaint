package com.izarcsoftware.satpam.data;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by Ampu2 on 03/08/2016.
 */
public class MarkerData
{
    String rlid, lat, ling, lfid, nama, waktu, prioritas, dukungan, komentar, status, urlfoto, alamat, oleh, didukung;

    public MarkerData() {
    }

    public MarkerData(String rlid, String lat, String ling, String lfid, String nama, String waktu, String prioritas, String dukungan, String komentar, String status, String urlfoto, String alamat, String oleh) {
        this.rlid = rlid;
        this.lat = lat;
        this.ling = ling;
        this.lfid = lfid;
        this.nama = nama;
        this.waktu = waktu;
        this.prioritas = prioritas;
        this.dukungan = dukungan;
        this.komentar = komentar;
        this.status = status;
        this.urlfoto = urlfoto;
        this.alamat = alamat;
        this.oleh = oleh;
    }

    public String getDidukung() {
        return didukung;
    }

    public void setDidukung(String didukung) {
        this.didukung = didukung;
    }

    public String getRlid() {
        return rlid;
    }

    public void setRlid(String rlid) {
        this.rlid = rlid;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLing() {
        return ling;
    }

    public void setLing(String ling) {
        this.ling = ling;
    }

    public String getLfid() {
        return lfid;
    }

    public void setLfid(String lfid) {
        this.lfid = lfid;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getWaktu() {
        return waktu;
    }

    public void setWaktu(String waktu) {
        this.waktu = waktu;
    }

    public String getPrioritas() {
        return prioritas;
    }

    public void setPrioritas(String prioritas) {
        this.prioritas = prioritas;
    }

    public String getDukungan() {
        return dukungan;
    }

    public void setDukungan(String dukungan) {
        this.dukungan = dukungan;
    }

    public String getKomentar() {
        return komentar;
    }

    public void setKomentar(String komentar) {
        this.komentar = komentar;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUrlfoto() {
        return urlfoto;
    }

    public void setUrlfoto(String urlfoto) {
        this.urlfoto = urlfoto;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getOleh() {
        return oleh;
    }

    public void setOleh(String oleh) {
        this.oleh = oleh;
    }
}
