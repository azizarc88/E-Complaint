package com.izarcsoftware.satpam.data;

/**
 * Created by Aziz Nur Ariffianto on 0007, 07 Feb 2016.
 */
public class KomentarItem
{
    private int kmid;
    private String nama;
    private String komentar;
    private String waktu;
    private String lfid;
    private String komentarku;
    private String sebagai;

    public String getBagian()
    {
        return bagian;
    }

    public void setBagian(String bagian)
    {
        this.bagian = bagian;
    }

    private String bagian;

    public KomentarItem(int kmid, String nama, String komentar, String waktu, String lfid, String komentarku, String sebagai, String bagian)
    {
        this.kmid = kmid;
        this.nama = nama;
        this.komentar = komentar;
        this.waktu = waktu;
        this.lfid = lfid;
        this.komentarku = komentarku;
        this.sebagai = sebagai;
        this.bagian = bagian;
    }

    public String getSebagai()
    {
        return sebagai;
    }

    public void setSebagai(String sebagai)
    {
        this.sebagai = sebagai;
    }

    public String getKomentarku()
    {
        return komentarku;
    }

    public void setKomentarku(String komentarku)
    {
        this.komentarku = komentarku;
    }

    public KomentarItem(){}

    public int getKmid()
    {
        return kmid;
    }

    public void setKmid(int kmid)
    {
        this.kmid = kmid;
    }

    public String getNama()
    {
        return nama;
    }

    public void setNama(String nama)
    {
        this.nama = nama;
    }

    public String getKomentar()
    {
        return komentar;
    }

    public void setKomentar(String komentar)
    {
        this.komentar = komentar;
    }

    public String getWaktu()
    {
        return waktu;
    }

    public void setWaktu(String waktu)
    {
        this.waktu = waktu;
    }

    public String getLfid()
    {
        return lfid;
    }

    public void setLfid(String lfid)
    {
        this.lfid = lfid;
    }
}
