package com.izarcsoftware.satpam.data;

/**
 * Created by Aziz Nur Ariffianto on 0017, 17 Feb 2016.
 */
public class PemberitahuanItem
{
    private String isi, tipe, pbid, id, nama;
    private int nomor;

    public PemberitahuanItem()
    {
    }

    public PemberitahuanItem(String isi, String tipe, String pbid, String id)
    {
        this.isi = isi;
        this.tipe = tipe;
        this.pbid = pbid;
        this.id = id;
    }

    public String getNama()
    {
        return nama;
    }

    public void setNama(String nama)
    {
        this.nama = nama;
    }

    public int getNomor()
    {
        return nomor;
    }

    public void setNomor(int nomor)
    {
        this.nomor = nomor;
    }

    public String getIsi()
    {
        return isi;
    }

    public void setIsi(String isi)
    {
        this.isi = isi;
    }

    public String getTipe()
    {
        return tipe;
    }

    public void setTipe(String tipe)
    {
        this.tipe = tipe;
    }

    public String getPbid()
    {
        return pbid;
    }

    public void setPbid(String pbid)
    {
        this.pbid = pbid;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }
}
