package com.izarcsoftware.satpam.data;

/**
 * Created by Aziz Nur Ariffianto on 0011, 11 Feb 2016.
 */
public class PemerintahItem
{
    private String pgid, sebagai, nama;
    private int nomor;

    public PemerintahItem()
    {
    }

    public PemerintahItem(String pgid, String sebagai, String nama, int nomor)
    {
        this.pgid = pgid;
        this.sebagai = sebagai;
        this.nama = nama;
        this.nomor = nomor;
    }

    public int getNomor()
    {
        return nomor;
    }

    public void setNomor(int nomor)
    {
        this.nomor = nomor;
    }

    public String getPgid()
    {
        return pgid;
    }

    public void setPgid(String pgid)
    {
        this.pgid = pgid;
    }

    public String getSebagai()
    {
        return sebagai;
    }

    public void setSebagai(String sebagai)
    {
        this.sebagai = sebagai;
    }

    public String getNama()
    {
        return nama;
    }

    public void setNama(String nama)
    {
        this.nama = nama;
    }
}
