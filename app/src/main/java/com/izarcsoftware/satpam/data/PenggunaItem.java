package com.izarcsoftware.satpam.data;

/**
 * Created by Aziz Nur Ariffianto on 0010, 10 Feb 2016.
 */
public class PenggunaItem
{
    private String info, pgid, nama, desa, kota, totallaporan;
    private int nomor;

    public PenggunaItem()
    {

    }

    public PenggunaItem(String info, String pgid, String nama, String desa, String kota, String totallaporan)
    {
        this.pgid = pgid;
        this.nama = nama;
        this.desa = desa;
        this.kota = kota;
        this.totallaporan = totallaporan;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public int getNomor()
    {
        return nomor;
    }

    public void setNomor(int nomor)
    {
        this.nomor = nomor;
    }

    public String getPgid()
    {
        return pgid;
    }

    public void setPgid(String pgid)
    {
        this.pgid = pgid;
    }

    public String getNama()
    {
        return nama;
    }

    public void setNama(String nama)
    {
        this.nama = nama;
    }

    public String getDesa()
    {
        return desa;
    }

    public void setDesa(String desa)
    {
        this.desa = desa;
    }

    public String getKota()
    {
        return kota;
    }

    public void setKota(String kota)
    {
        this.kota = kota;
    }

    public String getTotallaporan()
    {
        return totallaporan;
    }

    public void setTotallaporan(String totallaporan)
    {
        this.totallaporan = totallaporan;
    }
}
