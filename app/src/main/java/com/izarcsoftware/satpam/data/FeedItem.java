package com.izarcsoftware.satpam.data;

/**
 * Created by Aziz Nur Ariffianto on 0005, 05 Feb 2016.
 */
public class FeedItem {
    private int id;
    private String name;
    private String status;
    private String image;
    private String timeStamp;
    private String lat;
    private String ling;
    private String dukungan;
    private String komentar;
    private String prioritas;
    private String sudahlike;
    private String bagian;
    private String oleh;
    private String sebagai;

    public FeedItem(int id, String name, String status, String timeStamp, String lat, String ling, String dukungan, String komentar, String prioritas, String sudahlike, String oleh, String individual, String alamat, String bagian, String sebagai)
    {
        this.id = id;
        this.name = name;
        this.status = status;
        this.timeStamp = timeStamp;
        this.lat = lat;
        this.ling = ling;
        this.dukungan = dukungan;
        this.komentar = komentar;
        this.prioritas = prioritas;
        this.sudahlike = sudahlike;
        this.oleh = oleh;
        this.individual = individual;
        this.alamat = alamat;
        this.bagian = bagian;
        this.sebagai = sebagai;
    }

    public String getSebagai() {
        return sebagai;
    }

    public void setSebagai(String sebagai) {
        this.sebagai = sebagai;
    }

    public String getIndividual()
    {
        return individual;
    }

    public void setIndividual(String individual)
    {
        this.individual = individual;
    }

    private String individual;

    public String getAlamat()
    {
        return alamat;
    }

    public void setAlamat(String alamat)
    {
        this.alamat = alamat;
    }

    private String alamat;

    public String getBagian()
    {
        return bagian;
    }

    public void setBagian(String bagian)
    {
        this.bagian = bagian;
    }

    public FeedItem() {
    }

    public String getOleh()
    {
        return oleh;
    }

    public void setOleh(String oleh)
    {
        this.oleh = oleh;
    }

    public String getLat()
    {
        return lat;
    }

    public void setLat(String lat)
    {
        this.lat = lat;
    }

    public String getLing()
    {
        return ling;
    }

    public void setLing(String ling)
    {
        this.ling = ling;
    }

    public String getDukungan()
    {
        return dukungan;
    }

    public void setDukungan(String dukungan)
    {
        this.dukungan = dukungan;
    }

    public String getKomentar()
    {
        return komentar;
    }

    public void setKomentar(String komentar)
    {
        this.komentar = komentar;
    }

    public String getPrioritas()
    {
        return prioritas;
    }

    public void setPrioritas(String prioritas)
    {
        this.prioritas = prioritas;
    }

    public String getSudahlike()
    {
        return sudahlike;
    }

    public void setSudahlike(String sudahlike)
    {
        this.sudahlike = sudahlike;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImge() {
        return image;
    }

    public void setImge(String image) {
        this.image = image;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }
}
