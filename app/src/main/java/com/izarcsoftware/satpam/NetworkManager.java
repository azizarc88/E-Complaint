package com.izarcsoftware.satpam;

import com.android.volley.Cache;
import com.android.volley.NetworkResponse;
import com.android.volley.toolbox.HttpHeaderParser;

import java.util.Map;

/**
 * Created by Aziz Nur Ariffianto on 0008, 08 Feb 2016.
 */
public class NetworkManager
{
    public static Cache.Entry parseIgnoreCacheHeaders(String response) {
        long now = System.currentTimeMillis();

        Map<String, String> headers = null;
        long serverDate = 0;
        String serverEtag = null;

        final long cacheHitButRefreshed = 3 * 60 * 1000; // in 3 minutes cache will be hit, but also refreshed on background
        final long cacheExpired = 7 * 24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
        final long softExpire = now + cacheHitButRefreshed;
        final long ttl = now + cacheExpired;

        Cache.Entry entry = new Cache.Entry();
        entry.data = response.getBytes();
        entry.etag = serverEtag;
        entry.softTtl = softExpire;
        entry.ttl = ttl;
        entry.serverDate = serverDate;
        entry.responseHeaders = headers;

        return entry;
    }
}
