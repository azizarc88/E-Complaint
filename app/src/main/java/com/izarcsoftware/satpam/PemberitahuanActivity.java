package com.izarcsoftware.satpam;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.LayoutAnimationController;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.izarcsoftware.satpam.adapter.PemberitahuanListAdapter;
import com.izarcsoftware.satpam.app.AppController;
import com.izarcsoftware.satpam.data.PemberitahuanItem;
import com.izarcsoftware.satpam.data.PenggunaItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class PemberitahuanActivity extends AppCompatActivity
{
    private Toolbar toolbar;
    private static String URL_FEED;
    private static ListView LVPemberitahuan;
    private static TextView LInfo;
    private static PemberitahuanListAdapter listAdapter;
    private static List<PemberitahuanItem> pemberitahuanItems;
    private static String id;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pemberitahuan);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Pemberitahuan");
        toolbar.setNavigationOnClickListener(new Toolbar.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                finish();
            }
        });

        LVPemberitahuan = (ListView) findViewById(R.id.LVPemberitahuan);
        LInfo = (TextView) findViewById(R.id.LInfo);

        LVPemberitahuan.setOnItemClickListener(new ListView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                String tipe = pemberitahuanItems.get(position).getTipe();
                if (tipe.equalsIgnoreCase("Komentar Fasilitas"))
                {
                    Intent intent = new Intent(getApplicationContext(), Komentar.class);
                    String[] data = {pemberitahuanItems.get(position).getId(), "fas"};
                    intent.putExtra("data", data);
                    intent.putExtra("daripemberitahuan", true);
                    startActivity(intent);
                    new SudahBaca(getApplicationContext()).execute(pemberitahuanItems.get(position).getPbid());
                    finish();
                }
                else if (tipe.equalsIgnoreCase("Komentar Pelanggaran"))
                {
                    Intent intent = new Intent(getApplicationContext(), Komentar.class);
                    String[] data = {pemberitahuanItems.get(position).getId(), "plgr"};
                    intent.putExtra("data", data);
                    intent.putExtra("daripemberitahuan", true);
                    startActivity(intent);
                    new SudahBaca(getApplicationContext()).execute(pemberitahuanItems.get(position).getPbid());
                    finish();
                }
                else if (tipe.equalsIgnoreCase("Obrolan"))
                {
                    Intent intent = new Intent(getApplicationContext(), RuangObrolanActivity.class);
                    String[] data = {pemberitahuanItems.get(position).getId(), pemberitahuanItems.get(position).getNama()};
                    intent.putExtra("data", data);
                    startActivity(intent);
                    new SudahBaca(getApplicationContext()).execute(pemberitahuanItems.get(position).getPbid());
                    finish();
                }
            }
        });

        SharedPreferences sharedPref;
        sharedPref = this.getSharedPreferences("File_Pengaturan", Context.MODE_PRIVATE);
        id = sharedPref.getString(getString(R.string.p_id_pengguna), "Tidak Ada");

        URL_FEED = getString(R.string.alamat_server) + getString(R.string.link_ambil_pemberitahuan) + "?pgid=" + id;

        AnimationSet set = new AnimationSet(true);

        Animation animation = new AlphaAnimation(0.0f, 1.0f);
        animation.setDuration(800);
        set.addAnimation(animation);

        animation = new TranslateAnimation(
                Animation.RELATIVE_TO_SELF, 0.0f,Animation.RELATIVE_TO_SELF, 0.0f,
                Animation.RELATIVE_TO_SELF, -1.0f, Animation.RELATIVE_TO_SELF, 0.0f
        );
        animation.setDuration(300);
        set.addAnimation(animation);

        LayoutAnimationController controller = new LayoutAnimationController(set, 0.5f);
        LVPemberitahuan.setLayoutAnimation(controller);

        pemberitahuanItems = new ArrayList<PemberitahuanItem>();
        listAdapter = new PemberitahuanListAdapter(this, pemberitahuanItems);
        LVPemberitahuan.setAdapter(listAdapter);

        JsonObjectRequest jsonReq = new JsonObjectRequest(Request.Method.GET, URL_FEED, null, new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response) {

                if (response != null) {
                    pemberitahuanItems.clear();
                    parseJson(response);
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Snackbar.make(LVPemberitahuan, "Terjadi kesalahan, periksa koneksi.", Snackbar.LENGTH_LONG).show();
            }
        });

        AppController.getInstance().addToRequestQueue(jsonReq);
    }

    private void parseJson(JSONObject response)
    {
        try
        {
            JSONArray feedArray = response.getJSONArray("pemberitahuan");

            for (int i = 0; i < feedArray.length(); i++)
            {
                JSONObject feedObj = (JSONObject) feedArray.get(i);

                PemberitahuanItem item = new PemberitahuanItem();
                item.setNomor(i + 1);
                item.setNama(feedObj.getString("nama"));
                item.setTipe(feedObj.getString("tipe"));
                item.setPbid(feedObj.getString("pbid"));
                item.setId(feedObj.getString("id"));
                item.setIsi(feedObj.getString("isi"));

                pemberitahuanItems.add(item);
            }

            LInfo.setVisibility(View.GONE);
            LVPemberitahuan.setVisibility(View.VISIBLE);

            listAdapter.notifyDataSetChanged();
        }
        catch (Exception e)
        {
            LInfo.setVisibility(View.VISIBLE);
            LVPemberitahuan.setVisibility(View.GONE);
        }
    }

    class SudahBaca extends AsyncTask<String, Void, String>
    {
        private Context context;

        public SudahBaca(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... arg0)
        {
            RequestHandler rh = new RequestHandler();

            String link;
            String result;

            try
            {
                HashMap<String,String> data = new HashMap<>();
                data.put("pbid", arg0[0]);

                link = getString(R.string.alamat_server) + getString(R.string.link_sudahdibaca);

                result = rh.sendPostRequest(link, data);

                return result;
            } catch (Exception e)
            {
                return new String("Exception: " + e.getMessage());
            }
        }

        @Override
        protected void onPostExecute(String result)
        {
            String jsonStr = result;
            if (jsonStr != null)
            {
                try
                {
                    JSONObject jsonObj = new JSONObject(jsonStr);
                    String query_result = jsonObj.getString("hasil");
                    if (query_result.equals("SUKSES"))
                    {
                    } else if (query_result.equals("GAGAL"))
                    {
                        Snackbar.make(LVPemberitahuan, "Terjadi kesalahan.", Snackbar.LENGTH_LONG).show();
                    } else
                    {
                        Snackbar.make(LVPemberitahuan, "Tidak dapat terhubung ke database.", Snackbar.LENGTH_LONG).show();
                    }
                } catch (JSONException e)
                {
                    e.printStackTrace();
                    Toast.makeText(context, "Tidak dapat terhubung ke database, periksa koneksi internet kamu. " + jsonStr.toString(), Toast.LENGTH_LONG).show();
                }
            } else
            {
                Snackbar.make(LVPemberitahuan, "Tidak dapat mengambil data JSON.", Snackbar.LENGTH_LONG).show();
            }
        }
    }

    class SudahBacaSemua extends AsyncTask<String, Void, String>
    {
        private Context context;

        public SudahBacaSemua(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... arg0)
        {
            RequestHandler rh = new RequestHandler();

            String link;
            String result;

            try
            {
                HashMap<String,String> data = new HashMap<>();
                data.put("untuk", arg0[0]);

                link = getString(R.string.alamat_server) + getString(R.string.link_sudahdibacasemua);

                result = rh.sendPostRequest(link, data);

                return result;
            } catch (Exception e)
            {
                return new String("Exception: " + e.getMessage());
            }
        }

        @Override
        protected void onPostExecute(String result)
        {
            String jsonStr = result;
            if (jsonStr != null)
            {
                try
                {
                    JSONObject jsonObj = new JSONObject(jsonStr);
                    String query_result = jsonObj.getString("hasil");
                    if (query_result.equals("SUKSES"))
                    {
                        JsonObjectRequest jsonReq = new JsonObjectRequest(Request.Method.GET, URL_FEED, null, new Response.Listener<JSONObject>()
                        {
                            @Override
                            public void onResponse(JSONObject response) {

                                if (response != null) {
                                    pemberitahuanItems.clear();
                                    parseJson(response);
                                }
                            }
                        }, new Response.ErrorListener() {

                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Snackbar.make(LVPemberitahuan, "Terjadi kesalahan, periksa koneksi.", Snackbar.LENGTH_LONG).show();
                            }
                        });

                        AppController.getInstance().addToRequestQueue(jsonReq);
                    } else if (query_result.equals("GAGAL"))
                    {
                        Snackbar.make(LVPemberitahuan, "Terjadi kesalahan.", Snackbar.LENGTH_LONG).show();
                    } else
                    {
                        Snackbar.make(LVPemberitahuan, "Tidak dapat terhubung ke database.", Snackbar.LENGTH_LONG).show();
                    }
                } catch (JSONException e)
                {
                    e.printStackTrace();
                    Toast.makeText(context, "Tidak dapat terhubung ke database, periksa koneksi internet kamu. " + jsonStr.toString(), Toast.LENGTH_LONG).show();
                }
            } else
            {
                Snackbar.make(LVPemberitahuan, "Tidak dapat mengambil data JSON.", Snackbar.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_pemberitahuan, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();

        if (id == R.id.HapusSemua)
        {
            new SudahBacaSemua(this).execute(String.valueOf(this.id));
        }

        return super.onOptionsItemSelected(item);
    }
}
