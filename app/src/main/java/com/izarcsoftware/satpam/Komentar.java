package com.izarcsoftware.satpam;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.LayoutAnimationController;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.izarcsoftware.satpam.adapter.FeedListAdapter;
import com.izarcsoftware.satpam.adapter.KomentarListAdapter;
import com.izarcsoftware.satpam.app.AppController;
import com.izarcsoftware.satpam.app.ResourcesManager;
import com.izarcsoftware.satpam.app.Satpam;
import com.izarcsoftware.satpam.app.Validation;
import com.izarcsoftware.satpam.data.KomentarItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class Komentar extends AppCompatActivity
{
    public static String[] id;
    private static final String TAG = MainActivity.class.getSimpleName();
    private static TextView LStatus, LLokasi, LStatusKomentar, LKomentar, LDukungan, LNama, LWaktu, LPrioritas;
    private FeedImageView CVGambar;
    private static String URL_FEED, URL_KOMENTAR;
    private static ListView LVKomentar;
    private static LinearLayout LLTeksKomen;
    private ImageView BPilihan;
    private static ScrollView SVIsian;
    private static KomentarListAdapter listAdapter;
    private Button BKirimKomentar, BDukung;
    public static List<KomentarItem> komentarItems;
    public EditText TBKomentar;
    private String nama, waktu, prioritas, dukungan, komentar, status, urlforo, alamat, lfid;
    private String pgid, sebagai;

    public void Tampilkan(final Boolean tampilkan, final Button tombol, String teks, final String indeks, final String bagian)
    {
        tombol.setText(teks);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2)
        {
            tombol.animate().setDuration(100).alpha(!tampilkan ? 0 : 1).setListener(new AnimatorListenerAdapter()
            {
                @Override
                public void onAnimationEnd(Animator animation)
                {
                    tombol.setVisibility(!tampilkan ? View.INVISIBLE : View.VISIBLE);
                    tombol.setEnabled(true);
                }
            });
        }
        else
        {
            tombol.setVisibility(!tampilkan ? View.INVISIBLE : View.VISIBLE);
            tombol.setEnabled(true);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_komentar);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbar.setNavigationOnClickListener(new Toolbar.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                finish();
            }
        });

        KomentarListAdapter.cc = this;

        SharedPreferences sharedPref;
        sharedPref = getApplicationContext().getSharedPreferences("File_Pengaturan", Context.MODE_PRIVATE);
        pgid = sharedPref.getString(getString(R.string.p_id_pengguna), "Tidak Ada");
        sebagai = sharedPref.getString(getString(R.string.p_sebagai), "Tidak Ada");

        Intent intent = getIntent();
        id = intent.getStringArrayExtra(FeedListAdapter.ID);
        if (id == null)
        {
            id = intent.getStringArrayExtra("data");
        }

        nama = intent.getStringExtra("nama");
        waktu = intent.getStringExtra("waktu");
        prioritas = intent.getStringExtra("prioritas");
        dukungan = intent.getStringExtra("dukungan");
        komentar = intent.getStringExtra("komentar");
        status = intent.getStringExtra("status");
        urlforo = intent.getStringExtra("urlforo");
        alamat = intent.getStringExtra("alamat");
        lfid = intent.getStringExtra("lfid");
        lat = intent.getStringExtra("lat");
        ling = intent.getStringExtra("ling");
        dilaporkanoleh = intent.getStringExtra("oleh");
        daripemberitahuan = intent.getBooleanExtra("daripemberitahuan", true);
        didukung = intent.getBooleanExtra("didukung", false);

        LStatus = (TextView) findViewById(R.id.txtStatusMsg);
        LLokasi = (TextView) findViewById(R.id.LLokasi);
        LKomentar = (TextView) findViewById(R.id.LKomentar);
        LStatusKomentar = (TextView) findViewById(R.id.LStatusKomentar);
        LDukungan = (TextView) findViewById(R.id.LDukungan);
        LNama = (TextView) findViewById(R.id.LNama);
        LWaktu = (TextView) findViewById(R.id.LWaktu);
        LPrioritas = (TextView) findViewById(R.id.LPrioritas);
        CVGambar = (FeedImageView) findViewById(R.id.feedImage1);
        BKirimKomentar = (Button) findViewById(R.id.BKirimKomen);
        BDukung = (Button) findViewById(R.id.BDukung);
        TBKomentar = (EditText) findViewById(R.id.TBKomentar);
        LLTeksKomen = (LinearLayout) findViewById(R.id.LLTeksKomen);
        SVIsian = (ScrollView) findViewById(R.id.SVIsian);
        BPilihan = (ImageView) findViewById(R.id.BPilihan);

        ResourcesManager.ScaleIcon(getApplicationContext(), BDukung, R.drawable.icon_dukung);
        ResourcesManager.ScaleIcon(getApplicationContext(), LDukungan, R.drawable.icon_dukung);
        ResourcesManager.ScaleIcon(getApplicationContext(), LKomentar, R.drawable.icon_komentar);

        BKirimKomentar.requestFocus();

        if (id[1].equalsIgnoreCase("fas"))
        {
            URL_KOMENTAR = getString(R.string.alamat_server) + getString(R.string.link_ambil_komen_fasilitas) + "?lfid=" + id[0] + "&oleh=" + pgid;
            URL_FEED = getString(R.string.alamat_server) + getString(R.string.link_feed_laporanfas) + "?oleh=" + pgid + "&lfid=" + id[0];
        }
        else
        {
            URL_KOMENTAR = getString(R.string.alamat_server) + getString(R.string.link_ambil_komen_pelanggaran) + "?lpid=" + id[0] + "&oleh=" + pgid;
            URL_FEED = getString(R.string.alamat_server) + getString(R.string.link_feed_laporanplgr) + "?oleh=" + pgid + "&lpid=" + id[0];
        }

        LVKomentar = (ListView) findViewById(R.id.LVKomentar);

        LVKomentar.setOnTouchListener(new View.OnTouchListener() {
            // Setting on Touch Listener for handling the touch inside ScrollView
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // Disallow the touch request for parent scroll on touch of child view
                v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });

        class LaporkanManager extends AsyncTask<String, Void, String>
        {
            private Context context;

            public LaporkanManager(Context context) {
                this.context = context;
            }

            @Override
            protected void onPreExecute()
            {
                super.onPreExecute();
            }

            @Override
            protected String doInBackground(String... arg0)
            {
                RequestHandler rh = new RequestHandler();

                String link;
                String result;

                try
                {
                    HashMap<String,String> data = new HashMap<>();
                    data.put("waktu", arg0[1]);
                    data.put("oleh", arg0[2]);

                    if (arg0[3].equalsIgnoreCase("fas"))
                    {
                        data.put("lfid", arg0[0]);
                        link = getString(R.string.alamat_server) + getString(R.string.link_lapor_lapor_fasilitas);
                    }
                    else
                    {
                        data.put("lpid", arg0[0]);
                        link = getString(R.string.alamat_server) + getString(R.string.link_lapor_lapor_pelanggaran);
                    }

                    result = rh.sendPostRequest(link, data);

                    return result;
                } catch (Exception e)
                {
                    return new String("Exception: " + e.getMessage());
                }
            }

            @Override
            protected void onPostExecute(String result)
            {
                String jsonStr = result;
                if (jsonStr != null)
                {
                    try
                    {
                        JSONObject jsonObj = new JSONObject(jsonStr);
                        String query_result = jsonObj.getString("hasil");
                        if (query_result.equals("SUKSES"))
                        {
                            Snackbar.make(BDukung, "Terima kasih telah melaporkan laporan ini, kami akan menindak lanjuti itu.", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                        } else if (query_result.equals("SUDAH ADA"))
                        {
                            Snackbar.make(BDukung, "Anda sudah pernah melaporkan laporan ini.", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                        } else if (query_result.equals("GAGAL"))
                        {
                            Snackbar.make(BDukung, "Terjadi kesalahan, silakan coba lagi.", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                        } else
                        {
                            Snackbar.make(BDukung, "Tidak dapat terhubung ke database.", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                        }
                    } catch (JSONException e)
                    {
                        e.printStackTrace();
                        Toast.makeText(getApplicationContext(), "Tidak dapat terhubung ke database, periksa koneksi internet kamu. " + jsonStr.toString(), Toast.LENGTH_LONG).show();
                    }
                } else
                {
                    Snackbar.make(BDukung, "Tidak dapat mengambil data JSON.", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                }
            }
        }

        class HapusLaporanManager extends AsyncTask<String, Void, String>
        {
            private Context context;

            public HapusLaporanManager(Context context) {
                this.context = context;
            }

            @Override
            protected void onPreExecute()
            {
                super.onPreExecute();
            }

            @Override
            protected String doInBackground(String... arg0)
            {
                RequestHandler rh = new RequestHandler();

                String link;
                String result;

                try
                {
                    HashMap<String,String> data = new HashMap<>();

                    if (id[1].equalsIgnoreCase("fas"))
                    {
                        data.put("tabel", "laporanfasilitas");
                        data.put("namaid", "lfid");
                    }
                    else
                    {
                        data.put("tabel", "laporanpelanggaran");
                        data.put("namaid", "lpid");
                    }

                    data.put("id", arg0[0]);
                    link = getString(R.string.alamat_server) + getString(R.string.link_hapus_data);

                    result = rh.sendPostRequest(link, data);

                    return result;
                } catch (Exception e)
                {
                    return new String("Exception: " + e.getMessage());
                }
            }

            @Override
            protected void onPostExecute(String result)
            {
                String jsonStr = result;
                if (jsonStr != null)
                {
                    try
                    {
                        JSONObject jsonObj = new JSONObject(jsonStr);
                        String query_result = jsonObj.getString("hasil");
                        if (query_result.equals("SUKSES"))
                        {
                            Snackbar.make(BDukung, "Laporan yang Anda pilih sudah dihapus", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                            if (id[1].equalsIgnoreCase("fas"))
                            {
                                FragmentFasilitas.listAdapter.notifyDataSetChanged();
                                finish();
                            }
                            else
                            {
                                FragmentPelanggaran.listAdapter.notifyDataSetChanged();
                                finish();
                            }
                        } else if (query_result.equals("GAGAL"))
                        {
                            Snackbar.make(BDukung, "Terjadi kesalahan, silahkan coba lagi", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                        } else
                        {
                            Snackbar.make(BDukung, "Tidak dapat terhubung ke database.", Snackbar.LENGTH_LONG).show();
                        }
                    } catch (JSONException e)
                    {
                        e.printStackTrace();
                        Toast.makeText(context, "Tidak dapat terhubung ke database, periksa koneksi internet kamu. " + jsonStr.toString(), Toast.LENGTH_LONG).show();
                    }
                } else
                {
                    Snackbar.make(BDukung, "Tidak dapat mengambil data JSON.", Snackbar.LENGTH_LONG).show();
                }
            }
        }

        class SembunyikanLaporanManager extends AsyncTask<String, Void, String>
        {
            private Context context;

            public SembunyikanLaporanManager(Context context) {
                this.context = context;
            }

            @Override
            protected void onPreExecute()
            {
                super.onPreExecute();
            }

            @Override
            protected String doInBackground(String... arg0)
            {
                RequestHandler rh = new RequestHandler();

                String link;
                String result;

                try
                {
                    HashMap<String,String> data = new HashMap<>();

                    if (id[1].equalsIgnoreCase("fas"))
                    {
                        data.put("tabel", "laporanfasilitas");
                        data.put("namaid", "lfid");
                    }
                    else
                    {
                        data.put("tabel", "laporanpelanggaran");
                        data.put("namaid", "lpid");
                    }

                    data.put("id", arg0[0]);
                    link = getString(R.string.alamat_server) + getString(R.string.link_sembunyikan_laporan);

                    result = rh.sendPostRequest(link, data);

                    return result;
                } catch (Exception e)
                {
                    return new String("Exception: " + e.getMessage());
                }
            }

            @Override
            protected void onPostExecute(String result)
            {
                String jsonStr = result;
                if (jsonStr != null)
                {
                    try
                    {
                        JSONObject jsonObj = new JSONObject(jsonStr);
                        String query_result = jsonObj.getString("hasil");
                        if (query_result.equals("SUKSES"))
                        {
                            Snackbar.make(BDukung, "Laporan yang Anda pilih sudah disembunyikan dari publik", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                            if (id[1].equalsIgnoreCase("fas"))
                            {
                                FragmentFasilitas.refreshContent(Integer.parseInt(id[0]));
                            }
                            else
                            {
                                FragmentPelanggaran.refreshContent(Integer.parseInt(id[0]));
                            }
                        } else if (query_result.equals("GAGAL"))
                        {
                            Snackbar.make(BDukung, "Terjadi kesalahan, silahkan coba lagi", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                        } else
                        {
                            Snackbar.make(BDukung, "Tidak dapat terhubung ke database.", Snackbar.LENGTH_LONG).show();
                        }
                    } catch (JSONException e)
                    {
                        e.printStackTrace();
                        Toast.makeText(context, "Tidak dapat terhubung ke database, periksa koneksi internet kamu. " + jsonStr.toString(), Toast.LENGTH_LONG).show();
                    }
                } else
                {
                    Snackbar.make(BDukung, "Tidak dapat mengambil data JSON.", Snackbar.LENGTH_LONG).show();
                }
            }
        }

        BPilihan.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (sebagai.equalsIgnoreCase("Masyarakat"))
                {
                    AlertDialog.Builder builder2 = new AlertDialog.Builder(Komentar.this);
                    builder2.setTitle("Apa yang ingin Anda lakukan ?");
                    if (dilaporkanoleh.equalsIgnoreCase(pgid))
                    {
                        builder2.setItems(R.array.pilihan_laporan_my, new DialogInterface.OnClickListener()
                        {
                            public void onClick(DialogInterface dialog, int which)
                            {
                                if (which == 0)
                                {
                                    Satpam.setupFacebookShareIntent(Komentar.this, LNama.getText().toString(), LWaktu.getText().toString(), LLokasi.getText().toString(), LStatus.getText().toString(), urlforo, id[1].equalsIgnoreCase("fas") ? true : false);
                                }else if (which == 1)
                                {
                                    Satpam.Bagikan(Komentar.this, id[1].equalsIgnoreCase("fas") ? "Laporan Informasi dari " + LNama.getText().toString() : "Laporan Pelanggaran dari " + LNama.getText().toString(), LStatus.getText().toString(), urlforo);
                                }
                            }
                        });
                    }
                    else
                    {
                        builder2.setItems(R.array.pilihan_laporan, new DialogInterface.OnClickListener()
                        {
                            public void onClick(DialogInterface dialog, int which)
                            {
                                if (which == 0)
                                {
                                    final AlertDialog.Builder builder = new AlertDialog.Builder(Komentar.this);
                                    builder.setMessage("Apa Anda yakin ingin melaporkan laporan ini?")
                                            .setCancelable(true)
                                            .setPositiveButton("Iya", new DialogInterface.OnClickListener()
                                            {
                                                public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int did)
                                                {
                                                    Calendar cal = Calendar.getInstance();
                                                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                                    String formattedDate = df.format(cal.getTime());

                                                    new LaporkanManager(Komentar.this).execute(String.valueOf(id[0]), formattedDate, pgid, id[1]);
                                                }
                                            })
                                            .setNegativeButton("Tidak", new DialogInterface.OnClickListener()
                                            {
                                                public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                                                    dialog.cancel();
                                                }
                                            });
                                    final AlertDialog alert = builder.create();
                                    alert.show();
                                }
                                else if (which == 1)
                                {
                                    Satpam.setupFacebookShareIntent(Komentar.this, LNama.getText().toString(), LWaktu.getText().toString(), LLokasi.getText().toString(), LStatus.getText().toString(), urlforo, id[1].equalsIgnoreCase("fas") ? true : false);
                                }
                                else  if (which == 2)
                                {
                                    Satpam.Bagikan(Komentar.this, id[1].equalsIgnoreCase("fas") ? "Laporan Informasi dari " + LNama.getText().toString() : "Laporan Pelanggaran dari " + LNama.getText().toString(), LStatus.getText().toString(), urlforo);
                                }
                            }
                        });
                    }
                    builder2.show();
                }
                else
                {
                    AlertDialog.Builder builder2 = new AlertDialog.Builder(Komentar.this);
                    builder2.setTitle("Apa yang ingin Anda lakukan ?")
                            .setItems(R.array.pilihan_laporan_admin, new DialogInterface.OnClickListener()
                            {
                                public void onClick(DialogInterface dialog, int which)
                                {
                                    if (which == 0)
                                    {
                                        final AlertDialog.Builder builder = new AlertDialog.Builder(Komentar.this);
                                        builder.setMessage("Hapus laporan atau sembunyikan laporan dari publik ?\n\nLaporan yang sudah dihapus tidak dapat dikembalikan lagi.")
                                                .setCancelable(true)
                                                .setPositiveButton("Hapus", new DialogInterface.OnClickListener()
                                                {
                                                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int did)
                                                    {
                                                        new HapusLaporanManager(Komentar.this).execute(String.valueOf(id[0]));
                                                    }
                                                })
                                                .setNegativeButton("Batal", new DialogInterface.OnClickListener()
                                                {
                                                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id)
                                                    {
                                                        dialog.cancel();
                                                    }
                                                })
                                                .setNeutralButton("Sembunyikan", new DialogInterface.OnClickListener()
                                                {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which)
                                                    {
                                                        new SembunyikanLaporanManager(Komentar.this).execute(String.valueOf(id[0]));
                                                    }
                                                });
                                        final AlertDialog alert = builder.create();
                                        alert.show();
                                    }
                                    else if (which == 1)
                                    {
                                        Satpam.setupFacebookShareIntent(Komentar.this, LNama.getText().toString(), LWaktu.getText().toString(), LLokasi.getText().toString(), LStatus.getText().toString(), urlforo, id[1].equalsIgnoreCase("fas") ? true : false);
                                    }
                                    else  if (which == 2)
                                    {
                                        Satpam.Bagikan(Komentar.this, id[1].equalsIgnoreCase("fas") ? "Laporan Informasi dari " + LNama.getText().toString() : "Laporan Pelanggaran dari " + LNama.getText().toString(), LStatus.getText().toString(), urlforo);
                                    }
                                }
                            });
                    builder2.show();
                }
            }
        });

        if (didukung)
        {
            BDukung.setText("Batal Dukung");
            BDukung.setEnabled(true);
        }
        else
        {
            BDukung.setText("Dukung");
            BDukung.setEnabled(true);
        }

        AnimationSet set = new AnimationSet(true);

        Animation animation = new AlphaAnimation(0.0f, 1.0f);
        animation.setDuration(800);
        set.addAnimation(animation);

        animation = new TranslateAnimation(
                Animation.RELATIVE_TO_SELF, 0.0f,Animation.RELATIVE_TO_SELF, 0.0f,
                Animation.RELATIVE_TO_SELF, -1.0f, Animation.RELATIVE_TO_SELF, 0.0f
        );
        animation.setDuration(300);
        set.addAnimation(animation);

        LayoutAnimationController controller = new LayoutAnimationController(set, 0.5f);
        LVKomentar.setLayoutAnimation(controller);

        komentarItems = new ArrayList<KomentarItem>();
        listAdapter = new KomentarListAdapter(this, komentarItems);
        LVKomentar.setAdapter(listAdapter);

        BKirimKomentar.setOnClickListener(new Button.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (!TextUtils.isEmpty(TBKomentar.getText().toString()))
                {
                    if (Validation.isSopan(TBKomentar.getText().toString()))
                    {
                        Calendar cal = Calendar.getInstance();
                        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        String formattedDate = df.format(cal.getTime());

                        new KomentarManager(getApplicationContext()).execute(pgid, id[0], TBKomentar.getText().toString(), formattedDate);

                        TBKomentar.setText("");
                    } else
                    {
                        TBKomentar.setError(getString(R.string.error_tidak_sopan));
                        TBKomentar.requestFocus();
                    }
                } else
                {
                    TBKomentar.setError(getString(R.string.error_field_required));
                    TBKomentar.requestFocus();
                }
            }
        });

        CVGambar.setOnClickListener(new FeedImageView.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), FotoActivity.class);
                intent.putExtra("url", linkfoto);
                if (id[1].equalsIgnoreCase("fas"))
                {
                    FragmentFasilitas.perlu = false;
                    intent.putExtra("bagian", "Informasi");
                }
                else
                {
                    FragmentPelanggaran.perlu = false;
                    intent.putExtra("bagian", "Pelanggaran Polisi");
                }
                startActivity(intent);
            }
        });

        LLokasi.setOnClickListener(new TextView.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent i = new Intent(getApplicationContext(), MapsActivity.class);
                String[] ll = {lat, ling};
                i.putExtra(LATLING, ll);
                startActivity(i);
            }
        });

        if (!daripemberitahuan)
        {
            MuatData();
        }
        refreshContent();

        class DukungManager extends AsyncTask<String, Void, String>
        {
            private Context context;

            public DukungManager(Context context) {
                this.context = context;
            }

            @Override
            protected void onPreExecute()
            {
                super.onPreExecute();
            }

            @Override
            protected String doInBackground(String... arg0)
            {
                RequestHandler rh = new RequestHandler();

                String link;
                String result;

                try
                {
                    HashMap<String,String> data = new HashMap<>();
                    data.put("oleh", arg0[0]);
                    data.put("untuk", arg0[1]);

                    if (arg0[2].equalsIgnoreCase("fas"))
                    {
                        link = getString(R.string.alamat_server) + getString(R.string.link_dukung_fasilitas);
                    }
                    else
                    {
                        link = getString(R.string.alamat_server) + getString(R.string.link_dukung_pelanggaran);
                    }


                    result = rh.sendPostRequest(link, data);

                    return result;
                } catch (Exception e)
                {
                    return new String("Exception: " + e.getMessage());
                }
            }

            @Override
            protected void onPostExecute(String result)
            {
                String jsonStr = result;
                if (jsonStr != null)
                {
                    try
                    {
                        JSONObject jsonObj = new JSONObject(jsonStr);
                        String query_result = jsonObj.getString("hasil");
                        if (query_result.equals("SUKSES"))
                        {
                            Tampilkan(true, BDukung, "BATAL DUKUNG", id[0], id[1]);
                        } else if (query_result.equals("GAGAL"))
                        {
                            Tampilkan(true, BDukung, "DUKUNG", id[0], id[1]);
                            Snackbar.make(BDukung, "Terjadi kesalahan, silakan coba lagi.", Snackbar.LENGTH_LONG).show();
                        } else if (query_result.equals("SUDAH MENDUKUNG"))
                        {
                            Tampilkan(true, BDukung, "BATAL DUKUNG", id[0], id[1]);
                            Snackbar.make(BDukung, "Anda sudah mendukung masalah ini.", Snackbar.LENGTH_LONG).show();
                        } else
                        {
                            Tampilkan(true, BDukung, "DUKUNG", id[0], id[1]);
                            Snackbar.make(BDukung, "Tidak dapat terhubung ke database.", Snackbar.LENGTH_LONG).show();
                        }
                    } catch (JSONException e)
                    {
                        Tampilkan(true, BDukung, "DUKUNG", id[0], id[1]);
                        e.printStackTrace();
                        Toast.makeText(context, "Tidak dapat terhubung ke database, periksa koneksi internet kamu. " + jsonStr.toString(), Toast.LENGTH_LONG).show();
                    }
                } else
                {
                    Tampilkan(true, BDukung, "DUKUNG", id[0], id[1]);
                    Snackbar.make(BDukung, "Tidak dapat mengambil data JSON.", Snackbar.LENGTH_LONG).show();
                }
            }
        }

        class BatalDukungManager extends AsyncTask<String, Void, String>
        {
            private Context context;

            public BatalDukungManager(Context context) {
                this.context = context;
            }

            @Override
            protected void onPreExecute()
            {
                super.onPreExecute();
            }

            @Override
            protected String doInBackground(String... arg0)
            {
                RequestHandler rh = new RequestHandler();

                String link;
                String result;

                try
                {
                    HashMap<String,String> data = new HashMap<>();
                    data.put("oleh", arg0[0]);
                    data.put("untuk", arg0[1]);

                    if (arg0[2].equalsIgnoreCase("fas"))
                    {
                        link = getString(R.string.alamat_server) + getString(R.string.link_batal_dukung_fasilitas);
                    }
                    else
                    {
                        link = getString(R.string.alamat_server) + getString(R.string.link_batal_dukung_pelanggaran);
                    }

                    result = rh.sendPostRequest(link, data);

                    return result;
                } catch (Exception e)
                {
                    return new String("Exception: " + e.getMessage());
                }
            }

            @Override
            protected void onPostExecute(String result)
            {
                String jsonStr = result;
                if (jsonStr != null)
                {
                    try
                    {
                        JSONObject jsonObj = new JSONObject(jsonStr);
                        String query_result = jsonObj.getString("hasil");
                        if (query_result.equals("SUKSES"))
                        {
                            Tampilkan(true, BDukung, "DUKUNG", id[0], id[1]);
                        } else if (query_result.equals("GAGAL"))
                        {
                            Tampilkan(true, BDukung, "BATAL DUKUNG", id[0], id[1]);
                            Snackbar.make(BDukung, "Terjadi kesalahan, silakan coba lagi.", Snackbar.LENGTH_LONG).show();
                        } else
                        {
                            Tampilkan(true, BDukung, "BATAL DUKUNG", id[0], id[1]);
                            Snackbar.make(BDukung, "Tidak dapat terhubung ke database.", Snackbar.LENGTH_LONG).show();
                        }
                    } catch (JSONException e)
                    {
                        Tampilkan(true, BDukung, "BATAL DUKUNG", id[0], id[1]);
                        e.printStackTrace();
                        Toast.makeText(context, "Tidak dapat terhubung ke database, periksa koneksi internet kamu. " + jsonStr.toString(), Toast.LENGTH_LONG).show();
                    }
                } else
                {
                    Tampilkan(true, BDukung, "BATAL DUKUNG", id[0], id[1]);
                    Snackbar.make(BDukung, "Tidak dapat mengambil data JSON.", Snackbar.LENGTH_LONG).show();
                }
            }
        }

        BDukung.setOnClickListener(new Button.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                final Boolean tampilkan = true;
                BDukung.setEnabled(false);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2)
                {
                    final int shortAnimTime = 500;

                    if (BDukung.getText().toString().equalsIgnoreCase("DUKUNG"))
                    {
                        new DukungManager(getApplicationContext()).execute(pgid, String.valueOf(id[0]), id[1]);

                        BDukung.animate().setDuration(shortAnimTime).alpha(tampilkan ? 0 : 1).setListener(new AnimatorListenerAdapter()
                        {
                            @Override
                            public void onAnimationEnd(Animator animation)
                            {

                            }
                        });
                    } else
                    {
                        new BatalDukungManager(getApplicationContext()).execute(pgid, String.valueOf(id[0]), id[1]);

                        BDukung.animate().setDuration(shortAnimTime).alpha(tampilkan ? 0 : 1).setListener(new AnimatorListenerAdapter()
                        {
                            @Override
                            public void onAnimationEnd(Animator animation)
                            {

                            }
                        });
                    }

                } else
                {
                    BDukung.setVisibility(tampilkan ? View.INVISIBLE : View.VISIBLE);
                }
            }
        });
    }

    public static final String LATLING = "";

    ImageLoader imageLoader = AppController.getInstance().getImageLoader();

    private void MuatData()
    {
        if (imageLoader == null)
            imageLoader = AppController.getInstance().getImageLoader();

        String str = nama;
        String[] strArray = str.split(" ");
        StringBuilder builder = new StringBuilder();
        for (String s : strArray) {
            String cap = s.substring(0, 1).toUpperCase() + s.substring(1);
            builder.append(cap + " ");
        }

        LNama.setText(builder.toString());

        LStatus.setText(status.substring(0,1).toUpperCase() + status.substring(1));

        String expectedPattern = "yyyy-MM-dd HH:mm:ss";
        SimpleDateFormat formatter = new SimpleDateFormat(expectedPattern);
        String userInput = waktu;
        Date tanggal = null;
        try
        {
            tanggal = formatter.parse(userInput);
        } catch (ParseException e)
        {
            e.printStackTrace();
        }
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy hh:mm a");
        Calendar cal = Calendar.getInstance();
        sdf.setTimeZone(cal.getTimeZone());

        LWaktu.setText(sdf.format(tanggal));

        if (TextUtils.isEmpty(alamat))
        {
            if (!TextUtils.isEmpty(lat))
            {
                LLokasi.setText(lat + ", " + ling);
            }
            else
            {
                LLokasi.setVisibility(View.GONE);
            }
        }
        else
        {
            LLokasi.setText(alamat);
        }

        LDukungan.setText(dukungan);
        LKomentar.setText(komentar);

        LPrioritas.setText(prioritas);

        if (urlforo != null) {
            linkfoto = urlforo;
            CVGambar.setImageUrl(urlforo, imageLoader, "1");
            CVGambar.setVisibility(View.VISIBLE);
            CVGambar.setResponseObserver(new FeedImageView.ResponseObserver() {
                @Override
                public void onError() {
                }

                @Override
                public void onSuccess() {
                }
            });
        } else {
            CVGambar.setVisibility(View.GONE);
        }
    }

    String linkfoto, lat, ling;

    static boolean daritambah = false;

    private static void parseJsonKomentar(JSONObject response) {
        try {
            JSONArray feedArray = response.getJSONArray("komentar");

            komentarItems.clear();

            for (int i = 0; i < feedArray.length(); i++) {
                JSONObject feedObj = (JSONObject) feedArray.get(i);

                KomentarItem item = new KomentarItem();
                item.setKmid(feedObj.getInt("kmid"));
                item.setNama(feedObj.getString("nama"));
                item.setKomentar(feedObj.getString("komentar"));
                item.setWaktu(feedObj.getString("waktu"));
                item.setLfid(feedObj.getString("untuk"));
                item.setKomentarku(feedObj.getString("komentarku"));
                item.setBagian(id[1]);
                item.setSebagai(feedObj.getString("sebagai"));

                komentarItems.add(item);
            }

            LStatusKomentar.setText("Komentar");
            LVKomentar.setVisibility(View.VISIBLE);
            listAdapter.notifyDataSetChanged();

            if (daritambah)
            {
                LVKomentar.smoothScrollToPosition(LVKomentar.getCount());
                daritambah = false;
            }
        } catch (JSONException e) {
            e.printStackTrace();
            LStatusKomentar.setText("Tidak Ada Komentar");
            LVKomentar.setVisibility(View.GONE);
        }
    }

    class KomentarManager extends AsyncTask<String, Void, String>
    {
        private Context context;
        String oleh, untuk, komentar;

        public KomentarManager(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... arg0)
        {
            RequestHandler rh = new RequestHandler();

            String link;
            String result;

            try
            {
                HashMap<String,String> data = new HashMap<>();
                data.put("oleh", arg0[0]);
                oleh = arg0[0];
                data.put("untuk", arg0[1]);
                untuk = arg0[1];
                data.put("komentar", arg0[2]);
                data.put("waktu", arg0[3]);

                komentar = arg0[2];

                if (id[1].equalsIgnoreCase("fas"))
                {
                    link = getApplicationContext().getString(R.string.alamat_server) + getApplicationContext().getString(R.string.link_komen_fasilitas);
                }
                else
                {
                    link = getApplicationContext().getString(R.string.alamat_server) + getApplicationContext().getString(R.string.link_komen_pelanggaran);
                }

                result = rh.sendPostRequest(link, data);

                return result;
            } catch (Exception e)
            {
                return new String("Exception: " + e.getMessage());
            }
        }

        @Override
        protected void onPostExecute(String result)
        {
            String jsonStr = result;
            if (jsonStr != null)
            {
                try
                {
                    JSONObject jsonObj = new JSONObject(jsonStr);
                    String query_result = jsonObj.getString("hasil");
                    if (query_result.equals("SUKSES"))
                    {
                        TBKomentar.setText("");
                        refreshContent();
                        daritambah = true;

                        if (id[1].equalsIgnoreCase("fas") && !dilaporkanoleh.equalsIgnoreCase(pgid))
                        {
                            Intent intent = new Intent(getBaseContext(), GCMHandle.class);
                            intent.putExtra("tipe", "komentar");
                            intent.putExtra("oleh", oleh);
                            intent.putExtra("untuk", untuk);
                            intent.putExtra("bagian", "Komentar Fasilitas");
                            startService(intent);
                        }
                        else if (id[1].equalsIgnoreCase("plgr") && !dilaporkanoleh.equalsIgnoreCase(pgid))
                        {
                            Intent intent = new Intent(getBaseContext(), GCMHandle.class);
                            intent.putExtra("tipe", "komentar");
                            intent.putExtra("oleh", oleh);
                            intent.putExtra("untuk", untuk);
                            intent.putExtra("bagian", "Komentar Pelanggaran");
                            startService(intent);
                        }
                    } else if (query_result.equals("GAGAL"))
                    {
                        TBKomentar.setText(komentar);
                        Snackbar.make(LStatus, "Terjadi kesalahan, silakan coba lagi. - Komentar", Snackbar.LENGTH_LONG).show();
                    } else
                    {
                        TBKomentar.setText(komentar);
                        Snackbar.make(LStatus, "Tidak dapat terhubung ke database.", Snackbar.LENGTH_LONG).show();
                    }
                } catch (JSONException e)
                {
                    TBKomentar.setText(komentar);
                    e.printStackTrace();
                    Toast.makeText(context, "Tidak dapat terhubung ke database, periksa koneksi internet kamu. " + jsonStr.toString(), Toast.LENGTH_LONG).show();
                }
            } else
            {
                TBKomentar.setText(komentar);
                Snackbar.make(LStatus, "Tidak dapat mengambil data JSON.", Snackbar.LENGTH_LONG).show();
            }
        }
    }

    public String dilaporkanoleh;
    public boolean daripemberitahuan, didukung;

    private void parseJsonLaporan(JSONObject response) {
        try {
            JSONArray feedArray = response.getJSONArray("feed");
            if (imageLoader == null)
                imageLoader = AppController.getInstance().getImageLoader();

            JSONObject feedObj = (JSONObject) feedArray.get(0);

            String str = feedObj.getString("n");
            String[] strArray = str.split(" ");
            StringBuilder builder = new StringBuilder();
            for (String s : strArray) {
                String cap = s.substring(0, 1).toUpperCase() + s.substring(1);
                builder.append(cap + " ");
            }

            LNama.setText(builder.toString());

            if (id[1].equalsIgnoreCase("fas"))
            {
                LStatus.setText(feedObj.getString("c").substring(0,1).toUpperCase() + feedObj.getString("c").substring(1));
            }
            else
            {
                LStatus.setText(feedObj.getString("c").substring(0,1).toUpperCase() + feedObj.getString("c").substring(1));
            }

            dilaporkanoleh = feedObj.getString("j");

            String expectedPattern = "yyyy-MM-dd HH:mm:ss";
            SimpleDateFormat formatter = new SimpleDateFormat(expectedPattern);
            String userInput = feedObj.getString("h");
            Date tanggal = null;
            try
            {
                tanggal = formatter.parse(userInput);
            } catch (ParseException e)
            {
                e.printStackTrace();
            }
            SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy hh:mm a");
            Calendar cal = Calendar.getInstance();
            sdf.setTimeZone(cal.getTimeZone());

            LWaktu.setText(sdf.format(tanggal));

            if (TextUtils.isEmpty(feedObj.getString("f")))
            {
                if (!TextUtils.isEmpty(feedObj.getString("d")))
                {
                    LLokasi.setText(feedObj.getString("d") + ", " + feedObj.getString("e"));
                }
                else
                {
                    LLokasi.setVisibility(View.GONE);
                }
            }
            else
            {
                LLokasi.setText(feedObj.getString("f"));
            }

            LDukungan.setText(feedObj.getString("l"));
            LKomentar.setText(feedObj.getString("m"));

            LPrioritas.setText(feedObj.getString("g"));

            if (feedObj.getString("b") != null) {
                CVGambar.setImageUrl(feedObj.getString("b"), imageLoader, feedObj.getString("i"));
                CVGambar.setVisibility(View.VISIBLE);
                CVGambar.setResponseObserver(new FeedImageView.ResponseObserver() {
                    @Override
                    public void onError() {
                    }

                    @Override
                    public void onSuccess() {
                    }
                });
            } else {
                CVGambar.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            //Toast.makeText(getApplicationContext(), "Terjadi kesalahan, silakan coba lagi. " + e.toString(), Toast.LENGTH_LONG).show();
        }
    }

    public void refreshContent()
    {
        if (daripemberitahuan)
        {
            JsonObjectRequest jsonReq = new JsonObjectRequest(Request.Method.GET, URL_FEED, null, new Response.Listener<JSONObject>()
            {
                @Override
                public void onResponse(JSONObject response) {

                    VolleyLog.d(TAG, "Response: " + response.toString());
                    if (response != null) {
                        parseJsonLaporan(response);
                    }
                }
            }, new Response.ErrorListener()
            {
                @Override
                public void onErrorResponse(VolleyError error)
                {
                    if (!isFinishing())
                    {
                        refreshContent();
                    }
                }
            });

            AppController.getInstance().addToRequestQueue(jsonReq);
        }

        JsonObjectRequest jsonReq2 = new JsonObjectRequest(Request.Method.GET, URL_KOMENTAR, null, new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response) {
                VolleyLog.d(TAG, "Response: " + response.toString());
                if (response != null) {
                    parseJsonKomentar(response);
                }
            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                if (!isFinishing())
                {
                    refreshContent();
                }
            }
        });

        AppController.getInstance().addToRequestQueue(jsonReq2);
    }
}
