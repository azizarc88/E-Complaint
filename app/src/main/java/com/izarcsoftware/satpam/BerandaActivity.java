package com.izarcsoftware.satpam;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Menu;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.View;

public class BerandaActivity extends AppCompatActivity
{
    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private FragmentFasilitas fragmentFasilitas;
    private FragmentPelanggaran fragmentPelanggaran;

    public static boolean isBeranda()
    {
        return beranda;
    }

    public void setBeranda(boolean beranda)
    {
        this.beranda = beranda;
    }

    private static boolean beranda = true;

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        fragmentFasilitas = new FragmentFasilitas();
        fragmentPelanggaran = new FragmentPelanggaran();
        adapter.addFragment(fragmentFasilitas, "Laporan Informasi");
        adapter.addFragment(fragmentPelanggaran, "Laporan Pelanggaran");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter
    {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position)
        {
            return mFragmentTitleList.get(position);
        }
    }

    private int[] tabIcons = {
            R.drawable.ic_tab_fasilitas,
            R.drawable.ic_tab_pelanggaran
    };

    private void setupTabIcons() {
        tabLayout.getTabAt(0).setIcon(tabIcons[0]);
        tabLayout.getTabAt(1).setIcon(tabIcons[1]);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_beranda);

        Intent intent = getIntent();
        beranda = intent.getBooleanExtra("beranda", true);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new Toolbar.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                finish();
            }
        });

        if (!beranda)
        {
            getSupportActionBar().setTitle("Laporan Anda");
        }

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        setupTabIcons();

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener()
        {
            @Override
            public void onTabSelected(TabLayout.Tab tab)
            {
                if (String.valueOf(tab.getText()).equalsIgnoreCase("Laporan Informasi"))
                {
                    viewPager.setCurrentItem(0, true);
                    fragmentFasilitas.setIsDiliat(true);
                    fragmentPelanggaran.setIsDiliat(false);
                }
                else
                {
                    viewPager.setCurrentItem(1, true);
                    fragmentFasilitas.setIsDiliat(false);
                    fragmentPelanggaran.setIsDiliat(true);
                    fragmentPelanggaran.onSelected();
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab)
            {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab)
            {

            }
        });
    }
}
