package com.izarcsoftware.satpam;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.LayoutAnimationController;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class Lupakatasandi extends AppCompatActivity
{
    private Button BProses1, BProses2, BProses3;
    private EditText TBNoHp, TBEmail, TBPassword, TBUlangiPassword;
    private DatePicker DPTanggalLahir;
    private LinearLayout LLStep1, LLStep2, LLStep3;

    private Context context;

    private String pgid = "null", hasil;

    private void Transisi(final LinearLayout linearLayout1, final LinearLayout linearLayout2)
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2)
        {
            int shortAnimTime = 500;

            linearLayout1.animate().setDuration(shortAnimTime).alpha(0).setListener(new AnimatorListenerAdapter()
            {
                @Override
                public void onAnimationEnd(Animator animation)
                {
                    linearLayout1.setVisibility(View.GONE);

                    linearLayout2.setVisibility(View.VISIBLE);
                    linearLayout2.setAlpha(0);
                    linearLayout2.animate().setDuration(250).alpha(1).setListener(new AnimatorListenerAdapter()
                    {
                        @Override
                        public void onAnimationEnd(Animator animation)
                        {
                            linearLayout2.setVisibility(View.VISIBLE);
                        }
                    });
                }
            });
        }
        else
        {
            linearLayout1.setVisibility(View.GONE);
            linearLayout2.setVisibility(View.VISIBLE);
        }
    }

    private boolean isAllValid()
    {
        Boolean valid = true;

        if (TextUtils.isEmpty(TBNoHp.getText().toString()))
        {
            TBNoHp.setError(getString(R.string.error_field_required));
            TBNoHp.requestFocus();
            valid = false;
        }
        else if (!TextUtils.isEmpty(TBNoHp.getText().toString()) && TBNoHp.getText().toString().length() < 10)
        {
            TBNoHp.setError(getString(R.string.error_invalid_nohp));
            TBNoHp.requestFocus();
            valid = false;
        }
        else if (TextUtils.isEmpty(TBEmail.getText().toString()))
        {
            TBEmail.setError(getString(R.string.error_field_required));
            TBEmail.requestFocus();
            valid = false;
        }
        else if (TBEmail.getText().toString().contains("@") == false || TBEmail.getText().toString().contains(".") == false)
        {
            TBEmail.setError(getString(R.string.error_invalid_email));
            TBEmail.requestFocus();
            valid = false;
        }

        return valid;
    }

    private boolean isAllValid2()
    {
        Boolean valid = true;

        if (TextUtils.isEmpty(TBPassword.getText().toString()))
        {
            TBPassword.setError(getString(R.string.error_field_required));
            TBPassword.requestFocus();
            valid = false;
        }
        else if (!TextUtils.isEmpty(TBPassword.getText().toString()) && TBPassword.getText().toString().length() < 5)
        {
            TBPassword.setError(getString(R.string.error_invalid_password));
            TBPassword.requestFocus();
            valid = false;
        }
        else if (!TBPassword.getText().toString().equals(TBUlangiPassword.getText().toString()))
        {
            TBUlangiPassword.setError(getString(R.string.notmatch_password));
            TBUlangiPassword.requestFocus();
            valid = false;
        }
        else if (pgid.equalsIgnoreCase("null"))
        {
            valid = false;
            final AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
            builder1.setMessage("Maaf, Kami tidak bisa mencocokan data yang Anda kirim ke kami dengan akun apapun, kembali ke tahap 1 ?")
                    .setCancelable(false)
                    .setPositiveButton("Iya", new DialogInterface.OnClickListener()
                    {
                        public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id)
                        {
                            Transisi(LLStep3, LLStep1);
                        }
                    })
                    .setNegativeButton("Tidak", new DialogInterface.OnClickListener()
                    {
                        public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id)
                        {
                            dialog.cancel();
                        }
                    });
            final AlertDialog alert = builder1.create();
            alert.show();

        }

        return valid;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lupakatasandi);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        context = this;

        BProses1 = (Button) findViewById(R.id.BProses1);
        BProses2 = (Button) findViewById(R.id.BProses2);
        BProses3 = (Button) findViewById(R.id.BProses3);

        TBNoHp = (EditText) findViewById(R.id.TBNoHp);
        TBEmail = (EditText) findViewById(R.id.TBEmail);
        TBPassword = (EditText) findViewById(R.id.TBPassword);
        TBUlangiPassword = (EditText) findViewById(R.id.TBUlangiPassword);

        DPTanggalLahir = (DatePicker) findViewById(R.id.DPTanggalLahir);

        LLStep1 = (LinearLayout) findViewById(R.id.LLStep1);
        LLStep2 = (LinearLayout) findViewById(R.id.LLStep2);
        LLStep3 = (LinearLayout) findViewById(R.id.LLStep3);

        BProses1.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isAllValid())
                {
                    new ProsesIdentitas(context).execute(TBEmail.getText().toString(), TBNoHp.getText().toString(), String.valueOf(DPTanggalLahir.getYear()) + "-" + String.valueOf(DPTanggalLahir.getMonth() + 1) + "-" + String.valueOf(DPTanggalLahir.getDayOfMonth()));
                    Transisi(LLStep1, LLStep2);
                }
            }
        });

        BProses2.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                Transisi(LLStep2, LLStep3);
            }
        });

        BProses3.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isAllValid2())
                {
                    new UbahPass(context).execute(TBPassword.getText().toString(), pgid);
                }
            }
        });
    }

    class ProsesIdentitas extends AsyncTask<String, Void, String>
    {
        private Context context;

        public ProsesIdentitas(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... arg0)
        {
            RequestHandler rh = new RequestHandler();

            String link;
            String result;

            try
            {
                HashMap<String,String> data = new HashMap<>();
                data.put("email", arg0[0]);
                data.put("nohp", arg0[1]);
                data.put("tanggallahir", arg0[2]);

                link = getString(R.string.alamat_server) + getString(R.string.link_lupa_pass);

                result = rh.sendPostRequest(link, data);

                return result;
            } catch (Exception e)
            {
                return new String("Exception: " + e.getMessage());
            }
        }

        @Override
        protected void onPostExecute(String result)
        {
            String jsonStr = result;
            if (jsonStr != null)
            {
                try
                {
                    JSONObject jsonObj = new JSONObject(jsonStr);
                    String query_result = jsonObj.getString("hasil");
                    hasil = query_result;
                    if (query_result.equals("SUKSES"))
                    {
                        pgid = jsonObj.getString("pgid");
                    } else if (query_result.equals("GAGAL"))
                    {

                    } else
                    {
                        new ProsesIdentitas(context).execute(TBEmail.getText().toString(), TBNoHp.getText().toString(), String.valueOf(DPTanggalLahir.getYear()) + "-" + String.valueOf(DPTanggalLahir.getMonth() + 1) + "-" + String.valueOf(DPTanggalLahir.getDayOfMonth()));
                    }
                } catch (JSONException e)
                {
                    new ProsesIdentitas(context).execute(TBEmail.getText().toString(), TBNoHp.getText().toString(), String.valueOf(DPTanggalLahir.getYear()) + "-" + String.valueOf(DPTanggalLahir.getMonth() + 1) + "-" + String.valueOf(DPTanggalLahir.getDayOfMonth()));
                }
            } else
            {
                new ProsesIdentitas(context).execute(TBEmail.getText().toString(), TBNoHp.getText().toString(), String.valueOf(DPTanggalLahir.getYear()) + "-" + String.valueOf(DPTanggalLahir.getMonth() + 1) + "-" + String.valueOf(DPTanggalLahir.getDayOfMonth()));
            }
        }
    }

    private ProgressDialog pDialog;
    public static final String EMAIL = "";

    class UbahPass extends AsyncTask<String, Void, String>
    {
        private Context context;

        public UbahPass(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            pDialog = new ProgressDialog(Lupakatasandi.this);
            pDialog.setMessage("Memproses...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... arg0)
        {
            RequestHandler rh = new RequestHandler();

            String link;
            String result;

            try
            {
                HashMap<String,String> data = new HashMap<>();
                data.put("password", arg0[0]);
                data.put("pgid", arg0[1]);

                link = getString(R.string.alamat_server) + getString(R.string.link_ubah_pass);

                result = rh.sendPostRequest(link, data);

                return result;
            } catch (Exception e)
            {
                return new String("Exception: " + e.getMessage());
            }
        }

        @Override
        protected void onPostExecute(String result)
        {
            String jsonStr = result;
            if (jsonStr != null)
            {
                try
                {
                    JSONObject jsonObj = new JSONObject(jsonStr);
                    String query_result = jsonObj.getString("hasil");
                    hasil = query_result;
                    if (query_result.equals("SUKSES"))
                    {
                        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
                        builder.setMessage("Akun anda sudah berhasil diubah dengan password yang baru.")
                                .setCancelable(false)
                                .setNeutralButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                                        dialog.cancel();
                                        Intent i = new Intent(context, LoginActivity.class);
                                        i.putExtra(EMAIL, TBEmail.getText().toString());
                                        startActivity(i);
                                        finish();
                                    }
                                });
                        final AlertDialog alert = builder.create();
                        alert.show();
                    } else if (query_result.equals("GAGAL"))
                    {
                        Snackbar.make(BProses3, "Tidak dapat mereset password.", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                    } else
                    {
                        Snackbar.make(BProses3, "Terjadi kesalahan, harap coba lagi nanti.", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                    }
                } catch (JSONException e)
                {
                    Snackbar.make(BProses3, "Terjadi kesalahan, harap coba lagi nanti - " + e.toString(), Snackbar.LENGTH_LONG).setAction("Action", null).show();
                }
            } else
            {
                Snackbar.make(BProses3, "Terjadi kesalahan, harap coba lagi nanti - " + result, Snackbar.LENGTH_LONG).setAction("Action", null).show();
            }

            if (pDialog.isShowing())
                pDialog.dismiss();
        }
    }
}
