package com.izarcsoftware.satpam;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.LayoutAnimationController;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.izarcsoftware.satpam.adapter.ChatItemAdapter;
import com.izarcsoftware.satpam.app.AppController;
import com.izarcsoftware.satpam.data.ChatItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

public class RuangObrolanActivity extends AppCompatActivity
{
    private Toolbar toolbar;
    private static String URL_FEED;
    public static String tpgid;
    public static String nama;
    public static ListView LVObrolan;
    public static TextView LInfo;
    public static ChatItemAdapter listAdapter;
    public static List<ChatItem> chatItems;
    public static Button BKirim;
    public static EditText TBPesan;
    public String pgid;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ruang_obrolan);

        batalkan = false;

        Intent intent = getIntent();
        tpgid = intent.getStringArrayExtra("data")[0];
        nama = intent.getStringArrayExtra("data")[1];

        ChatItemAdapter.cc = this;

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Obrolan - " + nama);
        toolbar.setNavigationOnClickListener(new Toolbar.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                finish();
            }
        });

        LVObrolan = (ListView) findViewById(R.id.LVObrolan);
        TBPesan = (EditText) findViewById(R.id.TBPesan);
        BKirim = (Button) findViewById(R.id.BKirim);
        LInfo = (TextView) findViewById(R.id.LInfo);

        BKirim.setOnClickListener(new Button.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (!TextUtils.isEmpty(TBPesan.getText().toString()))
                {
                    Calendar cal = Calendar.getInstance();
                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String formattedDate = df.format(cal.getTime());

                    Intent intent = new Intent(getBaseContext(), PengirimanData.class);
                    intent.putExtra("dari", pgid);
                    intent.putExtra("untuk", tpgid);
                    intent.putExtra("isipesan", TBPesan.getText().toString());
                    intent.putExtra("waktu", formattedDate);
                    intent.putExtra("posisi", String.valueOf(chatItems.size()));
                    startService(intent);

                    ChatItem item = new ChatItem();
                    item.setDari(pgid);
                    item.setUntuk(tpgid);
                    item.setIsipesan(TBPesan.getText().toString());
                    item.setWaktu(formattedDate);
                    item.setPending(true);
                    chatItems.add(item);

                    listAdapter.notifyDataSetChanged();

                    TBPesan.setText("");
                    LVObrolan.smoothScrollToPosition(LVObrolan.getCount());
                }
                else
                {
                    TBPesan.setError(getString(R.string.error_field_required));
                    TBPesan.requestFocus();
                }
            }
        });

        SharedPreferences sharedPref;
        sharedPref = getApplicationContext().getSharedPreferences("File_Pengaturan", Context.MODE_PRIVATE);
        pgid = sharedPref.getString(getString(R.string.p_id_pengguna), "Tidak Ada");

        URL_FEED = getString(R.string.alamat_server) + getString(R.string.link_ambil_obrolan) + "?dari=" + pgid + "&untuk=" + tpgid;

        chatItems = new ArrayList<ChatItem>();
        listAdapter = new ChatItemAdapter(chatItems, this);
        LVObrolan.setAdapter(listAdapter);

        Cache cache = AppController.getInstance().getRequestQueue().getCache();
        Cache.Entry entry = cache.get(URL_FEED);
        if (entry != null) {
            // fetch the data from cache
            try {
                String data = new String(entry.data, "UTF-8");
                try {
                    parseJsonFeed(new JSONObject(data));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

        } else {
        }
        refreshContent();
    }

    private static void parseJsonFeed(JSONObject response)
    {
        try
        {
            JSONArray feedArray = response.getJSONArray("feed");

            chatItems.clear();
            for (int i = 0; i < feedArray.length(); i++)
            {
                JSONObject feedObj = (JSONObject) feedArray.get(i);

                ChatItem item = new ChatItem();
                item.setObid(feedObj.getString("obid"));
                item.setDari(feedObj.getString("dari"));
                item.setUntuk(feedObj.getString("untuk"));
                item.setIsipesan(feedObj.getString("isipesan"));
                item.setWaktu(feedObj.getString("waktu"));

                chatItems.add(item);
            }

            if (LInfo.getVisibility() == View.VISIBLE)
            {
                LInfo.setVisibility(View.GONE);
            }

            listAdapter.notifyDataSetChanged();

            LVObrolan.setSelection(LVObrolan.getCount());
        }
        catch (Exception e)
        {
            LInfo.setVisibility(View.VISIBLE);
        }
    }

    public static boolean batalkan = false;

    @Override
    protected void onPause()
    {
        super.onPause();
        batalkan = true;
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        batalkan = false;
    }

    static int jumlah = 0;

    public static void refreshContent()
    {
        JsonObjectRequest jsonReq = new JsonObjectRequest(Request.Method.GET,
                URL_FEED, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                if (response != null) {
                    parseJsonFeed(response);
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                if (!batalkan)
                {
                    refreshContent();
                }
            }
        });

        // Adding request to volley request queue
        AppController.getInstance().addToRequestQueue(jsonReq);
    }
}
