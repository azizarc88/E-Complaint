package com.izarcsoftware.satpam;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.text.TextUtils;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.LayoutAnimationController;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.izarcsoftware.satpam.app.Crypt;
import com.izarcsoftware.satpam.data.StringUtil;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener
{
    LinearLayout Isian;
    LinearLayout InfoAbout;
    SharedPreferences sharedPref;
    private String nama = "", belumlengkap, sebagai;
    public static final boolean BERANDA = false;
    private boolean firstrun;

    private void goToUrl (String url) {
        Uri uriUrl = Uri.parse(url);
        Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
        startActivity(launchBrowser);
    }

    private boolean BelumLengkap()
    {
        if (belumlengkap.equalsIgnoreCase("1") && sebagai.equalsIgnoreCase("Masyarakat"))
        {
            final android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(MainActivity.this);
            builder.setMessage("Anda belum melengkapi data diri Anda, segera lengkapi untuk dapat melaporkan sesuatu di sini.\n\nLengkapi ?")
                    .setCancelable(true)
                    .setPositiveButton("Iya", new DialogInterface.OnClickListener()
                    {
                        public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id)
                        {
                            Intent intent = new Intent(getApplicationContext(), Profil.class);
                            startActivity(intent);
                        }
                    })
                    .setNegativeButton("Nanti", new DialogInterface.OnClickListener() {
                        public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                            dialog.cancel();
                        }
                    });
            final android.app.AlertDialog alert = builder.create();
            alert.show();

            return false;
        }
        else
        {
            return true;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        Intent intent1 = getIntent();
        String infog = intent1.getStringExtra("infog");
        if (!TextUtils.isEmpty(infog))
        {
            final AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(infog + "\n\nUpdate sekarang ?")
                    .setCancelable(true)
                    .setPositiveButton("Iya", new DialogInterface.OnClickListener() {
                        public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                            final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                            try {
                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                            } catch (android.content.ActivityNotFoundException anfe) {
                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                            }
                        }
                    })
                    .setNegativeButton("Nanti", new DialogInterface.OnClickListener() {
                        public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                            dialog.cancel();
                        }
                    });
            final AlertDialog alert = builder.create();
            alert.show();
        }

        Isian = (LinearLayout) findViewById(R.id.Isian);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        final ImageView BLFasilitas = (ImageView) findViewById(R.id.BLFasilitas);
        BLFasilitas.setOnClickListener(new ImageView.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (BelumLengkap())
                {
                    Intent intent = new Intent(getApplicationContext(), LFasilitasActivity.class);
                    startActivity(intent);
                }
            }
        });

        ImageView BLPelanggaran = (ImageView) findViewById(R.id.BLPelanggaran);
        BLPelanggaran.setOnClickListener(new ImageView.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (BelumLengkap())
                {
                    Intent intent = new Intent(getApplicationContext(), LPelanggaranActivity.class);
                    startActivity(intent);
                }
            }
        });

        ImageView BObrolan = (ImageView) findViewById(R.id.BObrolan);
        BObrolan.setOnClickListener(new ImageView.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (BelumLengkap())
                {
                    Intent intent = new Intent(getApplicationContext(), ObrolanActivity.class);
                    startActivity(intent);
                }
            }
        });

        ImageView BBantuan = (ImageView) findViewById(R.id.BBeranda);
        BBantuan.setOnClickListener(new ImageView.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(getApplicationContext(), BerandaActivity.class);
                intent.putExtra("beranda", true);
                startActivity(intent);
            }
        });

        ImageView BLaporanTertinggi = (ImageView) findViewById(R.id.BPelaporTertinggi);
        BLaporanTertinggi.setOnClickListener(new ImageView.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(getApplicationContext(), PelaporActivity.class);
                startActivity(intent);
            }
        });

        ImageView BLaporanAnda = (ImageView) findViewById(R.id.BLaporanAnda);
        BLaporanAnda.setOnClickListener(new ImageView.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (BelumLengkap())
                {
                    Intent intent = new Intent(getApplicationContext(), BerandaActivity.class);
                    intent.putExtra("beranda", false);
                    startActivity(intent);
                }
            }
        });

        AnimationSet set = new AnimationSet(true);

        Animation animation = new AlphaAnimation(0.0f, 1.0f);
        animation.setDuration(500);
        set.addAnimation(animation);

        animation = new TranslateAnimation(
                Animation.RELATIVE_TO_SELF, 0.0f,Animation.RELATIVE_TO_SELF, 0.0f,
                Animation.RELATIVE_TO_SELF, -1.0f, Animation.RELATIVE_TO_SELF, 0.0f
        );
        animation.setDuration(250);
        set.addAnimation(animation);

        LayoutAnimationController controller = new LayoutAnimationController(set, 0.5f);
        Isian.setLayoutAnimation(controller);

        sharedPref = getSharedPreferences("File_Pengaturan", Context.MODE_PRIVATE);

        VV = BBantuan;

        sharedPref = getApplicationContext().getSharedPreferences("File_Pengaturan", Context.MODE_PRIVATE);
        firstrun = sharedPref.getBoolean(getString(R.string.p_f), true);
        String temp = sharedPref.getString(getString(R.string.p_firstrun), "Tidak Ada");
        belumlengkap = sharedPref.getString(getString(R.string.p_belumlengkap), "Tidak Ada");
        sebagai = sharedPref.getString(getString(R.string.p_sebagai), "Tidak Ada");

        if (firstrun)
        {
            Intent intent = new Intent(getApplicationContext(), Fitur.class);
            startActivity(intent);
        }

        if (temp.equalsIgnoreCase("Tidak Ada"))
        {
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putString(getString(R.string.p_firstrun), "0");
            editor.commit();
        }
        else if (temp.equalsIgnoreCase("0"))
        {
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putString(getString(R.string.p_firstrun), "1");
            editor.commit();
        }
        else if (temp.equalsIgnoreCase("1"))
        {
            BelumLengkap();
        }
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        nama = sharedPref.getString(getString(R.string.p_nama), "Tidak Ada");

        TextView LNama = (TextView) findViewById(R.id.LNamaPLogin);
        LNama.setText(nama);
    }

    @Override
    protected void onStart()
    {
        super.onStart();
    }

    @Override
    public void onBackPressed()
    {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START))
        {
            drawer.closeDrawer(GravityCompat.START);
        } else
        {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings)
        {
            Intent intent = new Intent(getApplicationContext(), tentang.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item)
    {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_profil)
        {
            Intent intent = new Intent(getApplicationContext(), Profil.class);
            startActivity(intent);
        } else if (id == R.id.nav_pemberitahuan)
        {
            Intent intent = new Intent(getApplicationContext(), PemberitahuanActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_pemberitahuanlangsung)
        {
            Intent intent = new Intent(getApplicationContext(), NotifikasiActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_tentang)
        {
            Intent intent = new Intent(getApplicationContext(), tentang.class);
            startActivity(intent);
        }
        else if (id == R.id.nav_logout)
        {
            final AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Apa Anda yakin ingin Logout akun ini ?")
                    .setCancelable(true)
                    .setPositiveButton("Iya", new DialogInterface.OnClickListener() {
                        public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                            String tidakada = "Tidak Ada";

                            SharedPreferences.Editor editor = sharedPref.edit();
                            editor.putString(getString(R.string.p_id_pengguna), tidakada);
                            editor.putString(getString(R.string.p_sebagai), tidakada);
                            editor.putString(getString(R.string.p_nama), tidakada);
                            editor.putString(getString(R.string.p_nohp), tidakada);
                            editor.putString(getString(R.string.p_email), tidakada);
                            editor.putString(getString(R.string.p_password), tidakada);
                            editor.commit();

                            Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                            startActivity(i);
                            finish();

                            try
                            {
                                LoginManager.getInstance().logOut();
                            }
                            catch (Exception e)
                            {

                            }
                        }
                    })
                    .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                        public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                            dialog.cancel();
                        }
                    });
            final AlertDialog alert = builder.create();
            alert.show();
        }
        else if (id == R.id.nav_keluar)
        {
            final AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Apa Anda yakin ingin keluar dari aplikasi ?")
                    .setCancelable(true)
                    .setPositiveButton("Iya", new DialogInterface.OnClickListener() {
                        public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id)
                        {
                            finish();
                        }
                    })
                    .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                        public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                            dialog.cancel();
                        }
                    });
            final AlertDialog alert = builder.create();
            alert.show();
        }
        else if (id == R.id.nav_bantuan)
        {
            Intent intent = new Intent(getApplicationContext(), Fitur.class);
            startActivity(intent);
        }
        else if (id == R.id.nav_lokasifas)
        {
            LokasiLaporanFas.cc = getApplicationContext();
            Intent i = new Intent(getApplicationContext(), LokasiLaporanFas.class);
            i.putExtra("bagian", "fas");
            startActivity(i);
        }
        else if (id == R.id.nav_lokasiplgr)
        {
            LokasiLaporanFas.cc = getApplicationContext();
            Intent i = new Intent(getApplicationContext(), LokasiLaporanFas.class);
            i.putExtra("bagian", "plgr");
            startActivity(i);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    String LATLING;
    public static View VV;

    public static void TampilkanPesan(String pesan)
    {
        Snackbar.make(VV, pesan, Snackbar.LENGTH_LONG).setAction("Action", null).show();
    }
}
