package com.izarcsoftware.satpam;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.izarcsoftware.satpam.app.AndroidMultiPartEntity;
import com.izarcsoftware.satpam.app.Validation;

import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.jar.JarEntry;

@SuppressWarnings("ALL")
public class LFasilitasActivity extends AppCompatActivity
{
    private Button BAmbilFoto, BPilihFoto, BKirim;
    private ImageView IFoto;
    public TextView LFotoDiambil, LUkuranFoto, LPosisi, LAlamat;
    private LinearLayout LLFotoDiambil, LLUkuranFoto;
    private EditText TBKeluhan;
    private Spinner SPrioritas;
    private CheckBox CBRahasia;

    private String filePath;
    private Bitmap Gambar;
    private String lat, ling;
    private LocationManager locationManager;
    private String provider;
    private LocationListener locationListener;

    public static Context cc;

    private static int KODE_AMBIL_FOTO = 1;
    private static int KODE_PILIH_FOTO = 2;

    public static final int MEDIA_TYPE_IMAGE = 1;
    public static final int MEDIA_TYPE_VIDEO = 2;

    private boolean batalkan;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lfasilitas);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        cc = this;

        toolbar.setNavigationOnClickListener(new Toolbar.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                finish();
            }
        });

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        locationListener = new LocationListener()
        {
            @Override
            public void onLocationChanged(Location location)
            {
                new AmbilAlamat().execute(location);
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras)
            {

            }

            @Override
            public void onProviderEnabled(String provider)
            {

            }

            @Override
            public void onProviderDisabled(String provider)
            {

            }
        };

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if (isAllValid())
                {
                    if (akurasi > 50.0f)
                    {
                        final AlertDialog.Builder builder = new AlertDialog.Builder(cc);
                        builder.setMessage("Lokasi yang GPS Anda temukan tampaknya belum akurat, tetap lanjutkan ?")
                                .setCancelable(true)
                                .setPositiveButton("Iya", new DialogInterface.OnClickListener()
                                {
                                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                                        final AlertDialog.Builder builder = new AlertDialog.Builder(cc);
                                        builder.setMessage("Data yang Anda kirim akan dilaporkan ke pihak terkait (Polres Banyumas), harap dipastikan data yang Anda kirim benar, lanjutkan ?")
                                                .setCancelable(true)
                                                .setPositiveButton("Iya", new DialogInterface.OnClickListener()
                                                {
                                                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id)
                                                    {
                                                        String[] isi = AmbilIsian();
                                                        Intent i = new Intent(getApplicationContext(), Mengirim.class);
                                                        i.putExtra("filePath", filePath);
                                                        i.putExtra("isian", isi);
                                                        i.putExtra("bagian", "foto_laporanfas");
                                                        startActivity(i);
                                                        finish();
                                                    }
                                                })
                                                .setNegativeButton("Tidak", new DialogInterface.OnClickListener()
                                                {
                                                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id)
                                                    {
                                                        dialog.cancel();
                                                    }
                                                });
                                        final AlertDialog alert = builder.create();
                                        alert.show();
                                    }
                                })
                                .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                                        dialog.cancel();
                                    }
                                });
                        final AlertDialog alert = builder.create();
                        alert.show();
                    }
                    else
                    {
                        final AlertDialog.Builder builder = new AlertDialog.Builder(cc);
                        builder.setMessage("Data yang Anda kirim akan dilaporkan ke pihak terkait (Polres Banyumas), harap dipastikan data yang Anda kirim benar, lanjutkan ?")
                                .setCancelable(true)
                                .setPositiveButton("Iya", new DialogInterface.OnClickListener()
                                {
                                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id)
                                    {
                                        String[] isi = AmbilIsian();
                                        Intent i = new Intent(getApplicationContext(), Mengirim.class);
                                        i.putExtra("filePath", filePath);
                                        i.putExtra("isian", isi);
                                        i.putExtra("bagian", "foto_laporanfas");
                                        startActivity(i);
                                        finish();
                                    }
                                })
                                .setNegativeButton("Tidak", new DialogInterface.OnClickListener()
                                {
                                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id)
                                    {
                                        dialog.cancel();
                                    }
                                });
                        final AlertDialog alert = builder.create();
                        alert.show();
                    }
                }
            }
        });

        lat = "";
        ling = "";

        IFoto = (ImageView) findViewById(R.id.IFoto);
        BAmbilFoto = (Button) findViewById(R.id.BAmbilFoto);
        BPilihFoto = (Button) findViewById(R.id.BPilihFoto);
        BKirim = (Button) findViewById(R.id.BKirim);
        LFotoDiambil = (TextView) findViewById(R.id.LFotoDiambil);
        LUkuranFoto = (TextView) findViewById(R.id.LUkuranFoto);
        LPosisi = (TextView) findViewById(R.id.LPosisi);
        LAlamat = (TextView) findViewById(R.id.LAlamat);
        LLFotoDiambil = (LinearLayout) findViewById(R.id.LLFotoDiambil);
        LLUkuranFoto = (LinearLayout) findViewById(R.id.LLUkuranFoto);
        TBKeluhan = (EditText) findViewById(R.id.TBKeluhan);
        CBRahasia = (CheckBox) findViewById(R.id.CBRahasia);
        SPrioritas = (Spinner) findViewById(R.id.SPrioritasfas);

        BAmbilFoto.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                openCamera();
            }
        });

        BPilihFoto.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                openGalerry();
            }
        });

        BKirim.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (isAllValid())
                {
                    if (akurasi > 50.0f)
                    {
                        final AlertDialog.Builder builder = new AlertDialog.Builder(cc);
                        builder.setMessage("Lokasi yang GPS Anda temukan tampaknya belum akurat, tetap lanjutkan ?")
                                .setCancelable(true)
                                .setPositiveButton("Iya", new DialogInterface.OnClickListener()
                                {
                                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                                        final AlertDialog.Builder builder = new AlertDialog.Builder(cc);
                                        builder.setMessage("Data yang Anda kirim akan dilaporkan ke pihak terkait (Polres Banyumas), harap dipastikan data yang Anda kirim benar, lanjutkan ?")
                                                .setCancelable(true)
                                                .setPositiveButton("Iya", new DialogInterface.OnClickListener()
                                                {
                                                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id)
                                                    {
                                                        String[] isi = AmbilIsian();
                                                        Intent i = new Intent(getApplicationContext(), Mengirim.class);
                                                        i.putExtra("filePath", filePath);
                                                        i.putExtra("isian", isi);
                                                        i.putExtra("bagian", "foto_laporanfas");
                                                        startActivity(i);
                                                        finish();
                                                    }
                                                })
                                                .setNegativeButton("Tidak", new DialogInterface.OnClickListener()
                                                {
                                                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id)
                                                    {
                                                        dialog.cancel();
                                                    }
                                                });
                                        final AlertDialog alert = builder.create();
                                        alert.show();
                                    }
                                })
                                .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                                        dialog.cancel();
                                    }
                                });
                        final AlertDialog alert = builder.create();
                        alert.show();
                    }
                    else
                    {
                        final AlertDialog.Builder builder = new AlertDialog.Builder(cc);
                        builder.setMessage("Data yang Anda kirim akan dilaporkan ke pihak terkait (Polres Banyumas), harap dipastikan data yang Anda kirim benar, lanjutkan ?")
                                .setCancelable(true)
                                .setPositiveButton("Iya", new DialogInterface.OnClickListener()
                                {
                                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id)
                                    {
                                        String[] isi = AmbilIsian();
                                        Intent i = new Intent(getApplicationContext(), Mengirim.class);
                                        i.putExtra("filePath", filePath);
                                        i.putExtra("isian", isi);
                                        i.putExtra("bagian", "foto_laporanfas");
                                        startActivity(i);
                                        finish();
                                    }
                                })
                                .setNegativeButton("Tidak", new DialogInterface.OnClickListener()
                                {
                                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id)
                                    {
                                        dialog.cancel();
                                    }
                                });
                        final AlertDialog alert = builder.create();
                        alert.show();
                    }
                }
            }
        });

        BPilihFoto.requestFocus();

        try
        {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
        } catch (Exception e)
        {}

        try
        {
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
        } catch (Exception e)
        {}

        if ( !locationManager.isProviderEnabled( LocationManager.GPS_PROVIDER ) ) {
            buildAlertMessageNoGps();
        }

        if (savedInstanceState != null)
        {
            filePath = savedInstanceState.getString("filePath");
            Gambar = ImageHandler.decodeSampledBitmapFromFile(filePath, 500, 500);
            IFoto.setImageBitmap(Gambar);
        }
    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("GPS di perangkat Anda tampaknya mati atau mode akurasi Lokasi Anda diset bukan Akurasi Tinggi, apakah Anda ingin mengaturnya ?")
                .setCancelable(true)
                .setPositiveButton("Iya", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    /* Request updates at startup */
    @Override
    protected void onResume()
    {
        super.onResume();

        try
        {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
        } catch (Exception e)
        {}

        try
        {
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
        } catch (Exception e)
        {}

        if ( !locationManager.isProviderEnabled( LocationManager.GPS_PROVIDER ) ) {
            buildAlertMessageNoGps();
        }

        if (!(getApplicationContext().getPackageManager().hasSystemFeature(PackageManager.FEATURE_LOCATION_GPS) || getApplicationContext().getPackageManager().hasSystemFeature(PackageManager.FEATURE_LOCATION_NETWORK) || getApplicationContext().getPackageManager().hasSystemFeature(PackageManager.FEATURE_LOCATION)))
        {
            Toast.makeText(getApplication(), "Tidak ditemukan GPS, tidak dapat melanjutkan.", Toast.LENGTH_LONG).show();
        }
    }

    /* Remove the locationlistener updates when Activity is paused */
    @Override
    protected void onPause()
    {
        super.onPause();
        locationManager.removeUpdates(locationListener);
    }

    private Uri fileUri;

    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    private static File getOutputMediaFile(int type)
    {
        File mediaStorageDir2 = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "Satpam File Upload");
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "Laporan E-Complaint");

        // Create the storage directory if it does not exist
        if (mediaStorageDir2.exists())
        {
            if (!mediaStorageDir2.renameTo(mediaStorageDir))
            {
            }
        }

        if (!mediaStorageDir.exists())
        {
            if (!mediaStorageDir.mkdirs())
            {
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } else if (type == MEDIA_TYPE_VIDEO) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "VID_" + timeStamp + ".mp4");
        } else {
            return null;
        }

        return mediaFile;
    }

    private void openCamera() {
        if (getApplicationContext().getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA))
        {
            Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
            fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
            startActivityForResult(intent, KODE_AMBIL_FOTO);
        }
        else
        {
            Toast.makeText(getApplication(), "Tidak ditemukan adanya kamera.", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        try
        {
            outState.putParcelable("file_uri", fileUri);
            outState.putString("filePath", filePath);
        }
        catch (Exception e)
        {}
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        try
        {
            fileUri = savedInstanceState.getParcelable("file_uri");
            filePath = savedInstanceState.getParcelable("filePath");
        }
        catch (Exception e)
        {}
    }

    private void openGalerry() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Pilih Foto"), KODE_PILIH_FOTO);
    }

    private Date lastModifiedDate;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == RESULT_OK)
        {
            if (requestCode == KODE_AMBIL_FOTO)
            {
                try
                {
                    Gambar = ImageHandler.decodeSampledBitmapFromFile(fileUri.getPath(), 500, 500);
                    IFoto.setImageBitmap(Gambar);
                    filePath = fileUri.getPath();

                    LLFotoDiambil.setVisibility(View.GONE);
                    LLUkuranFoto.setVisibility(View.VISIBLE);
                }
                catch (Exception ex)
                {
                    Toast.makeText(getApplicationContext(), ex.toString(), Toast.LENGTH_LONG).show();
                }
            }
            else if (requestCode == KODE_PILIH_FOTO && data != null && data.getData() != null)
            {
                try
                {
                    Uri pickedImage = data.getData();
                    String[] filePath2 = { MediaStore.Images.Media.DATA };
                    Cursor cursor = getContentResolver().query(pickedImage, filePath2, null, null, null);
                    cursor.moveToFirst();
                    filePath = cursor.getString(cursor.getColumnIndex(filePath2[0]));

                    if (filePath == null)
                    {
                        filePath = RealPath.getRealPathFromURI(getApplicationContext(), data.getData());
                    }

                    File file = new File(filePath);
                    long lastModified = file.lastModified();
                    lastModifiedDate = new Date(lastModified);

                    Gambar = ImageHandler.decodeSampledBitmapFromFile(filePath, 500, 500);
                    IFoto.setImageBitmap(Gambar);

                    SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy");
                    Calendar cal = Calendar.getInstance();
                    sdf.setTimeZone(cal.getTimeZone());

                    LFotoDiambil.setText(": " + sdf.format(lastModifiedDate));
                    LLFotoDiambil.setVisibility(View.VISIBLE);
                    LLUkuranFoto.setVisibility(View.VISIBLE);
                }
                catch (Exception ex)
                {
                    Toast.makeText(getApplicationContext(), ex.toString(), Toast.LENGTH_LONG).show();
                }
            }

            AndroidMultiPartEntity entity = new AndroidMultiPartEntity(
                    new AndroidMultiPartEntity.ProgressListener() {
                        @Override
                        public void transferred(long num) {
                        }
                    });

            try
            {
                File sourceFile = new File(filePath);
                entity.addPart("image", new FileBody(sourceFile));
            }
            catch (Exception e)
            {
            }

            LUkuranFoto.setText(": " + String.valueOf(entity.getContentLength() / 1024) + " KB");
        }
    }

    private boolean isAllValid()
    {
        Boolean valid = true;

        if (Gambar == null)
        {
            Snackbar.make(IFoto, "Sebagai bukti, kami memerlukan adanya foto.", Snackbar.LENGTH_LONG).setAction("Action", null).show();
            valid = false;
        }
        else if (TextUtils.isEmpty(TBKeluhan.getText().toString()))
        {
            TBKeluhan.setError(getString(R.string.error_field_required));
            TBKeluhan.requestFocus();
            valid = false;
        }
        else if (LPosisi.getText().toString().equalsIgnoreCase("Menemukan posisi..."))
        {
            Snackbar.make(LPosisi, "Menunggu lokasi...", Snackbar.LENGTH_LONG).setAction("Action", null).show();
            valid = false;
        }
        /*else if (SPrioritas.getSelectedItem().toString().equalsIgnoreCase("Pilih Prioritas"))
        {
            Snackbar.make(LPosisi, "Prioritas harap diisi", Snackbar.LENGTH_LONG).setAction("Action", null).show();
            valid = false;
        }*/
        else if (!Validation.isSopan(TBKeluhan.getText().toString()))
        {
            TBKeluhan.setError(getString(R.string.error_tidak_sopan));
            TBKeluhan.requestFocus();
            valid = false;
        }
        else
        {
            try
            {
                if (Calendar.getInstance().getTime().compareTo(lastModifiedDate) > 120)
                {
                    Snackbar.make(LFotoDiambil, "Foto terlalu usang", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                    valid = false;
                }
            }
            catch (Exception e)
            {

            }
        }

        return valid;
    }

    SharedPreferences sharedPref;

    public String[] AmbilIsian()
    {
        sharedPref = getSharedPreferences("File_Pengaturan", Context.MODE_PRIVATE);

        String[] Isian = {"", "", "", "", "", "", "", "", "", "", ""};

        Isian[0] = "";
        Isian[1] = TBKeluhan.getText().toString();
        Isian[2] = lat;
        Isian[3] = ling;
        Isian[4] = LPosisi.getText().toString();
        Isian[5] = "Tinggi";
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String formattedDate = df.format(cal.getTime());
        Isian[6] = formattedDate;
        Isian[7] = sharedPref.getString(getString(R.string.p_id_pengguna), "Tidak Ada");
        if (CBRahasia.isChecked())
        {
            Isian[8] = "1";
        }
        else
        {
            Isian[8] = "0";
        }
        Isian[9] = "";

        return  Isian;
    }

    public float akurasi = Float.MAX_VALUE;

    class AmbilAlamat extends AsyncTask<Location, Void, String[]>
    {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String[] s)
        {
            super.onPostExecute(s);
            if (!s[0].equalsIgnoreCase("LOWER"))
            {
                lat = s[1];
                ling = s[2];
                LPosisi.setText(s[0]);
                LAlamat.setText("Ketepatan lokasi " + s[3] + " meter");
                LAlamat.setVisibility(View.VISIBLE);
            }
        }

        @Override
        protected String[] doInBackground(Location... params) {
            Location location = params[0];

            location.getLatitude();
            location.getLongitude();

            String[] result = {"", "", "", ""};
            Geocoder geocoder = new Geocoder(getBaseContext());
            if (location.getAccuracy() < akurasi)
            {
                try
                {
                    List<Address> addressList = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
                    String add = "";
                    if (addressList.size() > 0)
                    {
                        for (int i = 0; i < addressList.get(0).getMaxAddressLineIndex(); i++)
                        {
                            if (i == addressList.get(0).getMaxAddressLineIndex() - 1)
                            {
                                add += addressList.get(0).getAddressLine(i);
                            }
                            else
                            {
                                add += addressList.get(0).getAddressLine(i) + ", ";
                            }
                        }
                    }

                    akurasi = location.getAccuracy();

                    result[0] = add;
                    result[1] = String.valueOf(location.getLatitude());
                    result[2] = String.valueOf(location.getLongitude());
                    result[3] = String.valueOf(location.getAccuracy());
                    return result;
                }
                catch (Exception e)
                {
                    result[0] = "Lat.: " + String.valueOf(location.getLatitude()) + ", Long.: " + String.valueOf(location.getLongitude());
                    result[1] = String.valueOf(location.getLatitude());
                    result[2] = String.valueOf(location.getLongitude());
                    result[3] = String.valueOf(location.getAccuracy());
                    return result;
                }
            }
            else
            {
                result[0] = "LOWER";
                return result;
            }
        }
    }
}
