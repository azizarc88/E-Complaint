package com.izarcsoftware.satpam;

import android.app.IntentService;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;

import com.izarcsoftware.satpam.data.ChatItem;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;

/**
 * Created by Ampu2 on 06/04/2016.
 */

public class PengirimanData extends IntentService
{
    String bdari, buntuk, bisipesan, bwaktu, bposisi;

    public PengirimanData() {
        super("PengirimanData");
    }

    @Override
    public void onCreate() {
    }

    @Override
    protected void onHandleIntent(Intent intent) {

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        if (intent != null)
        {
            bdari = intent.getStringExtra("dari");
            buntuk = intent.getStringExtra("untuk");
            bisipesan = intent.getStringExtra("isipesan");
            bwaktu = intent.getStringExtra("waktu");
            bposisi = intent.getStringExtra("posisi");

            new ObrolanManager(getApplicationContext()).execute(bdari, buntuk, bisipesan, bwaktu, bposisi);
        }
        return Service.START_STICKY;
    }

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    @Override
    public void onDestroy() {
    }

    class ObrolanManager extends AsyncTask<String, Void, String>
    {
        private Context context;
        private String  dari, untuk, isipesan, waktu, posisi;

        public ObrolanManager(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... arg0)
        {
            RequestHandler rh = new RequestHandler();

            String link;
            String result;

            try
            {
                HashMap<String,String> data = new HashMap<>();
                data.put("dari", arg0[0]);
                dari = arg0[0];
                data.put("untuk", arg0[1]);
                untuk = arg0[1];
                data.put("isipesan", arg0[2]);
                isipesan = arg0[2];
                data.put("waktu", arg0[3]);
                waktu = arg0[3];
                posisi = arg0[4];


                link = getApplicationContext().getString(R.string.alamat_server) + getApplicationContext().getString(R.string.link_tambah_obrolan);

                result = rh.sendPostRequest(link, data);

                return result;
            } catch (Exception e)
            {
                return new String("Exception: " + e.getMessage());
            }
        }

        @Override
        protected void onPostExecute(String result)
        {
            String jsonStr = result;
            if (jsonStr != null)
            {
                try
                {
                    JSONObject jsonObj = new JSONObject(jsonStr);
                    String query_result = jsonObj.getString("hasil");
                    if (query_result.equals("SUKSES"))
                    {
                        ChatItem item = new ChatItem();
                        item.setDari(dari);
                        item.setUntuk(untuk);
                        item.setIsipesan(isipesan);
                        item.setWaktu(waktu);
                        item.setPending(false);
                        int pos = Integer.valueOf(posisi);
                        RuangObrolanActivity.chatItems.remove(pos);
                        RuangObrolanActivity.chatItems.add(pos, item);

                        RuangObrolanActivity.listAdapter.notifyDataSetChanged();

                        Intent intent = new Intent(getBaseContext(), GCMHandle.class);
                        intent.putExtra("tipe", "obrolan");
                        intent.putExtra("dari", dari);
                        intent.putExtra("untuk", untuk);
                        intent.putExtra("isipesan", isipesan);
                        startService(intent);
                    } else if (query_result.equals("GAGAL"))
                    {
                    }
                } catch (JSONException e)
                {
                    new ObrolanManager(context).execute(dari, untuk, isipesan, waktu, posisi);
                }
            } else
            {
                new ObrolanManager(context).execute(dari, untuk, isipesan, waktu, posisi);
            }
        }
    }
}
