package com.izarcsoftware.satpam;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.animation.LayoutAnimationController;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.izarcsoftware.satpam.app.AndroidMultiPartEntity;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.logging.LogManager;

public class Mengirim extends AppCompatActivity
{
    private static final String TAG = MainActivity.class.getSimpleName();

    private ProgressBar progressBar;
    private String filePath, bagian = null;
    private Context context;
    private TextView txtPercentage, LCur;
    private Button BKirimUlang;
    long totalSize = 0;
    String[] isian;
    private boolean fotosukses, datasukses = false;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mengirim);
        txtPercentage = (TextView) findViewById(R.id.txtPercentage);
        LCur = (TextView) findViewById(R.id.LCur);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        BKirimUlang = (Button) findViewById(R.id.BKirimUlang);
        BKirimUlang.setOnClickListener(new Button.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (fotosukses)
                {
                    if (bagian.equalsIgnoreCase("foto_laporanfas"))
                    {
                        new LaporanFasilitasManager(context).execute(isian[0], isian[1], isian[2], isian[3], isian[4], isian[5], isian[6], isian[7], isian[8], isian[9]);
                    } else
                    {
                        new LaporanPelanggaranManager(context).execute(isian[0], isian[1], isian[2], isian[3], isian[4], isian[5], isian[6], isian[7], isian[8], isian[9]);
                    }
                } else
                {
                    new UploadFileToServer().execute();
                }
            }
        });
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Intent i = getIntent();
        isian = i.getStringArrayExtra("isian");
        filePath = i.getStringExtra("filePath");
        bagian = i.getStringExtra("bagian");

        if (filePath == null)
        {
            Toast.makeText(getApplicationContext(), "Maaf, file gambar tidak ditemukan.", Toast.LENGTH_LONG).show();
        }

        context = this.getApplicationContext();

        new UploadFileToServer().execute();
    }

    private class UploadFileToServer extends AsyncTask<Void, Integer, String>
    {
        @Override
        protected void onPreExecute() {
            // setting progress bar to zero
            progressBar.setProgress(0);
            txtPercentage.setText("0%");
            LCur.setText(String.valueOf("0 KB / " + String.valueOf(totalSize / 1024) + " KB"));
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            // Making progress bar visible
            progressBar.setVisibility(View.VISIBLE);

            // updating progress bar value
            progressBar.setProgress((int) ((progress[0] / (float) totalSize) * 100));

            // updating percentage value
            int nilai = (int) ((progress[0] / (float) totalSize) * 100);
            txtPercentage.setText(String.valueOf(nilai) + "%");

            LCur.setText(String.valueOf(progress[0] / 1024) + " KB / " + String.valueOf(totalSize / 1024) + " KB");
        }

        @Override
        protected String doInBackground(Void... params) {
            return uploadFile();
        }

        @SuppressWarnings("deprecation")
        private String uploadFile() {
            String responseString = null;

            HttpParams httpParameters = new BasicHttpParams();
            int timeoutConnection = 30000;
            HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
            int timeoutSocket = 30000;
            HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);

            HttpClient httpclient = new DefaultHttpClient(httpParameters);
            HttpPost httppost = new HttpPost(getString(R.string.alamat_server) + getString(R.string.link_upload_file));

            try {
                AndroidMultiPartEntity entity = new AndroidMultiPartEntity(
                        new AndroidMultiPartEntity.ProgressListener() {
                            @Override
                            public void transferred(long num) {
                                publishProgress((int) num);
                            }
                        });

                File sourceFile = new File(filePath);

                Calendar cal = Calendar.getInstance();
                SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd_HHmmssSSS");
                String formattedDate = df.format(cal.getTime());
                isian[9] = isian[7] + "_" + formattedDate + ".jpg";

                entity.addPart("image", new FileBody(sourceFile));
                entity.addPart("namafile", new StringBody(isian[9]));
                entity.addPart("bagian", new StringBody(bagian));

                totalSize = entity.getContentLength();
                httppost.setEntity(entity);

                HttpResponse response = httpclient.execute(httppost);
                HttpEntity r_entity = response.getEntity();

                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode == 200) {
                    responseString = EntityUtils.toString(r_entity);
                } else {
                    responseString = "Error occurred! Http Status Code: "
                            + statusCode;
                }

            } catch (ClientProtocolException e) {
                responseString = e.toString();
            } catch (IOException e) {
                responseString = e.toString();
            }
            return responseString;
        }

        @Override
        protected void onPostExecute(String result)
        {
            try
            {
                String jsonStr = result;
                if (jsonStr != null)
                {
                    try
                    {
                        JSONObject jsonObj = new JSONObject(jsonStr);
                        String query_result = jsonObj.getString("hasil");
                        if (query_result.equals("SUKSES"))
                        {
                            fotosukses = true;

                            if (bagian.equalsIgnoreCase("foto_laporanfas"))
                            {
                                new LaporanFasilitasManager(context).execute(isian[0], isian[1], isian[2], isian[3], isian[4], isian[5], isian[6], isian[7], isian[8], isian[9]);
                            }
                            else
                            {
                                new LaporanPelanggaranManager(context).execute(isian[0], isian[1], isian[2], isian[3], isian[4], isian[5], isian[6], isian[7], isian[8], isian[9]);
                            }
                        }
                        else if (query_result.equals("GAGAL"))
                        {
                            String pesan = jsonObj.getString("pesan");
                            Snackbar.make(progressBar, "Pengiriman gagal, silakan coba lagi. " + pesan + " - foto", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                            BKirimUlang.setVisibility(View.VISIBLE);
                        }
                        else
                        {
                            Toast.makeText(getApplicationContext(), result  + " - foto", Toast.LENGTH_LONG).show();
                            BKirimUlang.setVisibility(View.VISIBLE);
                        }
                    }
                    catch (Exception e)
                    {
                        String pesan = e.getMessage();
                        Snackbar.make(progressBar, "Pengiriman gagal, periksa koneksi Anda. " + pesan + " - foto", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                        BKirimUlang.setVisibility(View.VISIBLE);
                    }
                }
            }
            catch (Exception e)
            {
                String pesan = e.getMessage();
                Snackbar.make(progressBar, "Terjadi kesalahan, silakan coba lagi. " + pesan + " - foto", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                BKirimUlang.setVisibility(View.VISIBLE);
            }
            super.onPostExecute(result);
        }
    }

    class LaporanFasilitasManager extends AsyncTask<String, Void, String>
    {
        private Context context;

        public LaporanFasilitasManager(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... arg0)
        {
            RequestHandler rh = new RequestHandler();

            String link;
            String result;

            try
            {
                HashMap<String,String> data = new HashMap<>();
                data.put("keluhan", arg0[1]);
                data.put("lat", arg0[2]);
                data.put("ling", arg0[3]);
                data.put("alamat", arg0[4]);
                data.put("prioritas", arg0[5]);
                data.put("dilaporkanpada", arg0[6]);
                data.put("dilaporkanoleh", arg0[7]);
                data.put("laporanrahasia", arg0[8]);
                data.put("namafile", arg0[9]);
                data.put("foto", arg0[0]);

                link = getString(R.string.alamat_server) + getString(R.string.link_tambah_laporanfas);

                result = rh.sendPostRequest(link, data);

                return result;
            } catch (Exception e)
            {
                return new String("Exception: " + e.getMessage());
            }
        }

        @Override
        protected void onPostExecute(String result)
        {
            try
            {
                String jsonStr = result;
                if (jsonStr != null)
                {
                    try
                    {
                        JSONObject jsonObj = new JSONObject(jsonStr);
                        String query_result = jsonObj.getString("hasil");
                        if (query_result.equals("SUKSES"))
                        {
                            txtPercentage.setText("100%");
                            datasukses = true;
                            finish();
                            try
                            {
                                MainActivity.TampilkanPesan("Terimakasih telah mengirimkan sebuah informasi kepada kami.");
                            }
                            catch (Exception e)
                            {
                                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                                startActivity(i);
                                MainActivity.TampilkanPesan("Terimakasih telah mengirimkan sebuah informasi kepada kami.");
                            }
                        } else if (query_result.equals("GAGAL"))
                        {
                            Snackbar.make(progressBar, "Pengiriman gagal, silakan coba lagi. - data", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                            BKirimUlang.setVisibility(View.VISIBLE);
                        } else if (query_result.equals("SUDAH ADA"))
                        {
                            finish();
                            MainActivity.TampilkanPesan("Terimakasih telah mengirimkan sebuah informasi kepada kami.");
                        } else
                        {
                            Snackbar.make(progressBar, "Tidak dapat terhubung, periksa koneksi Anda. " + jsonStr.toString() + " - data", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                            BKirimUlang.setVisibility(View.VISIBLE);
                        }
                    } catch (JSONException e)
                    {
                        e.printStackTrace();
                        Toast.makeText(context, "Tidak dapat terhubung ke database, periksa koneksi internet kamu. " + jsonStr.toString() + " - data", Toast.LENGTH_LONG).show();
                        BKirimUlang.setVisibility(View.VISIBLE);
                    }
                } else
                {
                    Snackbar.make(progressBar, "Terjadi kesalahan, silakan coba lagi. - data", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                    BKirimUlang.setVisibility(View.VISIBLE);
                }
            }
            catch (Exception e)
            {
                Toast.makeText(context, "Terjadi kesalahan. " + e.toString() + " - data", Toast.LENGTH_LONG).show();
                BKirimUlang.setVisibility(View.VISIBLE);
            }
        }
    }

    class LaporanPelanggaranManager extends AsyncTask<String, Void, String>
    {
        private Context context;

        public LaporanPelanggaranManager (Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... arg0)
        {
            RequestHandler rh = new RequestHandler();

            String link;
            String result;

            try
            {
                HashMap<String,String> data = new HashMap<>();
                data.put("pelanggaran", arg0[1]);
                data.put("lat", arg0[2]);
                data.put("ling", arg0[3]);
                data.put("alamat", arg0[4]);
                data.put("prioritas", arg0[5]);
                data.put("dilaporkanpada", arg0[6]);
                data.put("dilaporkanoleh", arg0[7]);
                data.put("laporanrahasia", arg0[8]);
                data.put("namafile", arg0[9]);
                data.put("foto", arg0[0]);

                link = getString(R.string.alamat_server) + getString(R.string.link_tambah_laporanplgr);

                result = rh.sendPostRequest(link, data);

                return result;
            } catch (Exception e)
            {
                return new String("Exception: " + e.getMessage());
            }
        }

        @Override
        protected void onPostExecute(String result)
        {
            try
            {
                String jsonStr = result;
                if (jsonStr != null)
                {
                    try
                    {
                        JSONObject jsonObj = new JSONObject(jsonStr);
                        String query_result = jsonObj.getString("hasil");
                        if (query_result.equals("SUKSES"))
                        {
                            finish();
                            try
                            {
                                MainActivity.TampilkanPesan("Terimakasih telah melaporkan masalah pelanggaran kepada kami.");
                            }
                            catch (Exception e)
                            {
                                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                                startActivity(i);
                                MainActivity.TampilkanPesan("Terimakasih telah melaporkan masalah pelanggaran kepada kami.");
                            }
                        } else if (query_result.equals("GAGAL"))
                        {
                            Snackbar.make(progressBar, "Pengiriman gagal, silakan coba lagi. - data", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                            BKirimUlang.setVisibility(View.VISIBLE);
                        } else if (query_result.equals("SUDAH ADA"))
                        {
                            finish();
                            MainActivity.TampilkanPesan("Terimakasih telah melaporkan masalah pelanggaran polisi kepada kami.");
                        } else
                        {
                            Snackbar.make(progressBar, "Tidak dapat terhubung, periksa koneksi Anda. " + jsonStr.toString() + " - data", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                            BKirimUlang.setVisibility(View.VISIBLE);
                        }
                    } catch (JSONException e)
                    {
                        e.printStackTrace();
                        Toast.makeText(context, "Tidak dapat terhubung ke database, periksa koneksi internet kamu. " + jsonStr.toString() + " - data", Toast.LENGTH_LONG).show();
                        BKirimUlang.setVisibility(View.VISIBLE);
                    }
                } else
                {
                    Snackbar.make(progressBar, "Terjadi kesalahan, silakan coba lagi." + " - data", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                    BKirimUlang.setVisibility(View.VISIBLE);
                }
            }
            catch (Exception e)
            {
                Toast.makeText(context, "Terjadi kesalahan. " + e.toString(), Toast.LENGTH_LONG).show();
                BKirimUlang.setVisibility(View.VISIBLE);
            }
        }
    }
}
