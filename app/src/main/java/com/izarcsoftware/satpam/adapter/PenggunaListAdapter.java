package com.izarcsoftware.satpam.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.izarcsoftware.satpam.R;
import com.izarcsoftware.satpam.data.KomentarItem;
import com.izarcsoftware.satpam.data.PenggunaItem;

import java.util.List;

/**
 * Created by Aziz Nur Ariffianto on 0010, 10 Feb 2016.
 */
public class PenggunaListAdapter extends BaseAdapter
{
    private Activity activity;
    private LayoutInflater inflater;
    private List<PenggunaItem> penggunaItems;
    public static Context cc;

    public PenggunaListAdapter(List<PenggunaItem> penggunaItems, Activity activity)
    {
        this.penggunaItems = penggunaItems;
        this.activity = activity;
    }

    @Override
    public int getCount()
    {
        return penggunaItems.size();
    }

    @Override
    public Object getItem(int position)
    {
        return penggunaItems.get(position);
    }

    @Override
    public long getItemId(int position)
    {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        if (inflater == null)
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.pengguna_item, null);

        TextView LNamaPengguna = (TextView) convertView.findViewById(R.id.LNamaPengguna);
        TextView LTotalItem = (TextView) convertView.findViewById(R.id.LTotalItem);
        TextView LNomor = (TextView) convertView.findViewById(R.id.LNomor);

        final PenggunaItem item = penggunaItems.get(position);

        String str = item.getNama();
        String[] strArray = str.split(" ");
        StringBuilder builder = new StringBuilder();
        for (String s : strArray) {
            if (s.length() > 0)
            {
                String cap = s.substring(0, 1).toUpperCase() + s.substring(1).toLowerCase();
                builder.append(cap + " ");
            }
        }

        LNamaPengguna.setText(builder.toString());
        LTotalItem.setText("Total Laporan: " + item.getTotallaporan());
        LNomor.setText(String.valueOf(item.getNomor()) + ".");

        return convertView;
    }

    @Override
    public boolean isEnabled(int position) {
        return false;
    }
}
