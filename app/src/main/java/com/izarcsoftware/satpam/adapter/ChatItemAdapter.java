package com.izarcsoftware.satpam.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.izarcsoftware.satpam.R;
import com.izarcsoftware.satpam.data.ChatItem;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by Aziz Nur Ariffianto on 0011, 11 Feb 2016.
 */
public class ChatItemAdapter extends BaseAdapter
{
    private Activity activity;
    private LayoutInflater inflater;
    private List<ChatItem> chatItems;
    public static Context cc;

    public ChatItemAdapter(List<ChatItem> chatItems, Activity activity)
    {
        this.chatItems = chatItems;
        this.activity = activity;
    }

    @Override
    public int getCount()
    {
        return chatItems.size();
    }

    @Override
    public Object getItem(int position)
    {
        return chatItems.get(position);
    }

    @Override
    public long getItemId(int position)
    {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        if (inflater == null)
        {
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        if (convertView == null)
        {
            convertView = inflater.inflate(R.layout.chat_item, null);
        }

        TextView LIsiPesan = (TextView) convertView.findViewById(R.id.LIsiPesan);
        TextView LWaktuPesan = (TextView) convertView.findViewById(R.id.LWaktuPesan);
        LinearLayout LLObrolan = (LinearLayout) convertView.findViewById(R.id.LLObrolan);
        LinearLayout LLChat = (LinearLayout) convertView.findViewById(R.id.LLChat);

        final ChatItem item = chatItems.get(position);
        ChatItem itemafter = null;

        if (!(chatItems.size() - 1 < position + 1))
        {
            itemafter = chatItems.get(position + 1);

            if (itemafter.getWaktu().substring(0, 16).equalsIgnoreCase(item.getWaktu().substring(0, 16)) && itemafter.getDari().equalsIgnoreCase(item.getDari()))
            {
                LWaktuPesan.setVisibility(View.GONE);
            }
            else
            {
                LWaktuPesan.setVisibility(View.VISIBLE);
            }
        }
        else
        {
            LWaktuPesan.setVisibility(View.VISIBLE);
        }

        SharedPreferences sharedPref;
        sharedPref = cc.getSharedPreferences("File_Pengaturan", Context.MODE_PRIVATE);
        String id = sharedPref.getString("p_id_pengguna", "Tidak Ada");

        if (id.equalsIgnoreCase(item.getDari()))
        {
            if (item.isPending())
            {
                LLChat.setBackgroundResource(R.drawable.bg_obrolan_pending);
            }
            else
            {
                LLChat.setBackgroundResource(R.drawable.bg_obrolan_kanan);
            }
            LLObrolan.setGravity(Gravity.RIGHT);
            LWaktuPesan.setGravity(Gravity.RIGHT);

            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) LLChat.getLayoutParams();
            params.leftMargin = 50;
            params.rightMargin = 0;
            LLChat.setLayoutParams(params);
        }
        else
        {
            LLChat.setBackgroundResource(R.drawable.bg_obrolan_kiri);
            LLObrolan.setGravity(Gravity.LEFT);
            LWaktuPesan.setGravity(Gravity.LEFT);

            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) LLChat.getLayoutParams();
            params.leftMargin = 0;
            params.rightMargin = 50;
            LLChat.setLayoutParams(params);
        }

        LIsiPesan.setText(item.getIsipesan());

        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf;
        sdf = new SimpleDateFormat("dd MMMM yyyy  hh:mm a");
        sdf.setTimeZone(cal.getTimeZone());

        String expectedPattern = "yyyy-MM-dd HH:mm:ss";
        SimpleDateFormat formatter = new SimpleDateFormat(expectedPattern);
        String userInput = item.getWaktu();
        Date tanggal = null;
        try
        {
            tanggal = formatter.parse(userInput);
        } catch (ParseException e)
        {
            e.printStackTrace();
        }

        if (tanggal != null)
        {
            long selisih = (cal.getTime().getTime() / 100000000) - (tanggal.getTime() / 100000000);
            if (selisih == 0)
            {
                sdf = new SimpleDateFormat("hh:mm a");
            }
            else if(selisih < 6)
            {
                sdf = new SimpleDateFormat("EEEE, hh:mm a");
            }
            else
            {
                sdf = new SimpleDateFormat("dd MMMM yyyy  hh:mm a");
            }
        }

        LWaktuPesan.setText(sdf.format(tanggal));

        return convertView;
    }

    @Override
    public boolean isEnabled(int position) {
        return false;
    }
}
