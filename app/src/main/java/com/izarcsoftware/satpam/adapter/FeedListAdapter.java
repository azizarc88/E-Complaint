package com.izarcsoftware.satpam.adapter;

import com.izarcsoftware.satpam.BerandaActivity;
import com.izarcsoftware.satpam.FeedImageView;
import com.izarcsoftware.satpam.FotoActivity;
import com.izarcsoftware.satpam.FragmentFasilitas;
import com.izarcsoftware.satpam.FragmentPelanggaran;
import com.izarcsoftware.satpam.Komentar;
import com.izarcsoftware.satpam.MainActivity;
import com.izarcsoftware.satpam.MapsActivity;
import com.izarcsoftware.satpam.PemberitahuanLangsung;
import com.izarcsoftware.satpam.R;
import com.izarcsoftware.satpam.RequestHandler;
import com.izarcsoftware.satpam.app.AppController;
import com.izarcsoftware.satpam.app.ResourcesManager;
import com.izarcsoftware.satpam.app.Satpam;
import com.izarcsoftware.satpam.data.FeedItem;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.UserDictionary;
import android.support.design.widget.Snackbar;
import android.text.Html;
import android.text.InputType;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.text.method.LinkMovementMethod;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import org.json.JSONException;
import org.json.JSONObject;


/**
 * Created by Aziz Nur Ariffianto on 0005, 05 Feb 2016.
 */
public class FeedListAdapter extends BaseAdapter {
    private Activity activity;
    private LayoutInflater inflater;
    private List<FeedItem> feedItems;
    ImageLoader imageLoader = AppController.getInstance().getImageLoader();
    public static final String LATLING = "";
    public static final String ID = "";
    public static Context cc;
    public static String id, nama, sebagai;

    public FeedListAdapter(Activity activity, List<FeedItem> feedItems) {
        this.activity = activity;
        this.feedItems = feedItems;
    }

    @Override
    public int getCount() {
        return feedItems.size();
    }

    @Override
    public Object getItem(int location) {
        return feedItems.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void Tampilkan(final Boolean tampilkan, final Button tombol, String teks, final int indeks, final String bagian)
    {
        tombol.setText(teks);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2)
        {
            tombol.animate().setDuration(100).alpha(!tampilkan ? 0 : 1).setListener(new AnimatorListenerAdapter()
            {
                @Override
                public void onAnimationEnd(Animator animation)
                {
                    if (bagian.equalsIgnoreCase("fas"))
                    {
                        FragmentFasilitas.refreshContent(indeks);
                    }
                    else
                    {
                        FragmentPelanggaran.refreshContent(indeks);
                    }
                    tombol.setVisibility(!tampilkan ? View.INVISIBLE : View.VISIBLE);
                    tombol.setEnabled(true);
                }
            });
        }
        else
        {
            if (bagian.equalsIgnoreCase("fas"))
            {
                FragmentFasilitas.refreshContent(indeks);
            }
            else
            {
                FragmentPelanggaran.refreshContent(indeks);
            }
            tombol.setVisibility(!tampilkan ? View.INVISIBLE : View.VISIBLE);
            tombol.setEnabled(true);
        }
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent)
    {
        if (inflater == null)
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.feed_item, null);

        if (imageLoader == null)
            imageLoader = AppController.getInstance().getImageLoader();

        final TextView LNama = (TextView) convertView.findViewById(R.id.name);
        final TextView LTimeStamp = (TextView) convertView.findViewById(R.id.timestamp);
        TextView LKeluhan = (TextView) convertView.findViewById(R.id.txtStatusMsg);
        final TextView LLokasi = (TextView) convertView.findViewById(R.id.LLokasi);
        TextView LDukungan = (TextView) convertView.findViewById(R.id.LDukungan);
        TextView LKomentar = (TextView) convertView.findViewById(R.id.LKomentar);
        TextView LPrioritas = (TextView) convertView.findViewById(R.id.LPrioritas);
        TextView LBacaselengkapnya = (TextView) convertView.findViewById(R.id.LBacaLengkap);
        LinearLayout LL1 = (LinearLayout) convertView.findViewById(R.id.LL1);
        LinearLayout LL2 = (LinearLayout) convertView.findViewById(R.id.LL2);
        LinearLayout LBase = (LinearLayout) convertView.findViewById(R.id.LBase);
        final FeedImageView feedImageView = (FeedImageView) convertView.findViewById(R.id.feedImage1);
        final Button BDukung = (Button) convertView.findViewById(R.id.BDukung);
        Button BKomentar = (Button) convertView.findViewById(R.id.BKomentar);
        Button BLoadPost = (Button) convertView.findViewById(R.id.BLoadPost);
        final ImageView BLaporkanItem = (ImageView) convertView.findViewById(R.id.BLaporkanItem);
        final ImageView IVSeparator = (ImageView) convertView.findViewById(R.id.IVSeparator);
        ImageView SeparatorH = (ImageView) convertView.findViewById(R.id.imageView2);

        final FeedItem item = feedItems.get(position);

        SharedPreferences sharedPref;
        sharedPref = cc.getSharedPreferences("File_Pengaturan", Context.MODE_PRIVATE);
        id = sharedPref.getString(cc.getString(R.string.p_id_pengguna), "Tidak Ada");
        nama = sharedPref.getString(cc.getString(R.string.p_nama), "Tidak Ada");
        sebagai = sharedPref.getString(cc.getString(R.string.p_sebagai), "Tidak Ada");


        DisplayMetrics displayMetrics = new DisplayMetrics();
        WindowManager windowmanager = (WindowManager) cc.getSystemService(Context.WINDOW_SERVICE);
        windowmanager.getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;
        int height = displayMetrics.heightPixels;

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, height / 4);
        layoutParams.setMargins(3, 0, 3, 0);

        feedImageView.setLayoutParams(layoutParams);

        ResourcesManager.ScaleIcon(cc, BDukung, R.drawable.icon_dukung);
        ResourcesManager.ScaleIcon(cc, LDukungan, R.drawable.icon_dukung);
        ResourcesManager.ScaleIcon(cc, BKomentar, R.drawable.icon_komentar);
        ResourcesManager.ScaleIcon(cc, LKomentar, R.drawable.icon_komentar);

        BKomentar.setOnClickListener(new Button.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent i = new Intent(cc, Komentar.class);
                String[] n = {String.valueOf(item.getId()), item.getBagian()};
                i.putExtra(ID, n);
                i.putExtra("daripemberitahuan", false);
                i.putExtra("nama", item.getName());
                i.putExtra("waktu", item.getTimeStamp());
                i.putExtra("prioritas", item.getPrioritas());
                i.putExtra("dukungan", item.getDukungan());
                i.putExtra("komentar", item.getKomentar());
                i.putExtra("status", item.getStatus());
                i.putExtra("urlforo", item.getImge());
                i.putExtra("alamat", item.getAlamat());
                i.putExtra("lfid", String.valueOf(item.getId()));
                i.putExtra("lat", item.getLat());
                i.putExtra("ling", item.getLing());
                i.putExtra("oleh", item.getOleh());
                Boolean didukung;
                if (item.getSudahlike().equalsIgnoreCase("1"))
                {
                    didukung = true;
                }
                else
                {
                    didukung = false;
                }
                i.putExtra("didukung", didukung);
                activity.startActivity(i);
            }
        });

        BLoadPost.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (item.getBagian().equalsIgnoreCase("fas"))
                {
                    if (FragmentFasilitas.loadpo == false)
                    {
                        FragmentFasilitas.limit += 5;
                        FragmentFasilitas.refreshContent(false);
                        FragmentFasilitas.loadpo = true;
                    }
                }
                else
                {
                    if (FragmentPelanggaran.loadpo == false)
                    {
                        FragmentPelanggaran.limit += 5;
                        FragmentPelanggaran.refreshContent(false);
                        FragmentPelanggaran.loadpo = true;
                    }
                }
            }
        });

        if (item.getStatus().equals("LOAD POST"))
        {
            LL1.setVisibility(View.GONE);
            LL2.setVisibility(View.GONE);
            LKeluhan.setVisibility(View.GONE);
            feedImageView.setVisibility(View.GONE);
            LLokasi.setVisibility(View.GONE);
            IVSeparator.setVisibility(View.GONE);
            LL2.setVisibility(View.GONE);
            SeparatorH.setVisibility(View.GONE);
            BLoadPost.setVisibility(View.VISIBLE);
            LBase.setBackgroundResource(R.drawable.bg_parent_rounded_corner);
            LBacaselengkapnya.setVisibility(View.GONE);
            return convertView;
        }
        else
        {
            LL1.setVisibility(View.VISIBLE);
            LL2.setVisibility(View.VISIBLE);
            LKeluhan.setVisibility(View.VISIBLE);
            feedImageView.setVisibility(View.VISIBLE);
            LLokasi.setVisibility(View.VISIBLE);
            IVSeparator.setVisibility(View.VISIBLE);
            LL2.setVisibility(View.VISIBLE);
            SeparatorH.setVisibility(View.VISIBLE);
            BLoadPost.setVisibility(View.GONE);
        }

        if (!item.getSebagai().equalsIgnoreCase("1"))
        {
            LBase.setBackgroundResource(R.drawable.bg_laporan_admin);
        }
        else
        {
            LBase.setBackgroundResource(R.drawable.bg_parent_rounded_corner);
        }

        LBacaselengkapnya.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent i = new Intent(cc, Komentar.class);
                String[] n = {String.valueOf(item.getId()), item.getBagian()};
                i.putExtra(ID, n);
                i.putExtra("daripemberitahuan", false);
                i.putExtra("nama", item.getName());
                i.putExtra("waktu", item.getTimeStamp());
                i.putExtra("prioritas", item.getPrioritas());
                i.putExtra("dukungan", item.getDukungan());
                i.putExtra("komentar", item.getKomentar());
                i.putExtra("status", item.getStatus());
                i.putExtra("urlforo", item.getImge());
                i.putExtra("alamat", item.getAlamat());
                i.putExtra("lfid", String.valueOf(item.getId()));
                i.putExtra("lat", item.getLat());
                i.putExtra("ling", item.getLing());
                i.putExtra("oleh", item.getOleh());
                Boolean didukung;
                if (item.getSudahlike().equalsIgnoreCase("1"))
                {
                    didukung = true;
                }
                else
                {
                    didukung = false;
                }
                i.putExtra("didukung", didukung);
                activity.startActivity(i);
            }
        });

        LKeluhan.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent i = new Intent(cc, Komentar.class);
                String[] n = {String.valueOf(item.getId()), item.getBagian()};
                i.putExtra(ID, n);
                i.putExtra("daripemberitahuan", false);
                i.putExtra("nama", item.getName());
                i.putExtra("waktu", item.getTimeStamp());
                i.putExtra("prioritas", item.getPrioritas());
                i.putExtra("dukungan", item.getDukungan());
                i.putExtra("komentar", item.getKomentar());
                i.putExtra("status", item.getStatus());
                i.putExtra("urlforo", item.getImge());
                i.putExtra("alamat", item.getAlamat());
                i.putExtra("lfid", String.valueOf(item.getId()));
                i.putExtra("lat", item.getLat());
                i.putExtra("ling", item.getLing());
                i.putExtra("oleh", item.getOleh());
                Boolean didukung;
                if (item.getSudahlike().equalsIgnoreCase("1"))
                {
                    didukung = true;
                }
                else
                {
                    didukung = false;
                }
                i.putExtra("didukung", didukung);
                activity.startActivity(i);
            }
        });

        if (item.getSudahlike().equalsIgnoreCase("1"))
        {
            BDukung.setText("Batal Dukung");
            BDukung.setEnabled(true);
        }
        else
        {
            BDukung.setText("Dukung");
            BDukung.setEnabled(true);
        }

        class LaporkanManager extends AsyncTask<String, Void, String>
        {
            private Context context;

            public LaporkanManager(Context context) {
                this.context = context;
            }

            @Override
            protected void onPreExecute()
            {
                super.onPreExecute();
            }

            @Override
            protected String doInBackground(String... arg0)
            {
                RequestHandler rh = new RequestHandler();

                String link;
                String result;

                try
                {
                    HashMap<String,String> data = new HashMap<>();
                    data.put("waktu", arg0[1]);
                    data.put("oleh", arg0[2]);

                    if (arg0[3].equalsIgnoreCase("fas"))
                    {
                        data.put("lfid", arg0[0]);
                        link = cc.getString(R.string.alamat_server) + cc.getString(R.string.link_lapor_lapor_fasilitas);
                    }
                    else
                    {
                        data.put("lpid", arg0[0]);
                        link = cc.getString(R.string.alamat_server) + cc.getString(R.string.link_lapor_lapor_pelanggaran);
                    }

                    result = rh.sendPostRequest(link, data);

                    return result;
                } catch (Exception e)
                {
                    return new String("Exception: " + e.getMessage());
                }
            }

            @Override
            protected void onPostExecute(String result)
            {
                String jsonStr = result;
                if (jsonStr != null)
                {
                    try
                    {
                        JSONObject jsonObj = new JSONObject(jsonStr);
                        String query_result = jsonObj.getString("hasil");
                        if (query_result.equals("SUKSES"))
                        {
                            Snackbar.make(IVSeparator, "Terima kasih telah melaporkan laporan ini, kami akan menindak lanjuti itu.", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                        } else if (query_result.equals("SUDAH ADA"))
                        {
                            Snackbar.make(IVSeparator, "Anda sudah pernah melaporkan laporan ini.", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                        } else if (query_result.equals("GAGAL"))
                        {
                            Snackbar.make(IVSeparator, "Terjadi kesalahan, silakan coba lagi.", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                        } else
                        {
                            Snackbar.make(IVSeparator, "Tidak dapat terhubung ke database.", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                        }
                    } catch (JSONException e)
                    {
                        e.printStackTrace();
                        Toast.makeText(cc, "Tidak dapat terhubung ke database, periksa koneksi internet kamu. " + jsonStr.toString(), Toast.LENGTH_LONG).show();
                    }
                } else
                {
                    Snackbar.make(IVSeparator, "Tidak dapat mengambil data JSON.", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                }
            }
        }

        class DukungManager extends AsyncTask<String, Void, String>
        {
            private Context context;

            public DukungManager(Context context) {
                this.context = context;
            }

            @Override
            protected void onPreExecute()
            {
                super.onPreExecute();
            }

            @Override
            protected String doInBackground(String... arg0)
            {
                RequestHandler rh = new RequestHandler();

                String link;
                String result;

                try
                {
                    HashMap<String,String> data = new HashMap<>();
                    data.put("oleh", arg0[0]);
                    data.put("untuk", arg0[1]);

                    if (arg0[2].equalsIgnoreCase("fas"))
                    {
                        link = cc.getString(R.string.alamat_server) + cc.getString(R.string.link_dukung_fasilitas);
                    }
                    else
                    {
                        link = cc.getString(R.string.alamat_server) + cc.getString(R.string.link_dukung_pelanggaran);
                    }


                    result = rh.sendPostRequest(link, data);

                    return result;
                } catch (Exception e)
                {
                    return new String("Exception: " + e.getMessage());
                }
            }

            @Override
            protected void onPostExecute(String result)
            {
                String jsonStr = result;
                if (jsonStr != null)
                {
                    try
                    {
                        JSONObject jsonObj = new JSONObject(jsonStr);
                        String query_result = jsonObj.getString("hasil");
                        if (query_result.equals("SUKSES"))
                        {
                            Tampilkan(true, BDukung, "BATAL DUKUNG", item.getId(), item.getBagian());
                        } else if (query_result.equals("GAGAL"))
                        {
                            Tampilkan(true, BDukung, "DUKUNG", item.getId(), item.getBagian());
                            Snackbar.make(BDukung, "Terjadi kesalahan, silakan coba lagi.", Snackbar.LENGTH_LONG).show();
                        } else if (query_result.equals("SUDAH MENDUKUNG"))
                        {
                            Tampilkan(true, BDukung, "BATAL DUKUNG", item.getId(), item.getBagian());
                            Snackbar.make(BDukung, "Anda sudah mendukung masalah ini.", Snackbar.LENGTH_LONG).show();
                        } else
                        {
                            Tampilkan(true, BDukung, "DUKUNG", item.getId(), item.getBagian());
                            Snackbar.make(BDukung, "Tidak dapat terhubung ke database.", Snackbar.LENGTH_LONG).show();
                        }
                    } catch (JSONException e)
                    {
                        Tampilkan(true, BDukung, "DUKUNG", item.getId(), item.getBagian());
                        e.printStackTrace();
                        Toast.makeText(context, "Tidak dapat terhubung ke database, periksa koneksi internet kamu. " + jsonStr.toString(), Toast.LENGTH_LONG).show();
                    }
                } else
                {
                    Tampilkan(true, BDukung, "DUKUNG", item.getId(), item.getBagian());
                    Snackbar.make(BDukung, "Tidak dapat mengambil data JSON.", Snackbar.LENGTH_LONG).show();
                }
            }
        }

        class BatalDukungManager extends AsyncTask<String, Void, String>
        {
            private Context context;

            public BatalDukungManager(Context context) {
                this.context = context;
            }

            @Override
            protected void onPreExecute()
            {
                super.onPreExecute();
            }

            @Override
            protected String doInBackground(String... arg0)
            {
                RequestHandler rh = new RequestHandler();

                String link;
                String result;

                try
                {
                    HashMap<String,String> data = new HashMap<>();
                    data.put("oleh", arg0[0]);
                    data.put("untuk", arg0[1]);

                    if (arg0[2].equalsIgnoreCase("fas"))
                    {
                        link = cc.getString(R.string.alamat_server) + cc.getString(R.string.link_batal_dukung_fasilitas);
                    }
                    else
                    {
                        link = cc.getString(R.string.alamat_server) + cc.getString(R.string.link_batal_dukung_pelanggaran);
                    }

                    result = rh.sendPostRequest(link, data);

                    return result;
                } catch (Exception e)
                {
                    return new String("Exception: " + e.getMessage());
                }
            }

            @Override
            protected void onPostExecute(String result)
            {
                String jsonStr = result;
                if (jsonStr != null)
                {
                    try
                    {
                        JSONObject jsonObj = new JSONObject(jsonStr);
                        String query_result = jsonObj.getString("hasil");
                        if (query_result.equals("SUKSES"))
                        {
                            Tampilkan(true, BDukung, "DUKUNG", item.getId(), item.getBagian());
                        } else if (query_result.equals("GAGAL"))
                        {
                            Tampilkan(true, BDukung, "BATAL DUKUNG", item.getId(), item.getBagian());
                            Snackbar.make(BDukung, "Terjadi kesalahan, silakan coba lagi.", Snackbar.LENGTH_LONG).show();
                        } else
                        {
                            Tampilkan(true, BDukung, "BATAL DUKUNG", item.getId(), item.getBagian());
                            Snackbar.make(BDukung, "Tidak dapat terhubung ke database.", Snackbar.LENGTH_LONG).show();
                        }
                    } catch (JSONException e)
                    {
                        Tampilkan(true, BDukung, "BATAL DUKUNG", item.getId(), item.getBagian());
                        e.printStackTrace();
                        Toast.makeText(context, "Tidak dapat terhubung ke database, periksa koneksi internet kamu. " + jsonStr.toString(), Toast.LENGTH_LONG).show();
                    }
                } else
                {
                    Tampilkan(true, BDukung, "BATAL DUKUNG", item.getId(), item.getBagian());
                    Snackbar.make(BDukung, "Tidak dapat mengambil data JSON.", Snackbar.LENGTH_LONG).show();
                }
            }
        }

        class HapusLaporanManager extends AsyncTask<String, Void, String>
        {
            private Context context;

            public HapusLaporanManager(Context context) {
                this.context = context;
            }

            @Override
            protected void onPreExecute()
            {
                super.onPreExecute();
            }

            @Override
            protected String doInBackground(String... arg0)
            {
                RequestHandler rh = new RequestHandler();

                String link;
                String result;

                try
                {
                    HashMap<String,String> data = new HashMap<>();

                    if (item.getBagian().equalsIgnoreCase("fas"))
                    {
                        data.put("tabel", "laporanfasilitas");
                        data.put("namaid", "lfid");
                    }
                    else
                    {
                        data.put("tabel", "laporanpelanggaran");
                        data.put("namaid", "lpid");
                    }

                    data.put("id", arg0[0]);
                    link = cc.getString(R.string.alamat_server) + cc.getString(R.string.link_hapus_data);

                    result = rh.sendPostRequest(link, data);

                    return result;
                } catch (Exception e)
                {
                    return new String("Exception: " + e.getMessage());
                }
            }

            @Override
            protected void onPostExecute(String result)
            {
                String jsonStr = result;
                if (jsonStr != null)
                {
                    try
                    {
                        JSONObject jsonObj = new JSONObject(jsonStr);
                        String query_result = jsonObj.getString("hasil");
                        if (query_result.equals("SUKSES"))
                        {
                            Snackbar.make(IVSeparator, "Laporan yang Anda pilih sudah dihapus", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                            if (item.getBagian().equalsIgnoreCase("fas"))
                            {
                                FragmentFasilitas.feedItems.remove(position);
                                FragmentFasilitas.listAdapter.notifyDataSetChanged();
                            }
                            else
                            {
                                FragmentPelanggaran.feedItems.remove(position);
                                FragmentPelanggaran.listAdapter.notifyDataSetChanged();
                            }
                        } else if (query_result.equals("GAGAL"))
                        {
                            Snackbar.make(IVSeparator, "Terjadi kesalahan, silahkan coba lagi", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                        } else
                        {
                            Snackbar.make(BDukung, "Tidak dapat terhubung ke database.", Snackbar.LENGTH_LONG).show();
                        }
                    } catch (JSONException e)
                    {
                        e.printStackTrace();
                        Toast.makeText(context, "Tidak dapat terhubung ke database, periksa koneksi internet kamu. " + jsonStr.toString(), Toast.LENGTH_LONG).show();
                    }
                } else
                {
                    Snackbar.make(BDukung, "Tidak dapat mengambil data JSON.", Snackbar.LENGTH_LONG).show();
                }
            }
        }

        class SembunyikanLaporanManager extends AsyncTask<String, Void, String>
        {
            private Context context;

            public SembunyikanLaporanManager(Context context) {
                this.context = context;
            }

            @Override
            protected void onPreExecute()
            {
                super.onPreExecute();
            }

            @Override
            protected String doInBackground(String... arg0)
            {
                RequestHandler rh = new RequestHandler();

                String link;
                String result;

                try
                {
                    HashMap<String,String> data = new HashMap<>();

                    if (item.getBagian().equalsIgnoreCase("fas"))
                    {
                        data.put("tabel", "laporanfasilitas");
                        data.put("namaid", "lfid");
                    }
                    else
                    {
                        data.put("tabel", "laporanpelanggaran");
                        data.put("namaid", "lpid");
                    }

                    data.put("id", arg0[0]);
                    link = cc.getString(R.string.alamat_server) + cc.getString(R.string.link_sembunyikan_laporan);

                    result = rh.sendPostRequest(link, data);

                    return result;
                } catch (Exception e)
                {
                    return new String("Exception: " + e.getMessage());
                }
            }

            @Override
            protected void onPostExecute(String result)
            {
                String jsonStr = result;
                if (jsonStr != null)
                {
                    try
                    {
                        JSONObject jsonObj = new JSONObject(jsonStr);
                        String query_result = jsonObj.getString("hasil");
                        if (query_result.equals("SUKSES"))
                        {
                            Snackbar.make(IVSeparator, "Laporan yang Anda pilih sudah disembunyikan dari publik", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                            if (item.getBagian().equalsIgnoreCase("fas"))
                            {
                                FragmentFasilitas.refreshContent(item.getId());
                            }
                            else
                            {
                                FragmentPelanggaran.refreshContent(item.getId());
                            }
                        } else if (query_result.equals("GAGAL"))
                        {
                            Snackbar.make(IVSeparator, "Terjadi kesalahan, silahkan coba lagi", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                        } else
                        {
                            Snackbar.make(BDukung, "Tidak dapat terhubung ke database.", Snackbar.LENGTH_LONG).show();
                        }
                    } catch (JSONException e)
                    {
                        e.printStackTrace();
                        Toast.makeText(context, "Tidak dapat terhubung ke database, periksa koneksi internet kamu. " + jsonStr.toString(), Toast.LENGTH_LONG).show();
                    }
                } else
                {
                    Snackbar.make(BDukung, "Tidak dapat mengambil data JSON.", Snackbar.LENGTH_LONG).show();
                }
            }
        }

        BDukung.setOnClickListener(new Button.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                final Boolean tampilkan = true;
                BDukung.setEnabled(false);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2)
                {
                    final int shortAnimTime = 500;

                    if (BDukung.getText().toString().equalsIgnoreCase("DUKUNG"))
                    {
                        new DukungManager(cc).execute(id, String.valueOf(item.getId()), item.getBagian());

                        BDukung.animate().setDuration(shortAnimTime).alpha(tampilkan ? 0 : 1).setListener(new AnimatorListenerAdapter()
                        {
                            @Override
                            public void onAnimationEnd(Animator animation)
                            {

                            }
                        });
                    } else
                    {
                        new BatalDukungManager(cc).execute(id, String.valueOf(item.getId()), item.getBagian());

                        BDukung.animate().setDuration(shortAnimTime).alpha(tampilkan ? 0 : 1).setListener(new AnimatorListenerAdapter()
                        {
                            @Override
                            public void onAnimationEnd(Animator animation)
                            {

                            }
                        });
                    }

                } else
                {
                    BDukung.setVisibility(tampilkan ? View.INVISIBLE : View.VISIBLE);
                }
            }
        });

        LLokasi.setOnClickListener(new TextView.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent i = new Intent(cc, MapsActivity.class);
                String[] ll = {item.getLat(), item.getLing()};
                i.putExtra(LATLING, ll);
                activity.startActivity(i);
            }
        });

        String str = item.getName();
        String[] strArray = str.split(" ");
        StringBuilder builder = new StringBuilder();
        for (String s : strArray) {
            if (s.length() > 0)
            {
                String cap = s.substring(0, 1).toUpperCase() + s.substring(1).toLowerCase();
                builder.append(cap + " ");
            }
        }

        LNama.setText(builder.toString());

        LDukungan.setText(" " + item.getDukungan());
        LKomentar.setText(" " + item.getKomentar());
        LPrioritas.setText(item.getPrioritas());

        String expectedPattern = "yyyy-MM-dd HH:mm:ss";
        SimpleDateFormat formatter = new SimpleDateFormat(expectedPattern);
        String userInput = item.getTimeStamp();
        Date tanggal = null;
        try
        {
            tanggal = formatter.parse(userInput);
        } catch (ParseException e)
        {
            e.printStackTrace();
        }
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy hh:mm a");
        Calendar cal = Calendar.getInstance();
        sdf.setTimeZone(cal.getTimeZone());

        LTimeStamp.setText(sdf.format(tanggal));

        // Chcek for empty status message
        if (!TextUtils.isEmpty(item.getStatus())) {
            String text;
            if (item.getStatus().length() > 150)
            {
                LBacaselengkapnya.setVisibility(View.VISIBLE);
                text = item.getStatus().substring(0, 150) + "....";
                LKeluhan.setText(text.substring(0,1).toUpperCase() + text.substring(1));
            }
            else
            {
                LBacaselengkapnya.setVisibility(View.GONE);
                LKeluhan.setText(item.getStatus().substring(0,1).toUpperCase() + item.getStatus().substring(1));
            }
            LKeluhan.setVisibility(View.VISIBLE);
        } else {
            // status is empty, remove from view
            LKeluhan.setVisibility(View.GONE);
        }

        // Checking for null feed LLokasi
        if (!TextUtils.isEmpty(item.getLat())) {
            if (!TextUtils.isEmpty(item.getAlamat()))
            {
                LLokasi.setText(item.getAlamat());
            }
            else
            {
                LLokasi.setText(item.getLat() + ", " + item.getLing());
            }

            // Making LLokasi clickable
            LLokasi.setMovementMethod(LinkMovementMethod.getInstance());
            LLokasi.setVisibility(View.VISIBLE);
        } else {
            // LLokasi is null, remove from the view
            LLokasi.setVisibility(View.GONE);
        }

        // Feed image
        if (item.getImge() != null) {
            feedImageView.setImageUrl(item.getImge(), imageLoader, item.getIndividual());
            feedImageView.setVisibility(View.VISIBLE);
            feedImageView.setResponseObserver(new FeedImageView.ResponseObserver() {
                        @Override
                        public void onError() {
                        }

                        @Override
                        public void onSuccess() {
                        }
                    });
        } else {
            feedImageView.setVisibility(View.GONE);
        }

        feedImageView.setErrorImageResId(R.drawable.memuat);

        feedImageView.setOnClickListener(new FeedImageView.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(cc.getApplicationContext(), FotoActivity.class);
                intent.putExtra("url", item.getImge());
                if (item.getBagian().equalsIgnoreCase("fas"))
                {
                    FragmentFasilitas.perlu = false;
                    intent.putExtra("bagian", "Informasi");
                }
                else
                {
                    FragmentPelanggaran.perlu = false;
                    intent.putExtra("bagian", "Pelanggaran Polisi");
                }
                cc.startActivity(intent);
            }
        });

        BLaporkanItem.setOnClickListener(new Button.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (sebagai.equalsIgnoreCase("Masyarakat"))
                {
                    AlertDialog.Builder builder2 = new AlertDialog.Builder(cc);
                    builder2.setTitle("Apa yang ingin Anda lakukan ?");
                    if (item.getOleh().equalsIgnoreCase(id))
                    {
                        builder2.setItems(R.array.pilihan_laporan_my, new DialogInterface.OnClickListener()
                        {
                            public void onClick(DialogInterface dialog, int which)
                            {
                                if (which == 0)
                                {
                                    Satpam.setupFacebookShareIntent(activity, item.getName(), LTimeStamp.getText().toString(), LLokasi.getText().toString(), item.getStatus(), item.getImge(), item.getBagian().equalsIgnoreCase("fas") ? true : false);
                                }else if (which == 1)
                                {
                                    Satpam.Bagikan(activity, item.getBagian().equalsIgnoreCase("fas") ? "Laporan Informasi dari " + LNama.getText().toString() : "Laporan Pelanggaran dari " + LNama.getText().toString(), item.getStatus(), item.getImge());
                                }
                            }
                        });
                    }
                    else
                    {
                        builder2.setItems(R.array.pilihan_laporan, new DialogInterface.OnClickListener()
                        {
                            public void onClick(DialogInterface dialog, int which)
                            {
                                if (which == 0)
                                {
                                    final AlertDialog.Builder builder = new AlertDialog.Builder(cc);
                                    builder.setMessage("Apa Anda yakin ingin melaporkan laporan ini?")
                                            .setCancelable(true)
                                            .setPositiveButton("Iya", new DialogInterface.OnClickListener()
                                            {
                                                public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int did)
                                                {
                                                    Calendar cal = Calendar.getInstance();
                                                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                                    String formattedDate = df.format(cal.getTime());

                                                    new LaporkanManager(cc).execute(String.valueOf(item.getId()), formattedDate, id, item.getBagian());
                                                }
                                            })
                                            .setNegativeButton("Tidak", new DialogInterface.OnClickListener()
                                            {
                                                public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                                                    dialog.cancel();
                                                }
                                            });
                                    final AlertDialog alert = builder.create();
                                    alert.show();
                                }
                                else if (which == 1)
                                {
                                    Satpam.setupFacebookShareIntent(activity, item.getName(), LTimeStamp.getText().toString(), LLokasi.getText().toString(), item.getStatus(), item.getImge(), item.getBagian().equalsIgnoreCase("fas") ? true : false);
                                }
                                else  if (which == 2)
                                {
                                    Satpam.Bagikan(activity, item.getBagian().equalsIgnoreCase("fas") ? "Laporan Informasi dari " + LNama.getText().toString() : "Laporan Pelanggaran dari " + LNama.getText().toString(), item.getStatus(), item.getImge());
                                }
                            }
                        });
                    }
                    builder2.show();
                }
                else
                {
                    AlertDialog.Builder builder2 = new AlertDialog.Builder(cc);
                    builder2.setTitle("Apa yang ingin Anda lakukan ?")
                            .setItems(R.array.pilihan_laporan_admin, new DialogInterface.OnClickListener()
                            {
                                public void onClick(DialogInterface dialog, int which)
                                {
                                    if (which == 0)
                                    {
                                        final AlertDialog.Builder builder = new AlertDialog.Builder(cc);
                                        builder.setMessage("Hapus laporan atau sembunyikan laporan dari publik ?\n\nLaporan yang sudah dihapus tidak dapat dikembalikan lagi.")
                                                .setCancelable(true)
                                                .setPositiveButton("Hapus", new DialogInterface.OnClickListener()
                                                {
                                                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int did)
                                                    {
                                                        new HapusLaporanManager(cc).execute(String.valueOf(item.getId()));
                                                    }
                                                })
                                                .setNegativeButton("Batal", new DialogInterface.OnClickListener()
                                                {
                                                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id)
                                                    {
                                                        dialog.cancel();
                                                    }
                                                })
                                                .setNeutralButton("Sembunyikan", new DialogInterface.OnClickListener()
                                                {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which)
                                                    {
                                                        new SembunyikanLaporanManager(cc).execute(String.valueOf(item.getId()));
                                                    }
                                                });
                                        final AlertDialog alert = builder.create();
                                        alert.show();
                                    }
                                    else if (which == 1)
                                    {
                                        Satpam.setupFacebookShareIntent(activity, item.getName(), LTimeStamp.getText().toString(), LLokasi.getText().toString(), item.getStatus(), item.getImge(), item.getBagian().equalsIgnoreCase("fas") ? true : false);
                                    }
                                    else  if (which == 2)
                                    {
                                        Satpam.Bagikan(activity, item.getBagian().equalsIgnoreCase("fas") ? "Laporan Informasi dari " + LNama.getText().toString() : "Laporan Pelanggaran dari " + LNama.getText().toString(), item.getStatus(), item.getImge());
                                    }
                                }
                            });
                    builder2.show();
                }
            }
        });

        return convertView;
    }
}
