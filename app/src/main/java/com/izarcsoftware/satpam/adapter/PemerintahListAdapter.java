package com.izarcsoftware.satpam.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.izarcsoftware.satpam.R;
import com.izarcsoftware.satpam.data.PemerintahItem;
import com.izarcsoftware.satpam.data.PenggunaItem;

import java.util.List;

/**
 * Created by Aziz Nur Ariffianto on 0011, 11 Feb 2016.
 */
public class PemerintahListAdapter extends BaseAdapter
{
    private Activity activity;
    private LayoutInflater inflater;
    private List<PemerintahItem> pemerintahItems;
    public static Context cc;

    public PemerintahListAdapter(List<PemerintahItem> pemerintahItems, Activity activity)
    {
        this.pemerintahItems = pemerintahItems;
        this.activity = activity;
    }


    @Override
    public int getCount()
    {
        return pemerintahItems.size();
    }

    @Override
    public Object getItem(int position)
    {
        return pemerintahItems.get(position);
    }

    @Override
    public long getItemId(int position)
    {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        if (inflater == null)
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.pemerintah_item, null);

        TextView LNamaPemerintah = (TextView) convertView.findViewById(R.id.LNamaPemerintah);
        TextView LSebagai = (TextView) convertView.findViewById(R.id.LSebagai);
        TextView LNomor = (TextView) convertView.findViewById(R.id.LNomor);

        final PemerintahItem item = pemerintahItems.get(position);

        String str = item.getNama();
        String[] strArray = str.split(" ");
        StringBuilder builder = new StringBuilder();
        for (String s : strArray) {
            if (s.length() > 0)
            {
                String cap = s.substring(0, 1).toUpperCase() + s.substring(1).toLowerCase();
                builder.append(cap + " ");
            }
        }

        LNamaPemerintah.setText(builder.toString());
        LSebagai.setText(item.getSebagai());
        LNomor.setText(String.valueOf(item.getNomor()) + ".");

        return convertView;
    }
}
