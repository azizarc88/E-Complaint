package com.izarcsoftware.satpam.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.design.widget.Snackbar;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.izarcsoftware.satpam.Komentar;
import com.izarcsoftware.satpam.R;
import com.izarcsoftware.satpam.RequestHandler;
import com.izarcsoftware.satpam.data.KomentarItem;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Aziz Nur Ariffianto on 0007, 07 Feb 2016.
 */
public class KomentarListAdapter extends BaseAdapter
{
    private Activity activity;
    private LayoutInflater inflater;
    private List<KomentarItem> komentarItems;
    public static Context cc;
    private SharedPreferences sharedPreferences;

    public KomentarListAdapter(Activity activity, List<KomentarItem> komentarItems)
    {
        this.activity = activity;
        this.komentarItems = komentarItems;
    }

    @Override
    public int getCount()
    {
        return komentarItems.size();
    }

    @Override
    public Object getItem(int position)
    {
        return komentarItems.get(position);
    }

    @Override
    public long getItemId(int position)
    {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        if (inflater == null)
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.komentar_item, null);


        TextView LNamaKomen = (TextView) convertView.findViewById(R.id.LNamaKomen);
        TextView LWaktuKomen = (TextView) convertView.findViewById(R.id.LWaktuKomen);
        final TextView LKomentar = (TextView) convertView.findViewById(R.id.LKomentar);
        Button BHapusKomen = (Button) convertView.findViewById(R.id.BHapusKomen);
        Button BLaporkanKomen = (Button) convertView.findViewById(R.id.BLaporkan);
        LinearLayout LLKomentar = (LinearLayout) convertView.findViewById(R.id.LLKomentar);

        final View VV = BLaporkanKomen;

        final KomentarItem item = komentarItems.get(position);

        String str = item.getNama();
        String[] strArray = str.split(" ");
        StringBuilder builder = new StringBuilder();
        for (String s : strArray) {
            if (s.length() > 0)
            {
                String cap = s.substring(0, 1).toUpperCase() + s.substring(1).toLowerCase();
                builder.append(cap + " ");
            }
        }

        LNamaKomen.setText(builder.toString());

        String expectedPattern = "yyyy-MM-dd HH:mm:ss";
        SimpleDateFormat formatter = new SimpleDateFormat(expectedPattern);
        String userInput = item.getWaktu();
        Date tanggal = null;
        try
        {
            tanggal = formatter.parse(userInput);
        } catch (ParseException e)
        {
            e.printStackTrace();
        }
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy  hh:mm:ss a");
        Calendar cal = Calendar.getInstance();
        sdf.setTimeZone(cal.getTimeZone());

        LWaktuKomen.setText(sdf.format(tanggal));

        LKomentar.setText(item.getKomentar());

        sharedPreferences = cc.getSharedPreferences("File_Pengaturan", Context.MODE_PRIVATE);
        String sebagai = sharedPreferences.getString(cc.getString(R.string.p_sebagai), "Tidak Ada");

        if (item.getSebagai().equalsIgnoreCase("Masyarakat"))
        {
            LLKomentar.setBackgroundColor(Color.WHITE);
            if (item.getKomentarku().equalsIgnoreCase("1"))
            {
                BHapusKomen.setVisibility(View.VISIBLE);
                BLaporkanKomen.setVisibility(View.GONE);
            } else
            {
                if (sebagai.equalsIgnoreCase("Masyarakat"))
                {
                    BHapusKomen.setVisibility(View.GONE);
                    BLaporkanKomen.setVisibility(View.VISIBLE);
                }
                else if (sebagai.equalsIgnoreCase("Admin E-Complaint") || sebagai.equalsIgnoreCase("Developer E-Complaint") || sebagai.equalsIgnoreCase("Kapolres Banyumas"))
                {
                    BHapusKomen.setVisibility(View.VISIBLE);
                }
            }
        }
        else
        {
            if (sebagai.equalsIgnoreCase("Masyarakat"))
            {
                BHapusKomen.setVisibility(View.GONE);
            }
            else if (sebagai.equalsIgnoreCase("Admin E-Complaint") || sebagai.equalsIgnoreCase("Developer E-Complaint") || sebagai.equalsIgnoreCase("Kapolres Banyumas"))
            {
                BHapusKomen.setVisibility(View.VISIBLE);
            }

            BLaporkanKomen.setVisibility(View.GONE);
            LLKomentar.setBackgroundResource(R.color.colorCanvas2);
        }

        class BatalKomenManager extends AsyncTask<String, Void, String>
        {
            private Context context;

            public BatalKomenManager(Context context) {
                this.context = context;
            }

            @Override
            protected void onPreExecute()
            {
                super.onPreExecute();
            }

            @Override
            protected String doInBackground(String... arg0)
            {
                RequestHandler rh = new RequestHandler();

                String link;
                String result;

                try
                {
                    HashMap<String,String> data = new HashMap<>();
                    data.put("kmid", arg0[0]);
                    data.put("untuk", arg0[1]);

                    if (arg0[2].equalsIgnoreCase("fas"))
                    {
                        link = cc.getString(R.string.alamat_server) + cc.getString(R.string.link_batal_komen_fasilitas);
                    }
                    else
                    {
                        link = cc.getString(R.string.alamat_server) + cc.getString(R.string.link_batal_komen_pelanggaran);
                    }


                    result = rh.sendPostRequest(link, data);

                    return result;
                } catch (Exception e)
                {
                    return new String("Exception: " + e.getMessage());
                }
            }

            @Override
            protected void onPostExecute(String result)
            {
                String jsonStr = result;
                if (jsonStr != null)
                {
                    try
                    {
                        JSONObject jsonObj = new JSONObject(jsonStr);
                        String query_result = jsonObj.getString("hasil");
                        if (query_result.equals("SUKSES"))
                        {
                            new Komentar().refreshContent();
                        } else if (query_result.equals("GAGAL"))
                        {
                            Snackbar.make(VV, "Terjadi kesalahan, silakan coba lagi.", Snackbar.LENGTH_LONG).show();
                        } else
                        {
                            Snackbar.make(VV, "Tidak dapat terhubung ke database.", Snackbar.LENGTH_LONG).show();
                        }
                    } catch (JSONException e)
                    {
                        e.printStackTrace();
                        Toast.makeText(context, "Tidak dapat terhubung ke database, periksa koneksi internet kamu. " + jsonStr.toString(), Toast.LENGTH_LONG).show();
                    }
                } else
                {
                    Snackbar.make(VV, "Tidak dapat mengambil data JSON.", Snackbar.LENGTH_LONG).show();
                }
            }
        }

        class LaporkanKomenManager extends AsyncTask<String, Void, String>
        {
            private Context context;

            public LaporkanKomenManager(Context context) {
                this.context = context;
            }

            @Override
            protected void onPreExecute()
            {
                super.onPreExecute();
            }

            @Override
            protected String doInBackground(String... arg0)
            {
                RequestHandler rh = new RequestHandler();

                String link;
                String result;

                try
                {
                    HashMap<String,String> data = new HashMap<>();
                    data.put("kmid", arg0[0]);
                    data.put("waktu", arg0[1]);
                    data.put("oleh", arg0[2]);

                    if (arg0[3].equalsIgnoreCase("fas"))
                    {
                        link = cc.getString(R.string.alamat_server) + cc.getString(R.string.link_lapor_komen_fasilitas);
                    }
                    else
                    {
                        link = cc.getString(R.string.alamat_server) + cc.getString(R.string.link_lapor_komen_pelanggaran);
                    }


                    result = rh.sendPostRequest(link, data);

                    return result;
                } catch (Exception e)
                {
                    return new String("Exception: " + e.getMessage());
                }
            }

            @Override
            protected void onPostExecute(String result)
            {
                String jsonStr = result;
                if (jsonStr != null)
                {
                    try
                    {
                        JSONObject jsonObj = new JSONObject(jsonStr);
                        String query_result = jsonObj.getString("hasil");
                        if (query_result.equals("SUKSES"))
                        {
                            Snackbar.make(VV, "Terima kasih telah melaporkan komentar ini, kami akan menindak lanjuti itu.", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                        } else if (query_result.equals("SUDAH ADA"))
                        {
                            Snackbar.make(VV, "Anda sudah pernah melaporkan komentar ini.", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                        } else if (query_result.equals("GAGAL"))
                        {
                            Snackbar.make(VV, "Terjadi kesalahan, silakan coba lagi.", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                        } else
                        {
                            Snackbar.make(VV, "Tidak dapat terhubung ke database.", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                        }
                    } catch (JSONException e)
                    {
                        e.printStackTrace();
                        Toast.makeText(cc, "Tidak dapat terhubung ke database, periksa koneksi internet kamu. " + jsonStr.toString(), Toast.LENGTH_LONG).show();
                    }
                } else
                {
                    Snackbar.make(VV, "Tidak dapat mengambil data JSON.", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                }
            }
        }

        BHapusKomen.setOnClickListener(new Button.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                final AlertDialog.Builder builder = new AlertDialog.Builder(cc);
                builder.setMessage("Apa Anda yakin ingin menghapus komentar ini ?")
                        .setCancelable(true)
                        .setPositiveButton("Iya", new DialogInterface.OnClickListener()
                        {
                            public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id)
                            {
                                new BatalKomenManager(cc).execute(String.valueOf(item.getKmid()), item.getLfid(), item.getBagian());
                            }
                        })
                        .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                            public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                                dialog.cancel();
                            }
                        });
                final AlertDialog alert = builder.create();
                alert.show();
            }
        });

        BLaporkanKomen.setOnClickListener(new Button.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                final AlertDialog.Builder builder = new AlertDialog.Builder(cc);
                builder.setMessage("Apa Anda yakin ingin melaporkan komentar ini?")
                        .setCancelable(true)
                        .setPositiveButton("Iya", new DialogInterface.OnClickListener()
                        {
                            public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id)
                            {
                                Calendar cal = Calendar.getInstance();
                                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                String formattedDate = df.format(cal.getTime());

                                SharedPreferences sharedPref;
                                sharedPref = cc.getSharedPreferences("File_Pengaturan", Context.MODE_PRIVATE);
                                final String pgid = sharedPref.getString("p_id_pengguna", "Tidak Ada");

                                new LaporkanKomenManager(cc).execute(String.valueOf(item.getKmid()), formattedDate, pgid, item.getBagian());
                            }
                        })
                        .setNegativeButton("Tidak", new DialogInterface.OnClickListener()
                        {
                            public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                                dialog.cancel();
                            }
                        });
                final AlertDialog alert = builder.create();
                alert.show();
            }
        });

        return convertView;
    }
}
