package com.izarcsoftware.satpam.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.izarcsoftware.satpam.R;
import com.izarcsoftware.satpam.data.PemberitahuanItem;

import java.util.List;

/**
 * Created by Aziz Nur Ariffianto on 0017, 17 Feb 2016.
 */
public class PemberitahuanListAdapter extends BaseAdapter
{
    private Activity activity;
    private LayoutInflater inflater;
    private List<PemberitahuanItem> pemberitahuanItem;
    public static Context cc;

    public PemberitahuanListAdapter(Activity activity, List<PemberitahuanItem> pemberitahuanItem)
    {
        this.activity = activity;
        this.pemberitahuanItem = pemberitahuanItem;
    }

    @Override
    public int getCount()
    {
        return pemberitahuanItem.size();
    }

    @Override
    public Object getItem(int position)
    {
        return pemberitahuanItem.get(position);
    }

    @Override
    public long getItemId(int position)
    {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        if (inflater == null)
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.pemerintah_item, null);

        TextView LTipePemberitahuan = (TextView) convertView.findViewById(R.id.LNamaPemerintah);
        TextView LIsi = (TextView) convertView.findViewById(R.id.LSebagai);
        TextView LNomor = (TextView) convertView.findViewById(R.id.LNomor);

        PemberitahuanItem item = pemberitahuanItem.get(position);
        LTipePemberitahuan.setText(item.getTipe());
        LIsi.setText(item.getIsi());
        LNomor.setText(String.valueOf(item.getNomor()) + ".");

        return convertView;
    }

    @Override
    public boolean isEnabled(int position) {
        return true;
    }
}
