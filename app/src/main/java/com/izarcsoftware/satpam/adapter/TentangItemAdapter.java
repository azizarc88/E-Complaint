package com.izarcsoftware.satpam.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.izarcsoftware.satpam.R;
import com.izarcsoftware.satpam.data.PemerintahItem;
import com.izarcsoftware.satpam.data.TentangItem;

import java.util.List;

/**
 * Created by Ampu2 on 07/04/2016.
 */
public class TentangItemAdapter extends BaseAdapter
{
    private Activity activity;
    private LayoutInflater inflater;
    private List<TentangItem> tentangItems;
    public static Context cc;

    public TentangItemAdapter(List<TentangItem> tentangItems, Activity activity)
    {
        this.tentangItems = tentangItems;
        this.activity = activity;
    }


    @Override
    public int getCount()
    {
        return tentangItems.size();
    }

    @Override
    public Object getItem(int position)
    {
        return tentangItems.get(position);
    }

    @Override
    public long getItemId(int position)
    {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        if (inflater == null)
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.tentang_item, null);

        TextView LIsi = (TextView) convertView.findViewById(R.id.LIsi);
        ImageView IVIcon = (ImageView) convertView.findViewById(R.id.IVIcon);

        final TentangItem item = tentangItems.get(position);

        LIsi.setText(item.getIsi());
        IVIcon.setImageResource(item.getIcon());

        return convertView;
    }
}
