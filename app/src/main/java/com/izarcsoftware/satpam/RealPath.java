package com.izarcsoftware.satpam;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.CursorLoader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.provider.DocumentsContract;
import android.provider.MediaStore;

/**
 * Created by Aziz Nur Ariffianto on 0003, 03 Feb 2016.
 */
public class RealPath
{

    public static String getRealPathFromURI(Context context, Uri uri){
        String hasil = "eror";

        String filePath = "";
        Cursor cursor;

        try
        {
            String wholeID = null;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT)
            {
                wholeID = DocumentsContract.getDocumentId(uri);
            }
            String id = wholeID.split(":")[1];

            String[] column = { MediaStore.Images.Media.DATA };

            String sel = MediaStore.Images.Media._ID + "=?";

            cursor = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                    column, sel, new String[]{ id }, null);

            int columnIndex = cursor.getColumnIndex(column[0]);

            if (cursor.moveToFirst()) {
                filePath = cursor.getString(columnIndex);
            }
            cursor.close();
            hasil = filePath;
        }
        catch (NoClassDefFoundError e)
        {
            hasil = "eror";
        }

        if (hasil.equals("") || hasil.equals("eror"))
        {
            try
            {
                String[] proj = { MediaStore.Images.Media.DATA };
                String result = null;

                CursorLoader cursorLoader = new CursorLoader(
                        context,
                        uri, proj, null, null, null);
                cursor = cursorLoader.loadInBackground();

                if(cursor != null){
                    int column_index =
                            cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                    cursor.moveToFirst();
                    result = cursor.getString(column_index);
                }
                hasil = result;
            }
            catch (NoClassDefFoundError e)
            {
                hasil = "eror";
            }
        }

        if (hasil.equals("") || hasil.equals("eror"))
        {
            try
            {
                String[] proj = { MediaStore.Images.Media.DATA };
                String result = null;

                CursorLoader cursorLoader = new CursorLoader(
                        context,
                        uri, proj, null, null, null);
                cursor = cursorLoader.loadInBackground();

                if(cursor != null){
                    int column_index =
                            cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                    cursor.moveToFirst();
                    result = cursor.getString(column_index);
                }
                hasil = result;
            }
            catch (NoClassDefFoundError e)
            {
                hasil = "eror";
            }
        }

        if (hasil.equals("") || hasil.equals("eror"))
        {
            try
            {
                String[] proj = { MediaStore.Images.Media.DATA };
                cursor = context.getContentResolver().query(uri, proj, null, null, null);
                int column_index
                        = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                cursor.moveToFirst();
                hasil = cursor.getString(column_index);
            }
            catch (NoClassDefFoundError e)
            {
                hasil = "eror";
            }
        }

        return hasil;
    }
}
