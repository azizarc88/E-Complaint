package com.izarcsoftware.satpam;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.izarcsoftware.satpam.app.AppController;
import com.izarcsoftware.satpam.app.TouchImageView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class FotoActivity extends AppCompatActivity
{
    TouchImageView TIFoto;
    String url, bagian;
    boolean HD = false;
    private Bitmap bmp;
    String pesan = "";
    String namafile;
    private ImageLoader imageLoader = AppController.getInstance().getImageLoader();
    private float curzoom, curx = 0, cury = 0;

    /*private void AmbilFoto()
    {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    if (HD)
                    {
                        InputStream in = new URL(url.substring(0, url.lastIndexOf('/')) + "/Original/" + namafile).openStream();
                        bmp = BitmapFactory.decodeStream(in);
                    }
                    else
                    {
                        InputStream in = new URL(url).openStream();
                        bmp = BitmapFactory.decodeStream(in);
                    }
                } catch (OutOfMemoryError e) {
                    pesan = "OOM";
                }
                catch (Exception e)
                {
                    pesan = e.getMessage();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void result) {
                if (bmp != null)
                {
                    try
                    {
                        TIFoto.setImageBitmap(bmp);
                        TIFoto.setMaxZoom(8);
                    }
                    catch (Exception e)
                    {
                        Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }

                if (pesan.equalsIgnoreCase("OOM"))
                {
                    HD = false;
                    Toast.makeText(getApplicationContext(), "Ukuran foto HD terlalu besar untuk perangkat Anda", Toast.LENGTH_SHORT).show();
                }
                else if (!TextUtils.isEmpty(pesan))
                {
                    Toast.makeText(getApplicationContext(), pesan, Toast.LENGTH_SHORT).show();
                }
                pesan = "";
            }

        }.execute();
    }*/

    ImageLoader.ImageContainer newContainer;

    private void AmbilFoto()
    {
        String link;
        if (HD)
        {
            link = url.substring(0, url.lastIndexOf('/')) + "/Original/" + namafile;
            TIFoto.setMaxZoom(10);
        }
        else
        {
            TIFoto.setMaxZoom(3);
            link = url;
        }

        newContainer = imageLoader.get(link, new ImageLoader.ImageListener()
        {
            @Override
            public void onErrorResponse(VolleyError error) {
                TIFoto.setImageResource(R.drawable.memuat);
                if (HD)
                {
                    HD = false;
                }
            }

            @Override
            public void onResponse(final ImageLoader.ImageContainer response, boolean isImmediate)
            {
                if (response.getBitmap() != null)
                {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2)
                    {
                        TIFoto.animate().setDuration(300).alpha(0).setListener(new AnimatorListenerAdapter() {
                            @SuppressLint("NewApi")
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                TIFoto.setZoom(curzoom);
                                TIFoto.setScrollPosition(curx, cury);
                                TIFoto.setImageBitmap(response.getBitmap());
                                TIFoto.animate().setDuration(250).alpha(1).setListener(new AnimatorListenerAdapter()
                                {
                                    @Override
                                    public void onAnimationEnd(Animator animation)
                                    {
                                    }
                                });
                            }
                        });
                    }
                    else
                    {
                        TIFoto.setZoom(curzoom);
                        TIFoto.setScrollPosition(curx, cury);
                        TIFoto.setImageBitmap(response.getBitmap());
                    }

                }
                else
                {
                    TIFoto.setImageResource(R.drawable.memuat);
                    if (HD)
                    {
                        HD = false;
                    }
                }
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_foto);
        Intent intent = getIntent();
        url = intent.getStringExtra("url");
        bagian = intent.getStringExtra("bagian");

        namafile = url.substring(url.lastIndexOf('/') + 1);

        if (imageLoader == null) {
            imageLoader = AppController.getInstance().getImageLoader();
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Foto Laporan " + bagian);
        toolbar.setNavigationOnClickListener(new Toolbar.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        TIFoto = (TouchImageView) findViewById(R.id.TIFoto);
        curzoom = TIFoto.getCurrentZoom();
        AmbilFoto();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_foto_zoom, menu);
        return true;
    }

    @Override
    public void onBackPressed()
    {
        try
        {
            newContainer.cancelRequest();
        }
        catch (Exception ec)
        {

        }
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_simpan)
        {
            Bitmap bitmap = ((BitmapDrawable) TIFoto.getDrawable()).getBitmap();
            saveImageToInternalStorage(bitmap);
        }
        else if (id == R.id.action_hd)
        {
            if (!HD)
            {
                curzoom = TIFoto.getCurrentZoom();
                curx = TIFoto.getScrollPosition().x;
                cury = TIFoto.getScrollPosition().y;

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2)
                {
                    TIFoto.animate().setDuration(300).alpha(0).setListener(new AnimatorListenerAdapter() {
                        @SuppressLint("NewApi")
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            TIFoto.setImageResource(R.drawable.memuat);
                            TIFoto.resetZoom();
                            TIFoto.animate().setDuration(250).alpha(1).setListener(new AnimatorListenerAdapter() {
                                @Override
                                public void onAnimationEnd(Animator animation) {
                                    Toast.makeText(getApplicationContext(), "Mengambil foto kualitas tinggi", Toast.LENGTH_LONG).show();
                                    HD = true;
                                    AmbilFoto();
                                    Toast.makeText(getApplicationContext(), "Foto HD memiliki ukuran yang lebih besar, mungkin membutuhkan waktu yang lama", Toast.LENGTH_LONG).show();
                                }
                            });
                        }
                    });
                }
                else
                {
                    TIFoto.setImageResource(R.drawable.memuat);
                    TIFoto.resetZoom();

                    Toast.makeText(getApplicationContext(), "Mengambil foto kualitas tinggi", Toast.LENGTH_LONG).show();
                    HD = true;
                    AmbilFoto();
                    Toast.makeText(getApplicationContext(), "Foto HD memiliki ukuran yang lebih besar, mungkin membutuhkan waktu yang lama", Toast.LENGTH_LONG).show();
                }
            }
            else
            {
                Toast.makeText(getApplicationContext(), "Sudah HD", Toast.LENGTH_SHORT).show();
            }
        }
        else if (id == R.id.action_putar_clock)
        {
            TIFoto.setRotation(TIFoto.getRotation() + 90);
        }
        else if (id == R.id.action_putar_clockwise)
        {
            TIFoto.setRotation(TIFoto.getRotation() - 90);
        }

        return super.onOptionsItemSelected(item);
    }

    public boolean saveImageToInternalStorage(Bitmap image) {

        try {
            String root = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).toString();
            File myDir = new File(root + "/Laporan E-Complaint");

            File file = new File(myDir, namafile);

            if (!myDir.exists()) {
                myDir.mkdirs();
            }

            if (file.exists())
            {
                file.delete();
            }

            FileOutputStream fos = new FileOutputStream(file);
            image.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            fos.flush();
            fos.close();

            Toast.makeText(getApplicationContext(), "Foto telah disimpan di Pustaka", Toast.LENGTH_SHORT).show();
            return true;
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            return false;
        }
    }
}
