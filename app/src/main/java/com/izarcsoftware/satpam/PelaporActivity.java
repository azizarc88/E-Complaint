package com.izarcsoftware.satpam;

import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.LayoutAnimationController;
import android.view.animation.TranslateAnimation;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.izarcsoftware.satpam.adapter.PenggunaListAdapter;
import com.izarcsoftware.satpam.app.AppController;
import com.izarcsoftware.satpam.data.PenggunaItem;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class PelaporActivity extends AppCompatActivity
{
    private Toolbar toolbar;
    private static String URL_FEED;
    private TextView LNamaTop, LAlamatTop, LTotalTop, LInfo;
    private static ListView LVPengguna;
    private static PenggunaListAdapter listAdapter;
    private static List<PenggunaItem> penggunaItems;

    private void RefreshContent()
    {
        JsonObjectRequest jsonReq = new JsonObjectRequest(Request.Method.GET, URL_FEED, null, new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response) {
                if (response != null) {
                    penggunaItems.clear();
                    parseJsonLaporan(response);
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                RefreshContent();
            }
        });

        AppController.getInstance().addToRequestQueue(jsonReq);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pelapor);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Pelapor Tertinggi");
        toolbar.setNavigationOnClickListener(new Toolbar.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        LNamaTop = (TextView) findViewById(R.id.LNamaTop);
        LAlamatTop = (TextView) findViewById(R.id.LAlamatTop);
        LTotalTop = (TextView) findViewById(R.id.LTotalTop);
        LVPengguna = (ListView) findViewById(R.id.LVPengguna);
        LInfo = (TextView) findViewById(R.id.LInfo);

        URL_FEED = getString(R.string.alamat_server) + getString(R.string.link_ambil_pelapor_tertinggi);

        AnimationSet set = new AnimationSet(true);

        Animation animation = new AlphaAnimation(0.0f, 1.0f);
        animation.setDuration(300);
        set.addAnimation(animation);

        animation = new TranslateAnimation(
                Animation.RELATIVE_TO_SELF, 0.0f,Animation.RELATIVE_TO_SELF, 0.0f,
                Animation.RELATIVE_TO_SELF, -1.0f, Animation.RELATIVE_TO_SELF, 0.0f
        );
        animation.setDuration(150);
        set.addAnimation(animation);

        LayoutAnimationController controller = new LayoutAnimationController(set, 0.5f);
        LVPengguna.setLayoutAnimation(controller);

        penggunaItems = new ArrayList<PenggunaItem>();
        listAdapter = new PenggunaListAdapter(penggunaItems, this);
        LVPengguna.setAdapter(listAdapter);

        RefreshContent();
    }

    private void parseJsonLaporan(JSONObject response)
    {
        try
        {
            JSONArray feedArray = response.getJSONArray("feed");

            for (int i = 0; i < feedArray.length(); i++)
            {
                JSONObject feedObj = (JSONObject) feedArray.get(i);

                PenggunaItem item = new PenggunaItem();
                item.setNomor(i + 1);
                item.setInfo(feedObj.getString("info"));
                item.setPgid(feedObj.getString("pgid"));
                item.setNama(feedObj.getString("nama"));
                item.setDesa(feedObj.getString("desa"));
                item.setKota(feedObj.getString("kota"));
                item.setTotallaporan(feedObj.getString("totallaporan"));

                if (i == 0)
                {
                    if (!TextUtils.isEmpty(feedObj.getString("desa")))
                    {
                        LInfo.setText(feedObj.getString("info"));
                        LNamaTop.setText(feedObj.getString("nama"));
                        LAlamatTop.setText(feedObj.getString("desa") + ", " + feedObj.getString("kota"));
                        LTotalTop.setText("Total Laporan: " + feedObj.getString("totallaporan"));
                    }
                    else
                    {
                        LInfo.setText(feedObj.getString("info"));
                        LNamaTop.setText("Tidak ada");
                        LTotalTop.setText("Total Laporan: " + feedObj.getString("totallaporan"));
                    }
                }

                if (!TextUtils.isEmpty(feedObj.getString("desa")))
                {
                    penggunaItems.add(item);
                }
            }

            listAdapter.notifyDataSetChanged();
        }
        catch (Exception e)
        {
            Snackbar.make(LVPengguna, "Terjadi kesalahan, silakan coba lagi.", Snackbar.LENGTH_LONG).show();
        }
    }
}
