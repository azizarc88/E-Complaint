package com.izarcsoftware.satpam;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.izarcsoftware.satpam.adapter.FeedListAdapter;
import com.izarcsoftware.satpam.app.AppController;
import com.izarcsoftware.satpam.data.MarkerData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ampu2 on 03/08/2016.
 */
public class LokasiLaporanFas  extends ActionBarActivity implements OnMapReadyCallback
{
    private static GoogleMap mMap;
    private static String URL_FEED;
    public static SharedPreferences sharedPref;
    public static Context cc;
    public static List<MarkerData> feedItems;
    private static MenuItem menuItem, petasatelit;
    private static Boolean bulanterakhir = true;
    public static final String ALLOWED_URI_CHARS = "@#&=*+-_.,:!?()/~'%";

    private String bagian;

    private int min = 20;
    public static float zoom = 13;

    private boolean savezoom = false;


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.lokasi_menu, menu);
        menuItem = menu.findItem(R.id.bulanterakhir);
        menuItem.setChecked(true);
        petasatelit = menu.findItem(R.id.petasatelit);
        petasatelit.setChecked(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.bulanterakhir)
        {
            item.setChecked(!item.isChecked());
            bulanterakhir = item.isChecked();
            refreshContent(true);
        }
        else if (id == R.id.petasatelit)
        {
            item.setChecked(!item.isChecked());
            if (petasatelit.isChecked())
            {
                mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
            }
            else
            {
                mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            }
        }
        else if (id == R.id.atur_jumlah_min)
        {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Minimum Laporan yang Ditampilkan");

            final EditText input = new EditText(this);
            input.setText(String.valueOf(min));
            input.setInputType(InputType.TYPE_CLASS_NUMBER);
            builder.setView(input);

            builder.setPositiveButton("Terapkan", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    min = Integer.valueOf(input.getText().toString());
                    refreshContent(true);
                }
            });
            builder.setNegativeButton("Batal", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });

            builder.show();
        }

        return super.onOptionsItemSelected(item);
    }

    public void refreshContent(final boolean daricache)
    {
        String id = sharedPref.getString(cc.getString(R.string.p_id_pengguna), "Tidak Ada");
        String sebagai = sharedPref.getString(cc.getString(R.string.p_sebagai), "Tidak Ada");

        if (bagian.equalsIgnoreCase("fas"))
        {
            if (bulanterakhir)
            {
                URL_FEED = cc.getString(R.string.alamat_server) + cc.getString(R.string.link_ambil_lokasi_fas) + "?sebagai=" + sebagai + "&oleh=" + id;
            }
            else
            {
                URL_FEED = cc.getString(R.string.alamat_server) + cc.getString(R.string.link_ambil_lokasi_fas_all) + "?sebagai=" + sebagai + "&oleh=" + id + "&ll=" + String.valueOf(min);
            }
        }
        else
        {
            if (bulanterakhir)
            {
                URL_FEED = cc.getString(R.string.alamat_server) + cc.getString(R.string.link_ambil_lokasi_plgr) + "?sebagai=" + sebagai + "&oleh=" + id;
            }
            else
            {
                URL_FEED = cc.getString(R.string.alamat_server) + cc.getString(R.string.link_ambil_lokasi_plgr_all) + "?sebagai=" + sebagai + "&oleh=" + id + "&ll=" + String.valueOf(min);
            }
        }

        URL_FEED = Uri.encode(URL_FEED, LokasiLaporanFas.ALLOWED_URI_CHARS);

        JsonObjectRequest jsonReq = new JsonObjectRequest(Request.Method.GET, URL_FEED, null, new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                if (response != null) {
                    feedItems.clear();
                    parseJsonFeed(response, false);
                }
            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error) {
                try
                {
                }
                catch (Exception e)
                {
                    refreshContent(false);
                }
            }
        });

        AppController.getInstance().addToRequestQueue(jsonReq);
    }

    public void parseJsonFeed(JSONObject response, Boolean satuid) {
        try {
            JSONArray feedArray = response.getJSONArray("feed");

            mMap.clear();
            feedItems.clear();

            for (int i = 0; i < feedArray.length(); i++)
            {
                JSONObject feedObj = (JSONObject) feedArray.get(i);
                final MarkerData item = new MarkerData();
                item.setRlid(feedObj.getString("a"));
                item.setLat(feedObj.getString("b"));
                item.setLing(feedObj.getString("c"));
                item.setLfid(feedObj.getString("d"));
                item.setUrlfoto(feedObj.getString("e"));
                item.setStatus(feedObj.getString("f"));
                item.setAlamat(feedObj.getString("g"));
                item.setPrioritas(feedObj.getString("h"));
                item.setWaktu(feedObj.getString("i"));
                item.setOleh(feedObj.getString("j"));
                item.setDukungan(feedObj.getString("k"));
                item.setKomentar(feedObj.getString("l"));
                item.setNama(feedObj.getString("m"));
                item.setDidukung(feedObj.getString("o"));

                LatLng lokasi = new LatLng(Double.valueOf(item.getLat()), Double.valueOf(item.getLing()));
                mMap.addMarker(new MarkerOptions()
                        .position(lokasi)
                        .title(item.getNama() + "###" + i)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.marker))
                        .snippet(item.getStatus() + "###" + item.getAlamat())
                        .flat(true));

                feedItems.add(item);
            }

            LatLng lokasi = new LatLng(Double.valueOf(feedItems.get(feedItems.size() - 1).getLat()), Double.valueOf(feedItems.get(feedItems.size() - 1).getLing()));
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(lokasi, zoom));

            savezoom = true;
        } catch (JSONException e) {
            Toast.makeText(cc, "Belum ada laporan pada bulan ini", Toast.LENGTH_LONG).show();
        }
    }

    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lokasi_laporan_fas);

        bulanterakhir = true;

        Intent intent = getIntent();
        bagian = intent.getStringExtra("bagian");

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (bagian.equalsIgnoreCase("fas"))
        {
            getSupportActionBar().setTitle("Peta Lokasi Laporan Informasi");
        }
        else
        {
            getSupportActionBar().setTitle("Peta Lokasi Laporan Pelanggaran");
        }

        toolbar.setNavigationOnClickListener(new Toolbar.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                finish();
            }
        });


        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        sharedPref = getSharedPreferences("File_Pengaturan", Context.MODE_PRIVATE);

        feedItems = new ArrayList<MarkerData>();
    }

    class MyInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {

        private final View myContentsView;

        MyInfoWindowAdapter(){
            myContentsView = getLayoutInflater().inflate(R.layout.marker_info_window, null);
        }

        @Override
        public View getInfoContents(Marker marker) {

            TextView LNama = ((TextView) myContentsView.findViewById(R.id.LNama));
            TextView LAlamat = ((TextView) myContentsView.findViewById(R.id.LAlamat));
            TextView LLaporan = ((TextView) myContentsView.findViewById(R.id.LLaporan));

            String laporan = marker.getSnippet().substring(0, marker.getSnippet().lastIndexOf("###"));
            String alamat = marker.getSnippet().substring(marker.getSnippet().lastIndexOf("###") + 3);

            String judul = marker.getTitle().substring(0, marker.getTitle().lastIndexOf("###"));

            LNama.setText(judul);
            LAlamat.setText(alamat);
            LLaporan.setText(laporan);

            return myContentsView;
        }

        @Override
        public View getInfoWindow(Marker marker) {
            // TODO Auto-generated method stub
            return null;
        }

    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMyLocationEnabled(true);

        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.getUiSettings().setCompassEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(true);

        mMap.getUiSettings().setRotateGesturesEnabled(true);
        mMap.getUiSettings().setScrollGesturesEnabled(true);
        mMap.getUiSettings().setTiltGesturesEnabled(true);
        mMap.getUiSettings().setZoomGesturesEnabled(true);

        mMap.setInfoWindowAdapter(new MyInfoWindowAdapter());

        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                marker.showInfoWindow();
                return false;
            }
        });

        mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                try
                {
                    String posisi = marker.getTitle().substring(marker.getTitle().lastIndexOf("###") + 3);

                    MarkerData markerData = feedItems.get(Integer.valueOf(posisi));

                    Intent i = new Intent(getApplicationContext(), Komentar.class);
                    String[] n = {String.valueOf(markerData.getLfid()), bagian};
                    i.putExtra(FeedListAdapter.ID, n);
                    i.putExtra("daripemberitahuan", false);
                    i.putExtra("nama", markerData.getNama());
                    i.putExtra("waktu", markerData.getWaktu());
                    i.putExtra("prioritas", markerData.getPrioritas());
                    i.putExtra("dukungan", markerData.getDukungan());
                    i.putExtra("komentar", markerData.getKomentar());
                    i.putExtra("status", markerData.getStatus());
                    i.putExtra("urlforo", markerData.getUrlfoto());
                    i.putExtra("alamat", markerData.getAlamat());
                    i.putExtra("lfid", String.valueOf(markerData.getLfid()));
                    i.putExtra("lat", markerData.getLat());
                    i.putExtra("ling", markerData.getLing());
                    i.putExtra("oleh", markerData.getOleh());
                    Boolean didukung;
                    if (markerData.getDidukung().equalsIgnoreCase("1"))
                    {
                        didukung = true;
                    }
                    else
                    {
                        didukung = false;
                    }
                    i.putExtra("didukung", didukung);
                    startActivity(i);
                }
                catch (Exception ex)
                {
                    Toast.makeText(cc, ex.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        });

        mMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition cameraPosition) {
                if (savezoom)
                {
                    zoom = cameraPosition.zoom;
                }
            }
        });

        refreshContent(true);
    }
}
