package com.izarcsoftware.satpam;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.izarcsoftware.satpam.app.Crypt;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

public class Profil extends AppCompatActivity
{
    private EditText TBNama, TBAlamat, TBDesa, TBKota, TBNoHp, TBEmail, TBPassword, TBUlangiPassword, TBPassLama, TBPekerjaan;
    private DatePicker DPTanggalLahir;
    private Button BDaftar;
    private View CVPassLama;
    private String passlama, sebagai;
    private boolean done = false;

    public static final String EMAIL = "";

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profil);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new Toolbar.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                finish();
            }
        });

        sharedPref = getSharedPreferences("File_Pengaturan", Context.MODE_PRIVATE);

        TBNama = (EditText) findViewById(R.id.TBNama);
        TBAlamat = (EditText) findViewById(R.id.TBAlamat);
        TBDesa = (EditText) findViewById(R.id.TBDesa);
        TBKota = (EditText) findViewById(R.id.TBKota);
        TBNoHp = (EditText) findViewById(R.id.TBNoHp);
        TBEmail = (EditText) findViewById(R.id.TBEmail);
        TBPassword = (EditText) findViewById(R.id.TBPassword);
        TBPekerjaan = (EditText) findViewById(R.id.TBPekerjaan);
        TBUlangiPassword = (EditText) findViewById(R.id.TBUlangiPassword);
        TBPassLama = (EditText) findViewById(R.id.TBPassLama);

        CVPassLama = (View) findViewById(R.id.CVPassLama);

        DPTanggalLahir = (DatePicker) findViewById(R.id.DPTanggalLahir);

        TBPassword.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                if (done && TBPassLama.getVisibility() == View.GONE)
                {
                    TBPassword.setHint("Kata Sandi Baru");
                    TBUlangiPassword.setHint("Ulangi Kata Sandi Baru");
                    TBPassLama.setText("");
                    TBPassLama.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s)
            {

            }
        });

        BDaftar = (Button) findViewById(R.id.BDaftar);
        BDaftar.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (isAllValid())
                {
                    String[] isi = AmbilIsian();
                    new SignupActivity(getApplicationContext()).execute(isi[0],
                            isi[1],
                            isi[2],
                            isi[3],
                            isi[4],
                            isi[5],
                            isi[6],
                            isi[7],
                            isi[8],
                            isi[9],
                            isi[10]);
                }
            }
        });

        new BacaData(this).execute(sharedPref.getString(getString(R.string.p_id_pengguna), "Tidak Ada"));
    }

    private boolean isAllValid()
    {
        Boolean valid = true;

        long date = System.currentTimeMillis();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-M-dd");
        String dateString = sdf.format(date);
        curtanggal = String.valueOf(DPTanggalLahir.getYear()) + "-" + String.valueOf(DPTanggalLahir.getMonth() + 1) + "-" + String.valueOf(DPTanggalLahir.getDayOfMonth());

        if (TextUtils.isEmpty(TBNama.getText().toString()))
        {
            TBNama.setError(getString(R.string.error_field_required));
            TBNama.requestFocus();
            valid = false;
        }
        else if (TextUtils.isEmpty(TBAlamat.getText().toString()))
        {
            TBAlamat.setError(getString(R.string.error_field_required));
            TBAlamat.requestFocus();
            valid = false;
        }
        else if (TextUtils.isEmpty(TBDesa.getText().toString()))
        {
            TBDesa.setError(getString(R.string.error_field_required));
            TBDesa.requestFocus();
            valid = false;
        }
        else if (TextUtils.isEmpty(TBPekerjaan.getText().toString()))
        {
            TBPekerjaan.setError(getString(R.string.error_field_required));
            TBPekerjaan.requestFocus();
            valid = false;
        }
        else if (curtanggal.equalsIgnoreCase(dateString))
        {
            Toast.makeText(getApplicationContext(), "Tanggal lahir harap diisi", Toast.LENGTH_LONG).show();
            valid = false;
        }
        else if (TextUtils.isEmpty(TBPassLama.getText().toString()))
        {
            TBPassLama.setError(getString(R.string.error_field_required));
            TBPassLama.requestFocus();
            valid = false;
        }
        else if (TBPassLama.getText().toString().equals(passlama) == false)
        {
            TBPassLama.setError(getString(R.string.notmatch_password_lama));
            TBPassLama.requestFocus();
            valid = false;
        }
        else if (TextUtils.isEmpty(TBKota.getText().toString()))
        {
            TBKota.setError(getString(R.string.error_field_required));
            TBKota.requestFocus();
            valid = false;
        }
        else if (TextUtils.isEmpty(TBNoHp.getText().toString()))
        {
            TBNoHp.setError(getString(R.string.error_field_required));
            TBNoHp.requestFocus();
            valid = false;
        }
        else if (!TextUtils.isEmpty(TBNoHp.getText().toString()) && TBNoHp.getText().toString().length() < 10)
        {
            TBNoHp.setError(getString(R.string.error_invalid_nohp));
            TBNoHp.requestFocus();
            valid = false;
        }
        else if (TextUtils.isEmpty(TBEmail.getText().toString()))
        {
            TBEmail.setError(getString(R.string.error_field_required));
            TBEmail.requestFocus();
            valid = false;
        }
        else if (TBEmail.getText().toString().contains("@") == false || TBEmail.getText().toString().contains(".") == false)
        {
            TBEmail.setError(getString(R.string.error_invalid_email));
            TBEmail.requestFocus();
            valid = false;
        }
        else if (TextUtils.isEmpty(TBPassword.getText().toString()))
        {
            TBPassword.setError(getString(R.string.error_field_required));
            TBPassword.requestFocus();
            valid = false;
        }
        else if (!TextUtils.isEmpty(TBPassword.getText().toString()) && TBPassword.getText().toString().length() < 5)
        {
            TBPassword.setError(getString(R.string.error_invalid_password));
            TBPassword.requestFocus();
            valid = false;
        }
        else if (!TBPassword.getText().toString().equals(TBUlangiPassword.getText().toString()))
        {
            TBUlangiPassword.setError(getString(R.string.notmatch_password));
            TBUlangiPassword.requestFocus();
            valid = false;
        }

        return valid;
    }

    SharedPreferences sharedPref;

    public String[] AmbilIsian()
    {
        String[] Isian = {"", "", "", "", "", "", "", "", "", "", ""};

        Isian[0] = sharedPref.getString(getString(R.string.p_id_pengguna), "Tidak Ada");
        Isian[1] = sebagai;
        Isian[2] = TBNama.getText().toString();
        Isian[3] = TBAlamat.getText().toString();
        Isian[4] = TBDesa.getText().toString();
        Isian[5] = TBKota.getText().toString();
        Isian[6] = String.valueOf(DPTanggalLahir.getYear()) + "-" + String.valueOf(DPTanggalLahir.getMonth() + 1) + "-" + String.valueOf(DPTanggalLahir.getDayOfMonth());
        Isian[7] = TBNoHp.getText().toString();
        Isian[8] = TBEmail.getText().toString();
        Isian[9] = TBPekerjaan.getText().toString();
        Isian[10] = TBPassword.getText().toString();

        return  Isian;
    }

    private ProgressDialog pDialog;

    class SignupActivity extends AsyncTask<String, Void, String>
    {
        private Context context;

        public SignupActivity(Context context)
        {
            this.context = context;
        }

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            pDialog = new ProgressDialog(Profil.this);
            pDialog.setMessage("Mengubah...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... arg0)
        {
            RequestHandler rh = new RequestHandler();

            String link;
            String result;

            try {
                HashMap<String,String> data = new HashMap<>();
                data.put("pgid", arg0[0]);
                data.put("sebagai", arg0[1]);
                data.put("nama", arg0[2]);
                data.put("alamat", arg0[3]);
                data.put("desa", arg0[4]);
                data.put("kota", arg0[5]);
                data.put("tanggallahir", arg0[6]);
                data.put("nohp", arg0[7]);
                data.put("email", arg0[8]);
                data.put("pekerjaan", arg0[9]);
                data.put("password", arg0[10]);

                link = getString(R.string.alamat_server) + getString(R.string.link_ubah_pengguna);

                result = rh.sendPostRequest(link, data);

                return result;
            }
            catch (Exception e)
            {
                return new String("Exception: " + e.getMessage());
            }
        }

        @Override
        protected void onPostExecute(String result)
        {
            String jsonStr = result;
            if (jsonStr != null)
            {
                try
                {
                    JSONObject jsonObj = new JSONObject(jsonStr);
                    String query_result = jsonObj.getString("hasil");
                    if (query_result.equals("SUKSES"))
                    {
                        Snackbar.make(TBNama, "Profil Anda sudah dirubah", Snackbar.LENGTH_LONG).show();

                        SharedPreferences.Editor editor = sharedPref.edit();
                        editor.putString(getString(R.string.p_nama), TBNama.getText().toString());
                        editor.putString(getString(R.string.p_nohp), Crypt.Encrypt(TBNoHp.getText().toString()));
                        editor.putString(getString(R.string.p_email), Crypt.Encrypt(TBEmail.getText().toString()));
                        editor.putString(getString(R.string.p_password), Crypt.Encrypt(TBPassword.getText().toString()));
                        editor.commit();
                    } else if (query_result.equals("GAGAL"))
                    {
                        Snackbar.make(TBNama, "Terjadi kesalahan, silakan coba lagi", Snackbar.LENGTH_LONG).show();
                    } else if (query_result.equals("EMAIL TIDAK VALID"))
                    {
                        Toast.makeText(context, "Email sudah dipakai.", Toast.LENGTH_SHORT).show();
                    } else if (query_result.equals("NO HP TIDAK VALID"))
                    {
                        Toast.makeText(context, "No. Handphone sudah dipakai.", Toast.LENGTH_SHORT).show();
                    } else
                    {
                        Snackbar.make(TBNama, "Tidak dapat terhubung, periksa internet Anda", Snackbar.LENGTH_LONG).show();
                    }
                } catch (JSONException e)
                {
                    e.printStackTrace();
                    Snackbar.make(TBNama, "Tidak dapat terhubung, periksa internet Anda", Snackbar.LENGTH_LONG).show();
                }
            } else
            {
                Snackbar.make(TBNama, "Tidak dapat terhubung, periksa internet Anda", Snackbar.LENGTH_LONG).show();
            }

            if (pDialog.isShowing())
                pDialog.dismiss();
        }
    }

    String curtanggal;

    class BacaData extends AsyncTask<String, Void, String>
    {
        private Context context;

        public BacaData(Context context)
        {
            this.context = context;
        }

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();

            pDialog = new ProgressDialog(Profil.this);
            pDialog.setMessage("Memuat...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... arg0)
        {
            RequestHandler rh = new RequestHandler();

            String link;
            String result;

            try {
                HashMap<String,String> data = new HashMap<>();
                data.put("id", arg0[0]);

                link = getString(R.string.alamat_server) + getString(R.string.link_baca_pengguna);

                result = rh.sendPostRequest(link, data);

                return result;
            }
            catch (Exception e)
            {
                return new String("Exception: " + e.getMessage());
            }
        }

        @Override
        protected void onPostExecute(String result)
        {
            String jsonStr = Crypt.Decrypt(result);
            if (jsonStr != null)
            {
                try
                {
                    JSONObject jsonObj = new JSONObject(jsonStr);
                    String query_result = jsonObj.getString("hasil");
                    if (query_result.equals("SUKSES"))
                    {
                        TBNama.setText(jsonObj.getString("nama"));
                        sebagai = jsonObj.getString("sebagai");
                        TBAlamat.setText(jsonObj.getString("alamat"));
                        TBDesa.setText(jsonObj.getString("desa"));
                        TBKota.setText(jsonObj.getString("kota"));

                        String expectedPattern = "yyyy-MM-dd";
                        SimpleDateFormat formatter = new SimpleDateFormat(expectedPattern);
                        String userInput = jsonObj.getString("tanggallahir");
                        Date tanggal = null;
                        try
                        {
                            tanggal = formatter.parse(userInput);
                        } catch (ParseException e)
                        {
                            e.printStackTrace();
                        }

                        try
                        {
                            DPTanggalLahir.setMaxDate(tanggal.getTime());
                        }
                        catch (Exception ex)
                        {
                        }
                        try
                        {
                            DPTanggalLahir.setMaxDate(Calendar.getInstance().getTime().getTime());
                        }
                        catch (Exception ex)
                        {
                        }

                        TBNoHp.setText(jsonObj.getString("nohp"));
                        TBEmail.setText(jsonObj.getString("email"));
                        TBPekerjaan.setText(jsonObj.getString("pekerjaan"));
                        TBPassword.setText(jsonObj.getString("password"));
                        TBPassLama.setText(jsonObj.getString("password"));
                        passlama = jsonObj.getString("password");

                        if (TextUtils.isEmpty(TBPassword.getText()))
                        {
                            TBPassword.setHint("Kata Sandi Baru");
                            TBUlangiPassword.setHint("Ulangi Kata Sandi Baru");
                            TBPassLama.setText("");
                            TBPassLama.setVisibility(View.VISIBLE);
                        }

                        done = true;
                    } else if (query_result.equals("GAGAL"))
                    {
                        Snackbar.make(TBNama, "Terjadi kesalahan, silakan coba lagi", Snackbar.LENGTH_LONG).show();
                    } else
                    {
                        Snackbar.make(TBNama, "Tidak dapat terhubung, periksa internet Anda", Snackbar.LENGTH_LONG).show();
                    }
                } catch (JSONException e)
                {
                    e.printStackTrace();
                    Snackbar.make(TBNama, "Tidak dapat terhubung, periksa internet Anda", Snackbar.LENGTH_LONG).show();
                }
            } else
            {
                Snackbar.make(TBNama, "Tidak dapat terhubung, periksa internet Anda", Snackbar.LENGTH_LONG).show();
            }

            if (pDialog.isShowing())
                pDialog.dismiss();
        }
    }
}
