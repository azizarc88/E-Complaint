package com.izarcsoftware.satpam.app;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ScaleDrawable;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by Aziz Nur Ariffianto on 19 Agustus 2016.
 */
public class ResourcesManager
{
    public static void ScaleIcon(Context cc, Button view, int resources)
    {
        Drawable drawable = cc.getResources().getDrawable(resources);
        drawable.setBounds(0, 0, (int)(drawable.getIntrinsicWidth()*0.6), (int)(drawable.getIntrinsicHeight()*0.6));
        ScaleDrawable sd = new ScaleDrawable(drawable, 0, view.getWidth(), view.getHeight());
        view.setCompoundDrawables(sd.getDrawable(), null, null, null);
    }

    public static void ScaleIcon(Context cc, TextView view, int resources)
    {
        Drawable drawable = cc.getResources().getDrawable(resources);
        drawable.setBounds(0, 0, (int)(drawable.getIntrinsicWidth()*0.6), (int)(drawable.getIntrinsicHeight()*0.6));
        ScaleDrawable sd = new ScaleDrawable(drawable, 0, view.getWidth(), view.getHeight());
        view.setCompoundDrawables(sd.getDrawable(), null, null, null);
    }
}
