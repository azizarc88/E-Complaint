package com.izarcsoftware.satpam.app;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.net.Uri;
import android.util.Base64;
import android.widget.Toast;

import com.facebook.FacebookSdk;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.model.ShareOpenGraphAction;
import com.facebook.share.model.ShareOpenGraphContent;
import com.facebook.share.model.ShareOpenGraphObject;
import com.facebook.share.widget.ShareDialog;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by Aziz Nur Ariffianto on 04 September 2016.
 */
public class Satpam
{
    public static void Bagikan(Activity cc, String judul, String isi, String urlimage)
    {
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, judul);
        sharingIntent.putExtra(Intent.EXTRA_TEXT, isi);
        sharingIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        cc.startActivity(Intent.createChooser(sharingIntent, "Bagikan lewat"));
    }

    public static String printKeyHash(Activity context) {
        PackageInfo packageInfo;
        String key = null;
        try {
            //getting application package name, as defined in manifest
            String packageName = context.getApplicationContext().getPackageName();

            //Retriving package info
            packageInfo = context.getPackageManager().getPackageInfo(packageName,
                    PackageManager.GET_SIGNATURES);

            for (Signature signature : packageInfo.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                key = new String(Base64.encode(md.digest(), 0));
            }
        } catch (PackageManager.NameNotFoundException e1) {
        }
        catch (NoSuchAlgorithmException e) {
        } catch (Exception e) {
        }

        return key;
    }

    public static void setupFacebookShareIntent(final Activity activity, String asal, String waktu, String alamat, String isi, String foto, boolean informasi)
    {
        String tipe, objek;

        if (informasi)
        {
            tipe = "ecomplaint_polresba:laporan_informasi";
            objek = "laporan_informasi";
        }
        else
        {
            tipe = "ecomplaint_polresba:laporan_pelanggaran";
            objek = "laporan_pelanggaran";
        }

        ShareOpenGraphObject object = new ShareOpenGraphObject.Builder()
                .putString("og:type", tipe)
                .putString("og:title", asal)
                .putString("og:url", "https://play.google.com/store/apps/details?id=com.izarcsoftware.satpam")
                .putString("og:description", isi)
                .putString("og:locale", "id_ID")
                .putString("og:locale:alternate", "en_US")
                .putString("ecomplaint_polresba:lokasi", alamat)
                .putString("ecomplaint_polresba:waktu", waktu)
                .putString("og:image", foto)
                .build();

        ShareOpenGraphAction action = new ShareOpenGraphAction.Builder()
                .setActionType("ecomplaint_polresba:membagikan")
                .putObject(objek, object)
                .build();

        ShareOpenGraphContent content = new ShareOpenGraphContent.Builder()
                .setPreviewPropertyName(objek)
                .setAction(action)
                .build();

        ShareDialog.show(activity, content);
    }
}
