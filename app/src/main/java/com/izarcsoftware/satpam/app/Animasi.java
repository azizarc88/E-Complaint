package com.izarcsoftware.satpam.app;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.os.Build;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by Aziz Nur Ariffianto on 17 Agustus 2016.
 */
public class Animasi
{
    public static void Fade(final View view, final boolean tampilkan, int durasi)
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2)
        {
            view.animate().setDuration(durasi).alpha(!tampilkan ? 0 : 1).setListener(new AnimatorListenerAdapter()
            {
                @Override
                public void onAnimationEnd(Animator animation)
                {
                    view.setVisibility(!tampilkan ? View.GONE : View.VISIBLE);
                }
            });
        }
        else
        {
            view.setVisibility(!tampilkan ? View.GONE : View.VISIBLE);
        }
    }

    public static void Fade(final View view, final boolean tampilkan, int durasi, final boolean gone)
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2)
        {
            view.animate().setDuration(durasi).alpha(!tampilkan ? 0 : 1).setListener(new AnimatorListenerAdapter()
            {
                @Override
                public void onAnimationEnd(Animator animation)
                {
                    if (gone)
                    {
                        view.setVisibility(!tampilkan ? View.GONE : View.VISIBLE);
                    }
                    else
                    {
                        view.setVisibility(!tampilkan ? View.INVISIBLE : View.VISIBLE);
                    }
                }
            });
        }
        else
        {
            if (gone)
            {
                view.setVisibility(!tampilkan ? View.GONE : View.VISIBLE);
            }
            else
            {
                view.setVisibility(!tampilkan ? View.INVISIBLE : View.VISIBLE);
            }
        }
    }

    public static void Fade(final View view, final View view2, final int durasi)
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2)
        {
            view.animate().setDuration(durasi).alpha(0).setListener(new AnimatorListenerAdapter()
            {
                @Override
                public void onAnimationEnd(Animator animation)
                {
                    view.setVisibility(View.GONE);
                    view2.setVisibility(View.VISIBLE);
                    view2.setAlpha(0f);
                    view2.animate().setDuration(durasi).alpha(1).setListener(new AnimatorListenerAdapter()
                    {
                        @Override
                        public void onAnimationEnd(Animator animation)
                        {
                            view2.setVisibility(View.VISIBLE);
                        }
                    });
                }
            });
        }
        else
        {
            view.setVisibility(View.GONE);
            view.setVisibility(View.VISIBLE);
        }
    }

    public static void Fade(final View view, final View view2, final int durasi, final boolean gone)
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2)
        {
            view.animate().setDuration(durasi).alpha(0).setListener(new AnimatorListenerAdapter()
            {
                @Override
                public void onAnimationEnd(Animator animation)
                {
                    view.setVisibility(gone ? View.GONE : View.INVISIBLE);
                    view2.setVisibility(View.VISIBLE);
                    view2.setAlpha(0f);
                    view2.animate().setDuration(durasi).alpha(1).setListener(new AnimatorListenerAdapter()
                    {
                        @Override
                        public void onAnimationEnd(Animator animation)
                        {
                            view2.setVisibility(View.VISIBLE);
                        }
                    });
                }
            });
        }
        else
        {
            view.setVisibility(gone ? View.GONE : View.INVISIBLE);
            view.setVisibility(View.VISIBLE);
        }
    }

    public static void Fade(final View view, final int durasi, final int drawable)
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2)
        {
            view.animate().setDuration(durasi).alpha(0).setListener(new AnimatorListenerAdapter()
            {
                @Override
                public void onAnimationEnd(Animator animation)
                {
                    view.setBackgroundResource(drawable);
                    view.animate().setDuration(durasi).alpha(1).setListener(new AnimatorListenerAdapter()
                    {
                        @Override
                        public void onAnimationEnd(Animator animation)
                        {

                        }
                    });
                }
            });
        }
        else
        {
            view.setBackgroundResource(drawable);
        }
    }

    public static void Fade(final ImageView view, final int durasi, final int drawable, final AnimatorListenerAdapter animationListener)
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2)
        {
            view.animate().setDuration(durasi).alpha(0).setListener(new AnimatorListenerAdapter()
            {
                @Override
                public void onAnimationEnd(Animator animation)
                {
                    view.setImageResource(drawable);
                    view.animate().setDuration(durasi).alpha(1).setListener(animationListener);
                }
            });
        }
        else
        {
            view.setBackgroundResource(drawable);
        }
    }

    public static void Fade(final TextView view, final int durasi, final int warna)
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2)
        {
            view.animate().setDuration(durasi).alpha(0).setListener(new AnimatorListenerAdapter()
            {
                @Override
                public void onAnimationEnd(Animator animation)
                {
                    view.setTextColor(warna);
                    view.animate().setDuration(durasi).alpha(1).setListener(new AnimatorListenerAdapter()
                    {
                        @Override
                        public void onAnimationEnd(Animator animation)
                        {

                        }
                    });
                }
            });
        }
        else
        {
            view.setTextColor(warna);
        }
    }

    public static void Fade(final TextView view, final int durasi, final String teks)
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2)
        {
            view.animate().setDuration(durasi).alpha(0).setListener(new AnimatorListenerAdapter()
            {
                @Override
                public void onAnimationEnd(Animator animation)
                {
                    view.setText(teks);
                    view.animate().setDuration(durasi).alpha(1).setListener(new AnimatorListenerAdapter()
                    {
                        @Override
                        public void onAnimationEnd(Animator animation)
                        {

                        }
                    });
                }
            });
        }
        else
        {
            view.setText(teks);
        }
    }

    public static void Translate(final View view, int delay, float endvalue, String axis)
    {
        if (axis.equalsIgnoreCase("x"))
        {
            view.animate().setDuration(delay).translationX(view.getTranslationX()).translationXBy(endvalue).setListener(new AnimatorListenerAdapter()
            {
                @Override
                public void onAnimationEnd(Animator animation)
                {
                    super.onAnimationEnd(animation);
                }
            });
        }
        else
        {
            view.animate().setDuration(delay).translationY(view.getTranslationY()).translationYBy(endvalue).setListener(new AnimatorListenerAdapter()
            {
                @Override
                public void onAnimationEnd(Animator animation)
                {
                    super.onAnimationEnd(animation);
                }
            });
        }
    }
}
