package com.izarcsoftware.satpam.app;

import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Aziz Nur Ariffianto on 10 Agustus 2016.
 */
public class Crypt
{
    public static final String PASSWORD_CRYPT = "kldjforaeswutnpomdfsohaocwexc";

    private static char[] kar = {'#', '.', '!', '/', '\\', '[', ']', '?', '=', '\'', ',', '.', '-', ':', ' ', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b',
                                 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C',
                                 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
                                 '`', '~', '@', '$', '%', '^', '&', '*', '(', ')', '_', '+', '{', '}', ';', '"', '|', '<', '>'};

    private static int Konversi(char karakter)
    {
        return (int) karakter;
    }

    private static char Deversi(int nilai)
    {
        return (char) nilai;
    }

    public static String Encrypt(String teks)
    {
        String kunci = PASSWORD_CRYPT;
        String k = "";
        List<Integer> ki = new ArrayList<Integer>();
        List<Integer> ti = new ArrayList<Integer>();
        List<Integer> hi = new ArrayList<Integer>();
        String h = "";
        char[] cc = kunci.toCharArray();
        int b = 0;
        int v = 0;
        for (char c : teks.toCharArray())
        {
            k = (k + cc[b]);
            b++;
            if (b == kunci.length())
            {
                b = 0;
            }
        }

        for (char c : k.toCharArray())
        {
            ki.add(Konversi(c));
        }

        for (char c : teks.toCharArray()) {
            ti.add(Konversi(c));
        }

        int i;
        for (int c : ti)
        {
            i = c + ki.get(v);
            hi.add(i);

            v++;
        }

        for (int c : hi)
        {
            h = h + (char) (c);
        }

        return h;
    }

    public static String Decrypt(String teks)
    {
        String kunci = PASSWORD_CRYPT;
        String k = "";
        List<Integer> ki = new ArrayList<Integer>();
        List<Integer> ti = new ArrayList<Integer>();
        List<Integer> hi = new ArrayList<Integer>();
        String h = "";
        char[] cc = kunci.toCharArray();
        int b = 0;
        int v = 0;

        for (char c : teks.toCharArray())
        {
            k = (k + cc[b]);
            b++;
            if (b == kunci.length())
            {
                b = 0;
            }
        }

        for (char c : k.toCharArray())
        {
            ki.add(Konversi(c));
        }

        for (char c : teks.toCharArray())
        {
            ti.add((int) c);
        }

        int i;
        for (int c : ti)
        {
            i = c - ki.get(v);
            hi.add(i);

            v++;
        }

        for (int c : hi)
        {
            h = h + Deversi(c);
        }

        return h;
    }
}
