package com.izarcsoftware.satpam.app;

/**
 * Created by Aziz Nur Ariffianto on 0012, 12 Feb 2016.
 */
public class Validation
{
    public static boolean isSopan(String teks)
    {
        boolean hasil = true;
        String[] katakotor = {"dijual", "di jual", "wts", "dicari", "bangsat", "bngst", "bangst", "bngsat", "asu", "celek", "clek", "clek", "bajingan", "bajing", "bjingan", "bjngan", "bajingn", "tembelek", "tmbelek", "tmblek", "kopet", "kpet", "anjing", "anjng", "dapuk", "dafuq", "dfuk", "dfuq", "dafq", "damn", "kntol", "kontol", "kntl", "pjuh", "pejuh", "pejh", "pejring", "pjring", "pejrng", "anjrit", "anjrt", "njing", "fuck", "fck", "fvck", "asv", "pjvh", "pejvh", "dfvk", "dafvk", "dafvq", "dafvk"};

        for (String kata : katakotor)
        {
            int charcode;
            String temp = "";

            for (char huruf : teks.toCharArray())
            {
                charcode = (int) huruf;
                if (charcode > 64 && charcode < 123)
                {
                    temp += huruf;
                }
                else if (charcode == 32)
                {
                    temp += huruf;
                }
            }

            String[] katateks = temp.split(" ");

            for (String kata2 : katateks)
            {
                if (kata2.equalsIgnoreCase(kata))
                {
                    return false;
                }
            }
        }

        return hasil;
    }
}
