package com.izarcsoftware.satpam;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.izarcsoftware.satpam.app.AppController;
import com.izarcsoftware.satpam.data.PemberitahuanItem;

import org.json.JSONArray;
import org.json.JSONObject;

public class RefreshActivity extends AppCompatActivity
{
    String URL_FEED;
    private static String id;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_refresh);

        SharedPreferences sharedPref;
        sharedPref = this.getSharedPreferences("File_Pengaturan", Context.MODE_PRIVATE);
        id = sharedPref.getString(getString(R.string.p_id_pengguna), "Tidak Ada");

        URL_FEED = getString(R.string.alamat_server) + getString(R.string.link_ambil_pemberitahuan) + "?pgid=" + id;
    }
}
