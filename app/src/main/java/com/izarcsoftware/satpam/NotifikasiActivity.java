package com.izarcsoftware.satpam;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.izarcsoftware.satpam.app.ObjectSerializer;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Ampu2 on 04/04/2016.
 */
public class NotifikasiActivity extends AppCompatActivity
{
    private Toolbar toolbar;
    private ListView LVNotif;
    private TextView LInfo;
    public ArrayList<String> pemberitahuan, temp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifikasi);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Pemberitahuan Langsung");
        toolbar.setNavigationOnClickListener(new Toolbar.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        LVNotif = (ListView) findViewById(R.id.LVNotif);

        SharedPreferences prefs = getSharedPreferences("File_Pengaturan", Context.MODE_PRIVATE);

        pemberitahuan = new ArrayList<String>();
        temp = new ArrayList<String>();

        if (!prefs.getString("pemberitahuan", "Tidak Ada").equalsIgnoreCase("Tidak Ada"))
        {
            try {
                pemberitahuan = (ArrayList<String>) ObjectSerializer.deserialize(prefs.getString("pemberitahuan", ObjectSerializer.serialize(new ArrayList<String>())));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        for (String item : pemberitahuan)
        {
            if (item.length() > 50)
            {
                temp.add(item.substring(0, 50) + "...");
            }
            else
            {
                temp.add(item);
            }
        }

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, temp);
        LVNotif.setAdapter(arrayAdapter);

        LVNotif.setOnItemClickListener(new ListView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getApplicationContext(), PemberitahuanLangsung.class);
                intent.putExtra("info", pemberitahuan.get(position));
                startActivity(intent);
            }
        });
    }
}
